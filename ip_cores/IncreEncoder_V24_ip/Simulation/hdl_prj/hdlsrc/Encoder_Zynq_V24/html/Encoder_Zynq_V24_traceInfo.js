function RTW_Sid2UrlHash() {
	this.urlHashMap = new Array();
	/* <S5>/Abs */
	this.urlHashMap["Encoder_Zynq_V24:4013"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:632,633,634";
	/* <S5>/Average */
	this.urlHashMap["Encoder_Zynq_V24:4111"] = "msg=&block=Encoder_Zynq_V24:4111";
	/* <S5>/Average_linear_Reg */
	this.urlHashMap["Encoder_Zynq_V24:3649"] = "msg=&block=Encoder_Zynq_V24:3649";
	/* <S5>/Check_Direction */
	this.urlHashMap["Encoder_Zynq_V24:2052"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:306,307,308,309,310,311,312,313";
	/* <S5>/Counter */
	this.urlHashMap["Encoder_Zynq_V24:3596"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:338,339,340,341,342,343,344,345,346";
	/* <S5>/Counter_position */
	this.urlHashMap["Encoder_Zynq_V24:2109"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:359,360,361,362,363,364,365,366,367,368";
	/* <S5>/Counter_theta_ele */
	this.urlHashMap["Encoder_Zynq_V24:2149"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:348,349,350,351,352,353,354,355,356,357";
	/* <S5>/Data Type Conversion1 */
	this.urlHashMap["Encoder_Zynq_V24:4107"] = "msg=&block=Encoder_Zynq_V24:4107";
	/* <S5>/Data Type Conversion2 */
	this.urlHashMap["Encoder_Zynq_V24:3686"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:494";
	/* <S5>/Data Type Conversion3 */
	this.urlHashMap["Encoder_Zynq_V24:4106"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:396";
	/* <S5>/Data Type Conversion4 */
	this.urlHashMap["Encoder_Zynq_V24:4680"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:478";
	/* <S5>/Data Type Conversion9 */
	this.urlHashMap["Encoder_Zynq_V24:2187"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:611";
	/* <S5>/Div_i1 */
	this.urlHashMap["Encoder_Zynq_V24:4671"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:496,497,498,500,501,502";
	/* <S5>/HDL Reciprocal */
	this.urlHashMap["Encoder_Zynq_V24:3684"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:329,330,331,332,333,334,335,336";
	/* <S5>/Logical1 */
	this.urlHashMap["Encoder_Zynq_V24:2189"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:534";
	/* <S5>/Logical2 */
	this.urlHashMap["Encoder_Zynq_V24:2348"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:380";
	/* <S5>/Pipeline1 */
	this.urlHashMap["Encoder_Zynq_V24:4079"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:522,523,524,525,526,527,528,529,530,531";
	/* <S5>/Pipeline2 */
	this.urlHashMap["Encoder_Zynq_V24:4081"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:550,551,552,553,554,555,556,557,558,559";
	/* <S5>/Pipeline3 */
	this.urlHashMap["Encoder_Zynq_V24:4082"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:599,600,601,602,603,604,605,606,607,608";
	/* <S5>/Pipeline4 */
	this.urlHashMap["Encoder_Zynq_V24:4083"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:438,439,440,441,442,443,444,445,446,447";
	/* <S5>/Pipeline5 */
	this.urlHashMap["Encoder_Zynq_V24:5056"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:564,565,566,567,568,569,570,571,572,573";
	/* <S5>/Pipeline6 */
	this.urlHashMap["Encoder_Zynq_V24:4086"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:384,385,386,387,388,389,390,391,392,393";
	/* <S5>/Pipeline7 */
	this.urlHashMap["Encoder_Zynq_V24:4105"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:410,411,412,413,414,415,416,417,418,419";
	/* <S5>/Product_theta_el */
	this.urlHashMap["Encoder_Zynq_V24:2190"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:576,577,578,579";
	/* <S5>/Scope */
	this.urlHashMap["Encoder_Zynq_V24:4085"] = "msg=rtwMsg_notTraceable&block=Encoder_Zynq_V24:4085";
	/* <S5>/WaitSomeTime */
	this.urlHashMap["Encoder_Zynq_V24:4647"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:504,505,506,507,508,509,510,511,512,513";
	/* <S5>/hold1 */
	this.urlHashMap["Encoder_Zynq_V24:2193"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:424,425,426,427,428,429,430,431,432,433";
	/* <S5>/hold2 */
	this.urlHashMap["Encoder_Zynq_V24:4090"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:480,481,482,483,484,485,486,487,488,489";
	/* <S5>/hold3 */
	this.urlHashMap["Encoder_Zynq_V24:4645"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:464,465,466,467,468,469,470,471,472,473";
	/* <S5>/hold4 */
	this.urlHashMap["Encoder_Zynq_V24:4091"] = "msg=rtwMsg_notTraceable&block=Encoder_Zynq_V24:4091";
	/* <S5>/hold5 */
	this.urlHashMap["Encoder_Zynq_V24:4675"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:452,453,454,455,456,457,458,459,460,461";
	/* <S5>/omega_by_count_lines */
	this.urlHashMap["Encoder_Zynq_V24:3723"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:370,371,372,373,374,375,376,377,378";
	/* <S5>/omega_by_measure_time */
	this.urlHashMap["Encoder_Zynq_V24:2194"] = "IncreEncoder_V24_ip_src_IncreEncoder_V24.vhd:315,316,317,318,319,320,321,322,323,324,325,326,327";
	/* <S5>/omega_by_measure_time_old */
	this.urlHashMap["Encoder_Zynq_V24:4199"] = "msg=&block=Encoder_Zynq_V24:4199";
	/* <S5>/OverSamplFactor */
	this.urlHashMap["Encoder_Zynq_V24:5055"] = "msg=&block=Encoder_Zynq_V24:5055";
	/* <S33>/OmegaIn
 */
	this.urlHashMap["Encoder_Zynq_V24:4112"] = "msg=&block=Encoder_Zynq_V24:4112";
	/* <S33>/NewMeasurement */
	this.urlHashMap["Encoder_Zynq_V24:4113"] = "msg=&block=Encoder_Zynq_V24:4113";
	/* <S33>/SamplingFactorInv */
	this.urlHashMap["Encoder_Zynq_V24:4114"] = "msg=&block=Encoder_Zynq_V24:4114";
	/* <S33>/SamplingFactor */
	this.urlHashMap["Encoder_Zynq_V24:4115"] = "msg=&block=Encoder_Zynq_V24:4115";
	/* <S33>/Add */
	this.urlHashMap["Encoder_Zynq_V24:4116"] = "msg=&block=Encoder_Zynq_V24:4116";
	/* <S33>/Ask1 */
	this.urlHashMap["Encoder_Zynq_V24:4117"] = "msg=&block=Encoder_Zynq_V24:4117";
	/* <S33>/Ask2 */
	this.urlHashMap["Encoder_Zynq_V24:4118"] = "msg=&block=Encoder_Zynq_V24:4118";
	/* <S33>/Ask3 */
	this.urlHashMap["Encoder_Zynq_V24:4119"] = "msg=&block=Encoder_Zynq_V24:4119";
	/* <S33>/Ask4 */
	this.urlHashMap["Encoder_Zynq_V24:4120"] = "msg=&block=Encoder_Zynq_V24:4120";
	/* <S33>/Ask5 */
	this.urlHashMap["Encoder_Zynq_V24:4121"] = "msg=&block=Encoder_Zynq_V24:4121";
	/* <S33>/Constant1 */
	this.urlHashMap["Encoder_Zynq_V24:4122"] = "msg=&block=Encoder_Zynq_V24:4122";
	/* <S33>/NewSpeed */
	this.urlHashMap["Encoder_Zynq_V24:4123"] = "msg=&block=Encoder_Zynq_V24:4123";
	/* <S33>/NewSpeed1 */
	this.urlHashMap["Encoder_Zynq_V24:4124"] = "msg=&block=Encoder_Zynq_V24:4124";
	/* <S33>/NewSpeed2 */
	this.urlHashMap["Encoder_Zynq_V24:4125"] = "msg=&block=Encoder_Zynq_V24:4125";
	/* <S33>/NewSpeed3 */
	this.urlHashMap["Encoder_Zynq_V24:4126"] = "msg=&block=Encoder_Zynq_V24:4126";
	/* <S33>/NewSpeed4 */
	this.urlHashMap["Encoder_Zynq_V24:4127"] = "msg=&block=Encoder_Zynq_V24:4127";
	/* <S33>/Product */
	this.urlHashMap["Encoder_Zynq_V24:4128"] = "msg=&block=Encoder_Zynq_V24:4128";
	/* <S33>/hold */
	this.urlHashMap["Encoder_Zynq_V24:4129"] = "msg=&block=Encoder_Zynq_V24:4129";
	/* <S33>/hold1 */
	this.urlHashMap["Encoder_Zynq_V24:4130"] = "msg=&block=Encoder_Zynq_V24:4130";
	/* <S33>/hold2 */
	this.urlHashMap["Encoder_Zynq_V24:4131"] = "msg=&block=Encoder_Zynq_V24:4131";
	/* <S33>/hold3 */
	this.urlHashMap["Encoder_Zynq_V24:4132"] = "msg=&block=Encoder_Zynq_V24:4132";
	/* <S33>/hold4 */
	this.urlHashMap["Encoder_Zynq_V24:4133"] = "msg=&block=Encoder_Zynq_V24:4133";
	/* <S33>/OmegaOut */
	this.urlHashMap["Encoder_Zynq_V24:4134"] = "msg=&block=Encoder_Zynq_V24:4134";
	/* <S34>/Add1 */
	this.urlHashMap["Encoder_Zynq_V24:4170"] = "msg=&block=Encoder_Zynq_V24:4170";
	/* <S34>/Compare
To Constant */
	this.urlHashMap["Encoder_Zynq_V24:4196"] = "msg=&block=Encoder_Zynq_V24:4196";
	/* <S34>/Constant2 */
	this.urlHashMap["Encoder_Zynq_V24:4175"] = "msg=&block=Encoder_Zynq_V24:4175";
	/* <S34>/Constant3 */
	this.urlHashMap["Encoder_Zynq_V24:4198"] = "msg=&block=Encoder_Zynq_V24:4198";
	/* <S34>/Data Type Conversion2 */
	this.urlHashMap["Encoder_Zynq_V24:4177"] = "msg=&block=Encoder_Zynq_V24:4177";
	/* <S34>/Data Type Conversion3 */
	this.urlHashMap["Encoder_Zynq_V24:4183"] = "msg=&block=Encoder_Zynq_V24:4183";
	/* <S34>/Data Type Conversion4 */
	this.urlHashMap["Encoder_Zynq_V24:4193"] = "msg=&block=Encoder_Zynq_V24:4193";
	/* <S34>/Default */
	this.urlHashMap["Encoder_Zynq_V24:4148"] = "msg=&block=Encoder_Zynq_V24:4148";
	/* <S34>/Delay1 */
	this.urlHashMap["Encoder_Zynq_V24:4171"] = "msg=&block=Encoder_Zynq_V24:4171";
	/* <S34>/Delay2 */
	this.urlHashMap["Encoder_Zynq_V24:4136"] = "msg=&block=Encoder_Zynq_V24:4136";
	/* <S34>/Delay3 */
	this.urlHashMap["Encoder_Zynq_V24:4189"] = "msg=&block=Encoder_Zynq_V24:4189";
	/* <S34>/Delay4 */
	this.urlHashMap["Encoder_Zynq_V24:4191"] = "msg=&block=Encoder_Zynq_V24:4191";
	/* <S34>/Delay48 */
	this.urlHashMap["Encoder_Zynq_V24:4137"] = "msg=&block=Encoder_Zynq_V24:4137";
	/* <S34>/Delay49 */
	this.urlHashMap["Encoder_Zynq_V24:4138"] = "msg=&block=Encoder_Zynq_V24:4138";
	/* <S34>/Delay5 */
	this.urlHashMap["Encoder_Zynq_V24:4139"] = "msg=&block=Encoder_Zynq_V24:4139";
	/* <S34>/DelayComp_i1 */
	this.urlHashMap["Encoder_Zynq_V24:4140"] = "msg=&block=Encoder_Zynq_V24:4140";
	/* <S34>/DelayToCompensate_Divide */
	this.urlHashMap["Encoder_Zynq_V24:4176"] = "msg=&block=Encoder_Zynq_V24:4176";
	/* <S34>/Delta_i1 */
	this.urlHashMap["Encoder_Zynq_V24:4141"] = "msg=&block=Encoder_Zynq_V24:4141";
	/* <S34>/Div_i1 */
	this.urlHashMap["Encoder_Zynq_V24:4142"] = "msg=&block=Encoder_Zynq_V24:4142";
	/* <S34>/IfFinalState */
	this.urlHashMap["Encoder_Zynq_V24:4173"] = "msg=&block=Encoder_Zynq_V24:4173";
	/* <S34>/Inverse0 */
	this.urlHashMap["Encoder_Zynq_V24:4149"] = "msg=&block=Encoder_Zynq_V24:4149";
	/* <S34>/Inverse1 */
	this.urlHashMap["Encoder_Zynq_V24:4150"] = "msg=&block=Encoder_Zynq_V24:4150";
	/* <S34>/Inverse10 */
	this.urlHashMap["Encoder_Zynq_V24:4151"] = "msg=&block=Encoder_Zynq_V24:4151";
	/* <S34>/Inverse11 */
	this.urlHashMap["Encoder_Zynq_V24:4152"] = "msg=&block=Encoder_Zynq_V24:4152";
	/* <S34>/Inverse12 */
	this.urlHashMap["Encoder_Zynq_V24:4153"] = "msg=&block=Encoder_Zynq_V24:4153";
	/* <S34>/Inverse13 */
	this.urlHashMap["Encoder_Zynq_V24:4154"] = "msg=&block=Encoder_Zynq_V24:4154";
	/* <S34>/Inverse14 */
	this.urlHashMap["Encoder_Zynq_V24:4155"] = "msg=&block=Encoder_Zynq_V24:4155";
	/* <S34>/Inverse15 */
	this.urlHashMap["Encoder_Zynq_V24:4156"] = "msg=&block=Encoder_Zynq_V24:4156";
	/* <S34>/Inverse16 */
	this.urlHashMap["Encoder_Zynq_V24:4157"] = "msg=&block=Encoder_Zynq_V24:4157";
	/* <S34>/Inverse17 */
	this.urlHashMap["Encoder_Zynq_V24:4158"] = "msg=&block=Encoder_Zynq_V24:4158";
	/* <S34>/Inverse18 */
	this.urlHashMap["Encoder_Zynq_V24:4159"] = "msg=&block=Encoder_Zynq_V24:4159";
	/* <S34>/Inverse19 */
	this.urlHashMap["Encoder_Zynq_V24:4160"] = "msg=&block=Encoder_Zynq_V24:4160";
	/* <S34>/Inverse2 */
	this.urlHashMap["Encoder_Zynq_V24:4161"] = "msg=&block=Encoder_Zynq_V24:4161";
	/* <S34>/Inverse3 */
	this.urlHashMap["Encoder_Zynq_V24:4162"] = "msg=&block=Encoder_Zynq_V24:4162";
	/* <S34>/Inverse4 */
	this.urlHashMap["Encoder_Zynq_V24:4163"] = "msg=&block=Encoder_Zynq_V24:4163";
	/* <S34>/Inverse5 */
	this.urlHashMap["Encoder_Zynq_V24:4164"] = "msg=&block=Encoder_Zynq_V24:4164";
	/* <S34>/Inverse6 */
	this.urlHashMap["Encoder_Zynq_V24:4165"] = "msg=&block=Encoder_Zynq_V24:4165";
	/* <S34>/Inverse7 */
	this.urlHashMap["Encoder_Zynq_V24:4166"] = "msg=&block=Encoder_Zynq_V24:4166";
	/* <S34>/Inverse8 */
	this.urlHashMap["Encoder_Zynq_V24:4167"] = "msg=&block=Encoder_Zynq_V24:4167";
	/* <S34>/Inverse9 */
	this.urlHashMap["Encoder_Zynq_V24:4168"] = "msg=&block=Encoder_Zynq_V24:4168";
	/* <S34>/Reset */
	this.urlHashMap["Encoder_Zynq_V24:4172"] = "msg=&block=Encoder_Zynq_V24:4172";
	/* <S34>/Reset1 */
	this.urlHashMap["Encoder_Zynq_V24:4190"] = "msg=&block=Encoder_Zynq_V24:4190";
	/* <S34>/Reset_omega_old_with_0 */
	this.urlHashMap["Encoder_Zynq_V24:4186"] = "msg=&block=Encoder_Zynq_V24:4186";
	/* <S34>/Scope1 */
	this.urlHashMap["Encoder_Zynq_V24:4178"] = "msg=&block=Encoder_Zynq_V24:4178";
	/* <S34>/Scope2 */
	this.urlHashMap["Encoder_Zynq_V24:4187"] = "msg=&block=Encoder_Zynq_V24:4187";
	/* <S34>/Switch_idx3 */
	this.urlHashMap["Encoder_Zynq_V24:4169"] = "msg=&block=Encoder_Zynq_V24:4169";
	/* <S34>/Switch_on2 */
	this.urlHashMap["Encoder_Zynq_V24:4143"] = "msg=&block=Encoder_Zynq_V24:4143";
	/* <S34>/Switch_on4 */
	this.urlHashMap["Encoder_Zynq_V24:4185"] = "msg=&block=Encoder_Zynq_V24:4185";
	/* <S34>/Switch_on5 */
	this.urlHashMap["Encoder_Zynq_V24:4145"] = "msg=&block=Encoder_Zynq_V24:4145";
	/* <S34>/m_act_i1 */
	this.urlHashMap["Encoder_Zynq_V24:4146"] = "msg=&block=Encoder_Zynq_V24:4146";
	/* <S34>/omega->n1 */
	this.urlHashMap["Encoder_Zynq_V24:4184"] = "msg=&block=Encoder_Zynq_V24:4184";
	/* <S34>/omega->n2 */
	this.urlHashMap["Encoder_Zynq_V24:4194"] = "msg=&block=Encoder_Zynq_V24:4194";
	/* <S35>/Add */
	this.urlHashMap["Encoder_Zynq_V24:2055"] = "IncreEncoder_V24_ip_src_Check_Direction.vhd:82,83,84";
	/* <S35>/Add1 */
	this.urlHashMap["Encoder_Zynq_V24:2056"] = "IncreEncoder_V24_ip_src_Check_Direction.vhd:117,118,119";
	/* <S35>/Add2 */
	this.urlHashMap["Encoder_Zynq_V24:2057"] = "IncreEncoder_V24_ip_src_Check_Direction.vhd:150";
	/* <S35>/Change */
	this.urlHashMap["Encoder_Zynq_V24:2058"] = "IncreEncoder_V24_ip_src_Check_Direction.vhd:136";
	/* <S35>/Change1 */
	this.urlHashMap["Encoder_Zynq_V24:2059"] = "IncreEncoder_V24_ip_src_Check_Direction.vhd:138,139,140,141,142,143,144,145,146,147";
	/* <S35>/Save_Direction */
	this.urlHashMap["Encoder_Zynq_V24:2060"] = "IncreEncoder_V24_ip_src_Check_Direction.vhd:165,166";
	/* <S35>/T_count_switch */
	this.urlHashMap["Encoder_Zynq_V24:2062"] = "IncreEncoder_V24_ip_src_Check_Direction.vhd:98,99";
	/* <S35>/TrnLeft */
	this.urlHashMap["Encoder_Zynq_V24:2063"] = "IncreEncoder_V24_ip_src_Check_Direction.vhd:121,122";
	/* <S35>/TrnRight */
	this.urlHashMap["Encoder_Zynq_V24:2064"] = "IncreEncoder_V24_ip_src_Check_Direction.vhd:86,87";
	/* <S35>/TurnLeft */
	this.urlHashMap["Encoder_Zynq_V24:2065"] = "IncreEncoder_V24_ip_src_Check_Direction.vhd:130";
	/* <S35>/TurnRight */
	this.urlHashMap["Encoder_Zynq_V24:2066"] = "IncreEncoder_V24_ip_src_Check_Direction.vhd:95";
	/* <S35>/count_old */
	this.urlHashMap["Encoder_Zynq_V24:2067"] = "IncreEncoder_V24_ip_src_Check_Direction.vhd:70,71,72,73,74,75,76,77,78,79";
	/* <S35>/count_old1 */
	this.urlHashMap["Encoder_Zynq_V24:2068"] = "IncreEncoder_V24_ip_src_Check_Direction.vhd:105,106,107,108,109,110,111,112,113,114";
	/* <S35>/count_old2 */
	this.urlHashMap["Encoder_Zynq_V24:2069"] = "IncreEncoder_V24_ip_src_Check_Direction.vhd:168,169,170,171,172,173,174,175,176,177";
	/* <S35>/count_old_switch */
	this.urlHashMap["Encoder_Zynq_V24:2070"] = "IncreEncoder_V24_ip_src_Check_Direction.vhd:67,68";
	/* <S35>/count_old_switch1 */
	this.urlHashMap["Encoder_Zynq_V24:2071"] = "IncreEncoder_V24_ip_src_Check_Direction.vhd:102,103";
	/* <S35>/new_value */
	this.urlHashMap["Encoder_Zynq_V24:2072"] = "IncreEncoder_V24_ip_src_Check_Direction.vhd:152,153,154,155,156,157,158,159,160,161";
	/* <S35>/nothing */
	this.urlHashMap["Encoder_Zynq_V24:2073"] = "IncreEncoder_V24_ip_src_Check_Direction.vhd:128";
	/* <S35>/nothing1 */
	this.urlHashMap["Encoder_Zynq_V24:2074"] = "IncreEncoder_V24_ip_src_Check_Direction.vhd:93";
	/* <S35>/speed_old_switch */
	this.urlHashMap["Encoder_Zynq_V24:2075"] = "IncreEncoder_V24_ip_src_Check_Direction.vhd:133,134";
	/* <S36>/Detec_edge */
	this.urlHashMap["Encoder_Zynq_V24:3606"] = "IncreEncoder_V24_ip_src_Counter.vhd:69,70,71";
	/* <S36>/Reset_flag */
	this.urlHashMap["Encoder_Zynq_V24:3609"] = "IncreEncoder_V24_ip_src_Counter.vhd:91,92,93";
	/* <S36>/reset_old */
	this.urlHashMap["Encoder_Zynq_V24:3614"] = "IncreEncoder_V24_ip_src_Counter.vhd:79,80,81,82,83,84,85,86,87,88";
	/* <S36>/reset_old1 */
	this.urlHashMap["Encoder_Zynq_V24:3615"] = "IncreEncoder_V24_ip_src_Counter.vhd:57,58,59,60,61,62,63,64,65,66";
	/* <S36>/reset_old_switch */
	this.urlHashMap["Encoder_Zynq_V24:3620"] = "IncreEncoder_V24_ip_src_Counter.vhd:76,77";
	/* <S36>/reset_old_switch1 */
	this.urlHashMap["Encoder_Zynq_V24:3621"] = "IncreEncoder_V24_ip_src_Counter.vhd:54,55";
	/* <S37>/Add1 */
	this.urlHashMap["Encoder_Zynq_V24:2114"] = "IncreEncoder_V24_ip_src_Counter_position.vhd:160,161";
	/* <S37>/Constant1 */
	this.urlHashMap["Encoder_Zynq_V24:2115"] = "IncreEncoder_V24_ip_src_Counter_position.vhd:108";
	/* <S37>/Constant2 */
	this.urlHashMap["Encoder_Zynq_V24:2116"] = "IncreEncoder_V24_ip_src_Counter_position.vhd:122";
	/* <S37>/Constant3 */
	this.urlHashMap["Encoder_Zynq_V24:2117"] = "IncreEncoder_V24_ip_src_Counter_position.vhd:114";
	/* <S37>/Constant5 */
	this.urlHashMap["Encoder_Zynq_V24:2119"] = "IncreEncoder_V24_ip_src_Counter_position.vhd:120";
	/* <S37>/Relational
Operator */
	this.urlHashMap["Encoder_Zynq_V24:2121"] = "IncreEncoder_V24_ip_src_Counter_position.vhd:196,197";
	/* <S37>/Relational
Operator1 */
	this.urlHashMap["Encoder_Zynq_V24:2122"] = "IncreEncoder_V24_ip_src_Counter_position.vhd:168,169";
	/* <S37>/Resett_switch1 */
	this.urlHashMap["Encoder_Zynq_V24:2125"] = "IncreEncoder_V24_ip_src_Counter_position.vhd:212,213";
	/* <S37>/Resett_switch2 */
	this.urlHashMap["Encoder_Zynq_V24:2126"] = "IncreEncoder_V24_ip_src_Counter_position.vhd:164,165";
	/* <S37>/Resett_switch3 */
	this.urlHashMap["Encoder_Zynq_V24:2127"] = "IncreEncoder_V24_ip_src_Counter_position.vhd:190,191";
	/* <S37>/only_use_for_neg_speed */
	this.urlHashMap["Encoder_Zynq_V24:2128"] = "IncreEncoder_V24_ip_src_Counter_position.vhd:186,187";
	/* <S37>/reset_old1 */
	this.urlHashMap["Encoder_Zynq_V24:4052"] = "IncreEncoder_V24_ip_src_Counter_position.vhd:148,149,150,151,152,153,154,155,156,157";
	/* <S37>/reset_old2 */
	this.urlHashMap["Encoder_Zynq_V24:2131"] = "IncreEncoder_V24_ip_src_Counter_position.vhd:171,172,173,174,175,176,177,178,179,180";
	/* <S37>/reset_old3 */
	this.urlHashMap["Encoder_Zynq_V24:2132"] = "IncreEncoder_V24_ip_src_Counter_position.vhd:199,200,201,202,203,204,205,206,207,208";
	/* <S37>/reset_old4 */
	this.urlHashMap["Encoder_Zynq_V24:2133"] = "IncreEncoder_V24_ip_src_Counter_position.vhd:128,129,130,131,132,133,134,135,136,137";
	/* <S37>/reset_old5 */
	this.urlHashMap["Encoder_Zynq_V24:2134"] = "IncreEncoder_V24_ip_src_Counter_position.vhd:78,79,80,81,82,83,84,85,86,87";
	/* <S37>/reset_old6 */
	this.urlHashMap["Encoder_Zynq_V24:2135"] = "IncreEncoder_V24_ip_src_Counter_position.vhd:92,93,94,95,96,97,98,99,100,101";
	/* <S37>/speed_old_switch1 */
	this.urlHashMap["Encoder_Zynq_V24:2138"] = "IncreEncoder_V24_ip_src_Counter_position.vhd:141,142";
	/* <S37>/speed_old_switch2 */
	this.urlHashMap["Encoder_Zynq_V24:2139"] = "IncreEncoder_V24_ip_src_Counter_position.vhd:125,126";
	/* <S38>/Add1 */
	this.urlHashMap["Encoder_Zynq_V24:2154"] = "IncreEncoder_V24_ip_src_Counter_theta_ele.vhd:167";
	/* <S38>/Constant1 */
	this.urlHashMap["Encoder_Zynq_V24:2155"] = "IncreEncoder_V24_ip_src_Counter_theta_ele.vhd:117";
	/* <S38>/Constant2 */
	this.urlHashMap["Encoder_Zynq_V24:2156"] = "IncreEncoder_V24_ip_src_Counter_theta_ele.vhd:131";
	/* <S38>/Constant3 */
	this.urlHashMap["Encoder_Zynq_V24:2157"] = "IncreEncoder_V24_ip_src_Counter_theta_ele.vhd:123";
	/* <S38>/Constant5 */
	this.urlHashMap["Encoder_Zynq_V24:2158"] = "IncreEncoder_V24_ip_src_Counter_theta_ele.vhd:129";
	/* <S38>/Relational
Operator */
	this.urlHashMap["Encoder_Zynq_V24:2160"] = "IncreEncoder_V24_ip_src_Counter_theta_ele.vhd:199,201,202";
	/* <S38>/Relational
Operator1 */
	this.urlHashMap["Encoder_Zynq_V24:2161"] = "IncreEncoder_V24_ip_src_Counter_theta_ele.vhd:174,175";
	/* <S38>/Reset_switch3 */
	this.urlHashMap["Encoder_Zynq_V24:2165"] = "IncreEncoder_V24_ip_src_Counter_theta_ele.vhd:196,197";
	/* <S38>/Resett_switch1 */
	this.urlHashMap["Encoder_Zynq_V24:2163"] = "IncreEncoder_V24_ip_src_Counter_theta_ele.vhd:217,218";
	/* <S38>/Resett_switch2 */
	this.urlHashMap["Encoder_Zynq_V24:2164"] = "IncreEncoder_V24_ip_src_Counter_theta_ele.vhd:170,171";
	/* <S38>/only_use_for_neg_speed */
	this.urlHashMap["Encoder_Zynq_V24:2166"] = "IncreEncoder_V24_ip_src_Counter_theta_ele.vhd:192,193";
	/* <S38>/reset_old1 */
	this.urlHashMap["Encoder_Zynq_V24:4050"] = "IncreEncoder_V24_ip_src_Counter_theta_ele.vhd:87,88,89,90,91,92,93,94,95,96";
	/* <S38>/reset_old2 */
	this.urlHashMap["Encoder_Zynq_V24:2169"] = "IncreEncoder_V24_ip_src_Counter_theta_ele.vhd:177,178,179,180,181,182,183,184,185,186";
	/* <S38>/reset_old3 */
	this.urlHashMap["Encoder_Zynq_V24:2170"] = "IncreEncoder_V24_ip_src_Counter_theta_ele.vhd:204,205,206,207,208,209,210,211,212,213";
	/* <S38>/reset_old4 */
	this.urlHashMap["Encoder_Zynq_V24:2171"] = "IncreEncoder_V24_ip_src_Counter_theta_ele.vhd:137,138,139,140,141,142,143,144,145,146";
	/* <S38>/reset_old5 */
	this.urlHashMap["Encoder_Zynq_V24:2172"] = "IncreEncoder_V24_ip_src_Counter_theta_ele.vhd:101,102,103,104,105,106,107,108,109,110";
	/* <S38>/reset_old6 */
	this.urlHashMap["Encoder_Zynq_V24:4053"] = "IncreEncoder_V24_ip_src_Counter_theta_ele.vhd:155,156,157,158,159,160,161,162,163,164";
	/* <S38>/speed_old_switch1 */
	this.urlHashMap["Encoder_Zynq_V24:2175"] = "IncreEncoder_V24_ip_src_Counter_theta_ele.vhd:150,151";
	/* <S38>/speed_old_switch2 */
	this.urlHashMap["Encoder_Zynq_V24:2176"] = "IncreEncoder_V24_ip_src_Counter_theta_ele.vhd:134,135";
	/* <S39>/Add1 */
	this.urlHashMap["Encoder_Zynq_V24:3744"] = "IncreEncoder_V24_ip_src_omega_by_count_lines.vhd:111";
	/* <S39>/Constant1 */
	this.urlHashMap["Encoder_Zynq_V24:3745"] = "IncreEncoder_V24_ip_src_omega_by_count_lines.vhd:109";
	/* <S39>/Constant2 */
	this.urlHashMap["Encoder_Zynq_V24:3746"] = "IncreEncoder_V24_ip_src_omega_by_count_lines.vhd:91";
	/* <S39>/Constant3 */
	this.urlHashMap["Encoder_Zynq_V24:3747"] = "IncreEncoder_V24_ip_src_omega_by_count_lines.vhd:89";
	/* <S39>/Constant4 */
	this.urlHashMap["Encoder_Zynq_V24:3748"] = "IncreEncoder_V24_ip_src_omega_by_count_lines.vhd:67";
	/* <S39>/Relational
Operator */
	this.urlHashMap["Encoder_Zynq_V24:3750"] = "IncreEncoder_V24_ip_src_omega_by_count_lines.vhd:118,119";
	/* <S39>/Reset_when_overflow */
	this.urlHashMap["Encoder_Zynq_V24:3752"] = "IncreEncoder_V24_ip_src_omega_by_count_lines.vhd:71,72,73";
	/* <S39>/Resett_switch1 */
	this.urlHashMap["Encoder_Zynq_V24:3753"] = "IncreEncoder_V24_ip_src_omega_by_count_lines.vhd:122,123";
	/* <S39>/Resett_switch2 */
	this.urlHashMap["Encoder_Zynq_V24:3754"] = "IncreEncoder_V24_ip_src_omega_by_count_lines.vhd:114,115";
	/* <S39>/reset_old1 */
	this.urlHashMap["Encoder_Zynq_V24:3780"] = "IncreEncoder_V24_ip_src_omega_by_count_lines.vhd:137,138,139,140,141,142,143,144,145,146";
	/* <S39>/reset_old3 */
	this.urlHashMap["Encoder_Zynq_V24:3758"] = "IncreEncoder_V24_ip_src_omega_by_count_lines.vhd:125,126,127,128,129,130,131,132,133,134";
	/* <S39>/reset_old4 */
	this.urlHashMap["Encoder_Zynq_V24:3759"] = "IncreEncoder_V24_ip_src_omega_by_count_lines.vhd:93,94,95,96,97,98,99,100,101,102";
	/* <S39>/reset_old5 */
	this.urlHashMap["Encoder_Zynq_V24:3760"] = "IncreEncoder_V24_ip_src_omega_by_count_lines.vhd:75,76,77,78,79,80,81,82,83,84";
	/* <S39>/reset_old_switch1 */
	this.urlHashMap["Encoder_Zynq_V24:3781"] = "IncreEncoder_V24_ip_src_omega_by_count_lines.vhd:150,151";
	/* <S39>/speed_old_switch1 */
	this.urlHashMap["Encoder_Zynq_V24:3762"] = "IncreEncoder_V24_ip_src_omega_by_count_lines.vhd:106,107";
	/* <S40>/ReadBack_Omega */
	this.urlHashMap["Encoder_Zynq_V24:4646"] = "msg=&block=Encoder_Zynq_V24:4646";
	/* <S40>/OverSamplOmega */
	this.urlHashMap["Encoder_Zynq_V24:4650"] = "msg=&block=Encoder_Zynq_V24:4650";
	/* <S40>/ABS */
	this.urlHashMap["Encoder_Zynq_V24:4097"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:390,392,393,394";
	/* <S40>/Abs */
	this.urlHashMap["Encoder_Zynq_V24:4676"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:224,226,227,228";
	/* <S40>/Add1 */
	this.urlHashMap["Encoder_Zynq_V24:2199"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:346";
	/* <S40>/Add2 */
	this.urlHashMap["Encoder_Zynq_V24:4096"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:368";
	/* <S40>/Add3 */
	this.urlHashMap["Encoder_Zynq_V24:4235"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:308,309";
	/* <S40>/Constant */
	this.urlHashMap["Encoder_Zynq_V24:2200"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:344";
	/* <S40>/Constant1 */
	this.urlHashMap["Encoder_Zynq_V24:2201"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:340";
	/* <S40>/Constant2 */
	this.urlHashMap["Encoder_Zynq_V24:2202"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:428";
	/* <S40>/Constant3 */
	this.urlHashMap["Encoder_Zynq_V24:3558"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:194";
	/* <S40>/Constant4 */
	this.urlHashMap["Encoder_Zynq_V24:4237"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:202";
	/* <S40>/Constant5 */
	this.urlHashMap["Encoder_Zynq_V24:4238"] = "msg=&block=Encoder_Zynq_V24:4238";
	/* <S40>/Count */
	this.urlHashMap["Encoder_Zynq_V24:4666"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:152,153,154,155,156,157,158,159,160";
	/* <S40>/Data Type Conversion1 */
	this.urlHashMap["Encoder_Zynq_V24:4672"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:283,284";
	/* <S40>/Delay1 */
	this.urlHashMap["Encoder_Zynq_V24:4239"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:311,312,313,314,315,316,317,318,319,320";
	/* <S40>/Delay2 */
	this.urlHashMap["Encoder_Zynq_V24:4652"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:290,291,292,293,294,295,296,297,298,299";
	/* <S40>/Delay3 */
	this.urlHashMap["Encoder_Zynq_V24:5053"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:256,257,258,259,260,261,262,263,264,265";
	/* <S40>/Delay4 */
	this.urlHashMap["Encoder_Zynq_V24:4674"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:436,437,438,439,440,441,442,443,444,445";
	/* <S40>/Delay5 */
	this.urlHashMap["Encoder_Zynq_V24:5054"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:230,231,232,233,234,235,236,237,238,239";
	/* <S40>/Delay6 */
	this.urlHashMap["Encoder_Zynq_V24:4667"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:244,245,246,247,248,249,250,251,252,253";
	/* <S40>/Delay7 */
	this.urlHashMap["Encoder_Zynq_V24:4668"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:270,271,272,273,274,275,276,277,278,279";
	/* <S40>/Delay8 */
	this.urlHashMap["Encoder_Zynq_V24:4670"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:290,291,292,293,294,295,296,297,298,299";
	/* <S40>/Detec_edge */
	this.urlHashMap["Encoder_Zynq_V24:2088"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:178,179,180";
	/* <S40>/IfFinalState */
	this.urlHashMap["Encoder_Zynq_V24:4240"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:323,325,326";
	/* <S40>/OnlyAllowInNewPeriod */
	this.urlHashMap["Encoder_Zynq_V24:4651"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:287,288";
	/* <S40>/Pipeline */
	this.urlHashMap["Encoder_Zynq_V24:4098"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:378,379,380,381,382,383,384,385,386,387";
	/* <S40>/Pipeline1 */
	this.urlHashMap["Encoder_Zynq_V24:4099"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:396,397,398,399,400,401,402,403,404,405";
	/* <S40>/ProtectOverflow_T_count */
	this.urlHashMap["Encoder_Zynq_V24:2205"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:353,354";
	/* <S40>/ProtectOverflow_rps */
	this.urlHashMap["Encoder_Zynq_V24:2206"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:431,432";
	/* <S40>/ProtectToUseOmegaIfThetaJump1 */
	this.urlHashMap["Encoder_Zynq_V24:3557"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:197,198";
	/* <S40>/Reset */
	this.urlHashMap["Encoder_Zynq_V24:4241"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:305,306";
	/* <S40>/Reset1 */
	this.urlHashMap["Encoder_Zynq_V24:4673"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:449,450";
	/* <S40>/Scope */
	this.urlHashMap["Encoder_Zynq_V24:3683"] = "msg=rtwMsg_notTraceable&block=Encoder_Zynq_V24:3683";
	/* <S40>/T_count */
	this.urlHashMap["Encoder_Zynq_V24:2207"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:356,357,358,359,360,361,362,363,364,365";
	/* <S40>/T_count_switch */
	this.urlHashMap["Encoder_Zynq_V24:2208"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:375,376";
	/* <S40>/count_old1 */
	this.urlHashMap["Encoder_Zynq_V24:3555"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:182,183,184,185,186,187,188,189,190,191";
	/* <S40>/reset_old1 */
	this.urlHashMap["Encoder_Zynq_V24:2095"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:166,167,168,169,170,171,172,173,174,175";
	/* <S40>/reset_old_switch1 */
	this.urlHashMap["Encoder_Zynq_V24:2099"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:163,164";
	/* <S40>/speed_old */
	this.urlHashMap["Encoder_Zynq_V24:2211"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:412,413,414,415,416,417,418,419,420,421";
	/* <S40>/speed_old_switch */
	this.urlHashMap["Encoder_Zynq_V24:2212"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:425,426";
	/* <S40>/speed_old_switch2 */
	this.urlHashMap["Encoder_Zynq_V24:4095"] = "IncreEncoder_V24_ip_src_omega_by_measure_time.vhd:371,372";
	/* <S40>/OverSamplingFactor */
	this.urlHashMap["Encoder_Zynq_V24:4644"] = "msg=&block=Encoder_Zynq_V24:4644";
	/* <S41>/Edge */
	this.urlHashMap["Encoder_Zynq_V24:4200"] = "msg=&block=Encoder_Zynq_V24:4200";
	/* <S41>/T */
	this.urlHashMap["Encoder_Zynq_V24:4201"] = "msg=&block=Encoder_Zynq_V24:4201";
	/* <S41>/Dir */
	this.urlHashMap["Encoder_Zynq_V24:4202"] = "msg=&block=Encoder_Zynq_V24:4202";
	/* <S41>/ABS */
	this.urlHashMap["Encoder_Zynq_V24:4203"] = "msg=&block=Encoder_Zynq_V24:4203";
	/* <S41>/Add1 */
	this.urlHashMap["Encoder_Zynq_V24:4204"] = "msg=&block=Encoder_Zynq_V24:4204";
	/* <S41>/Add2 */
	this.urlHashMap["Encoder_Zynq_V24:4205"] = "msg=&block=Encoder_Zynq_V24:4205";
	/* <S41>/Constant */
	this.urlHashMap["Encoder_Zynq_V24:4206"] = "msg=&block=Encoder_Zynq_V24:4206";
	/* <S41>/Constant1 */
	this.urlHashMap["Encoder_Zynq_V24:4207"] = "msg=&block=Encoder_Zynq_V24:4207";
	/* <S41>/Constant2 */
	this.urlHashMap["Encoder_Zynq_V24:4208"] = "msg=&block=Encoder_Zynq_V24:4208";
	/* <S41>/Constant3 */
	this.urlHashMap["Encoder_Zynq_V24:4209"] = "msg=&block=Encoder_Zynq_V24:4209";
	/* <S41>/Detec_edge */
	this.urlHashMap["Encoder_Zynq_V24:4210"] = "msg=&block=Encoder_Zynq_V24:4210";
	/* <S41>/Pipeline */
	this.urlHashMap["Encoder_Zynq_V24:4211"] = "msg=&block=Encoder_Zynq_V24:4211";
	/* <S41>/Pipeline1 */
	this.urlHashMap["Encoder_Zynq_V24:4212"] = "msg=&block=Encoder_Zynq_V24:4212";
	/* <S41>/ProtectOverflow_T_count */
	this.urlHashMap["Encoder_Zynq_V24:4213"] = "msg=&block=Encoder_Zynq_V24:4213";
	/* <S41>/ProtectOverflow_rps */
	this.urlHashMap["Encoder_Zynq_V24:4214"] = "msg=&block=Encoder_Zynq_V24:4214";
	/* <S41>/ProtectToUseOmegaIfThetaJump1 */
	this.urlHashMap["Encoder_Zynq_V24:4215"] = "msg=&block=Encoder_Zynq_V24:4215";
	/* <S41>/Scope */
	this.urlHashMap["Encoder_Zynq_V24:4216"] = "msg=&block=Encoder_Zynq_V24:4216";
	/* <S41>/T_count */
	this.urlHashMap["Encoder_Zynq_V24:4217"] = "msg=&block=Encoder_Zynq_V24:4217";
	/* <S41>/T_count_switch */
	this.urlHashMap["Encoder_Zynq_V24:4218"] = "msg=&block=Encoder_Zynq_V24:4218";
	/* <S41>/count_old1 */
	this.urlHashMap["Encoder_Zynq_V24:4219"] = "msg=&block=Encoder_Zynq_V24:4219";
	/* <S41>/reset_old1 */
	this.urlHashMap["Encoder_Zynq_V24:4220"] = "msg=&block=Encoder_Zynq_V24:4220";
	/* <S41>/reset_old_switch1 */
	this.urlHashMap["Encoder_Zynq_V24:4221"] = "msg=&block=Encoder_Zynq_V24:4221";
	/* <S41>/speed_old */
	this.urlHashMap["Encoder_Zynq_V24:4222"] = "msg=&block=Encoder_Zynq_V24:4222";
	/* <S41>/speed_old_switch */
	this.urlHashMap["Encoder_Zynq_V24:4223"] = "msg=&block=Encoder_Zynq_V24:4223";
	/* <S41>/speed_old_switch2 */
	this.urlHashMap["Encoder_Zynq_V24:4224"] = "msg=&block=Encoder_Zynq_V24:4224";
	/* <S41>/one_Omega */
	this.urlHashMap["Encoder_Zynq_V24:4225"] = "msg=&block=Encoder_Zynq_V24:4225";
	/* <S41>/NewMeasurement */
	this.urlHashMap["Encoder_Zynq_V24:4226"] = "msg=&block=Encoder_Zynq_V24:4226";
	/* <S42>/u */
	this.urlHashMap["Encoder_Zynq_V24:4196:1"] = "msg=&block=Encoder_Zynq_V24:4196:1";
	/* <S42>/Compare */
	this.urlHashMap["Encoder_Zynq_V24:4196:2"] = "msg=&block=Encoder_Zynq_V24:4196:2";
	/* <S42>/Constant */
	this.urlHashMap["Encoder_Zynq_V24:4196:3"] = "msg=&block=Encoder_Zynq_V24:4196:3";
	/* <S42>/y */
	this.urlHashMap["Encoder_Zynq_V24:4196:4"] = "msg=&block=Encoder_Zynq_V24:4196:4";
	/* <S43>:1 */
	this.urlHashMap["Encoder_Zynq_V24:4666:1"] = "IncreEncoder_V24_ip_src_Count.vhd:76";
	/* <S43>:1:20 */
	this.urlHashMap["Encoder_Zynq_V24:4666:1:20"] = "msg=&block=Encoder_Zynq_V24:4666:1:20";
	/* <S43>:1:21 */
	this.urlHashMap["Encoder_Zynq_V24:4666:1:21"] = "IncreEncoder_V24_ip_src_Count.vhd:86";
	/* <S43>:1:22 */
	this.urlHashMap["Encoder_Zynq_V24:4666:1:22"] = "IncreEncoder_V24_ip_src_Count.vhd:88";
	/* <S43>:1:23 */
	this.urlHashMap["Encoder_Zynq_V24:4666:1:23"] = "msg=&block=Encoder_Zynq_V24:4666:1:23";
	/* <S43>:1:24 */
	this.urlHashMap["Encoder_Zynq_V24:4666:1:24"] = "IncreEncoder_V24_ip_src_Count.vhd:95";
	/* <S43>:1:25 */
	this.urlHashMap["Encoder_Zynq_V24:4666:1:25"] = "IncreEncoder_V24_ip_src_Count.vhd:97";
	/* <S43>:1:27 */
	this.urlHashMap["Encoder_Zynq_V24:4666:1:27"] = "IncreEncoder_V24_ip_src_Count.vhd:99";
	/* <S43>:1:28 */
	this.urlHashMap["Encoder_Zynq_V24:4666:1:28"] = "IncreEncoder_V24_ip_src_Count.vhd:100";
	/* <S43>:1:32 */
	this.urlHashMap["Encoder_Zynq_V24:4666:1:32"] = "msg=&block=Encoder_Zynq_V24:4666:1:32";
	/* <S43>:1:33 */
	this.urlHashMap["Encoder_Zynq_V24:4666:1:33"] = "msg=&block=Encoder_Zynq_V24:4666:1:33";
	this.getUrlHash = function(sid) { return this.urlHashMap[sid];}
}
RTW_Sid2UrlHash.instance = new RTW_Sid2UrlHash();
function RTW_rtwnameSIDMap() {
	this.rtwnameHashMap = new Array();
	this.sidHashMap = new Array();
	this.rtwnameHashMap["<Root>"] = {sid: "Encoder_Zynq_V24"};
	this.sidHashMap["Encoder_Zynq_V24"] = {rtwname: "<Root>"};
	this.rtwnameHashMap["<S5>/A"] = {sid: "Encoder_Zynq_V24:2043"};
	this.sidHashMap["Encoder_Zynq_V24:2043"] = {rtwname: "<S5>/A"};
	this.rtwnameHashMap["<S5>/B"] = {sid: "Encoder_Zynq_V24:2044"};
	this.sidHashMap["Encoder_Zynq_V24:2044"] = {rtwname: "<S5>/B"};
	this.rtwnameHashMap["<S5>/I"] = {sid: "Encoder_Zynq_V24:2045"};
	this.sidHashMap["Encoder_Zynq_V24:2045"] = {rtwname: "<S5>/I"};
	this.rtwnameHashMap["<S5>/PI2_Inc_AXI4"] = {sid: "Encoder_Zynq_V24:2049"};
	this.sidHashMap["Encoder_Zynq_V24:2049"] = {rtwname: "<S5>/PI2_Inc_AXI4"};
	this.rtwnameHashMap["<S5>/Timer_FPGA_ms_AXI4"] = {sid: "Encoder_Zynq_V24:3105"};
	this.sidHashMap["Encoder_Zynq_V24:3105"] = {rtwname: "<S5>/Timer_FPGA_ms_AXI4"};
	this.rtwnameHashMap["<S5>/IncPerTurn_mech_AXI4"] = {sid: "Encoder_Zynq_V24:3106"};
	this.sidHashMap["Encoder_Zynq_V24:3106"] = {rtwname: "<S5>/IncPerTurn_mech_AXI4"};
	this.rtwnameHashMap["<S5>/IncPerTurn_elek_AXI4"] = {sid: "Encoder_Zynq_V24:3108"};
	this.sidHashMap["Encoder_Zynq_V24:3108"] = {rtwname: "<S5>/IncPerTurn_elek_AXI4"};
	this.rtwnameHashMap["<S5>/OmegaPerOverSampl_AXI4"] = {sid: "Encoder_Zynq_V24:3645"};
	this.sidHashMap["Encoder_Zynq_V24:3645"] = {rtwname: "<S5>/OmegaPerOverSampl_AXI4"};
	this.rtwnameHashMap["<S5>/PeriodEnd"] = {sid: "Encoder_Zynq_V24:3737"};
	this.sidHashMap["Encoder_Zynq_V24:3737"] = {rtwname: "<S5>/PeriodEnd"};
	this.rtwnameHashMap["<S5>/1//rps->1//omega"] = {sid: "Encoder_Zynq_V24:3305"};
	this.sidHashMap["Encoder_Zynq_V24:3305"] = {rtwname: "<S5>/1//rps->1//omega"};
	this.rtwnameHashMap["<S5>/Abs"] = {sid: "Encoder_Zynq_V24:4013"};
	this.sidHashMap["Encoder_Zynq_V24:4013"] = {rtwname: "<S5>/Abs"};
	this.rtwnameHashMap["<S5>/Average"] = {sid: "Encoder_Zynq_V24:4111"};
	this.sidHashMap["Encoder_Zynq_V24:4111"] = {rtwname: "<S5>/Average"};
	this.rtwnameHashMap["<S5>/Average_linear_Reg"] = {sid: "Encoder_Zynq_V24:3649"};
	this.sidHashMap["Encoder_Zynq_V24:3649"] = {rtwname: "<S5>/Average_linear_Reg"};
	this.rtwnameHashMap["<S5>/Check_Direction"] = {sid: "Encoder_Zynq_V24:2052"};
	this.sidHashMap["Encoder_Zynq_V24:2052"] = {rtwname: "<S5>/Check_Direction"};
	this.rtwnameHashMap["<S5>/Counter"] = {sid: "Encoder_Zynq_V24:3596"};
	this.sidHashMap["Encoder_Zynq_V24:3596"] = {rtwname: "<S5>/Counter"};
	this.rtwnameHashMap["<S5>/Counter_position"] = {sid: "Encoder_Zynq_V24:2109"};
	this.sidHashMap["Encoder_Zynq_V24:2109"] = {rtwname: "<S5>/Counter_position"};
	this.rtwnameHashMap["<S5>/Counter_theta_ele"] = {sid: "Encoder_Zynq_V24:2149"};
	this.sidHashMap["Encoder_Zynq_V24:2149"] = {rtwname: "<S5>/Counter_theta_ele"};
	this.rtwnameHashMap["<S5>/Data Type Conversion1"] = {sid: "Encoder_Zynq_V24:4107"};
	this.sidHashMap["Encoder_Zynq_V24:4107"] = {rtwname: "<S5>/Data Type Conversion1"};
	this.rtwnameHashMap["<S5>/Data Type Conversion2"] = {sid: "Encoder_Zynq_V24:3686"};
	this.sidHashMap["Encoder_Zynq_V24:3686"] = {rtwname: "<S5>/Data Type Conversion2"};
	this.rtwnameHashMap["<S5>/Data Type Conversion3"] = {sid: "Encoder_Zynq_V24:4106"};
	this.sidHashMap["Encoder_Zynq_V24:4106"] = {rtwname: "<S5>/Data Type Conversion3"};
	this.rtwnameHashMap["<S5>/Data Type Conversion4"] = {sid: "Encoder_Zynq_V24:4680"};
	this.sidHashMap["Encoder_Zynq_V24:4680"] = {rtwname: "<S5>/Data Type Conversion4"};
	this.rtwnameHashMap["<S5>/Data Type Conversion9"] = {sid: "Encoder_Zynq_V24:2187"};
	this.sidHashMap["Encoder_Zynq_V24:2187"] = {rtwname: "<S5>/Data Type Conversion9"};
	this.rtwnameHashMap["<S5>/Div_i1"] = {sid: "Encoder_Zynq_V24:4671"};
	this.sidHashMap["Encoder_Zynq_V24:4671"] = {rtwname: "<S5>/Div_i1"};
	this.rtwnameHashMap["<S5>/Divide"] = {sid: "Encoder_Zynq_V24:3721"};
	this.sidHashMap["Encoder_Zynq_V24:3721"] = {rtwname: "<S5>/Divide"};
	this.rtwnameHashMap["<S5>/HDL Reciprocal"] = {sid: "Encoder_Zynq_V24:3684"};
	this.sidHashMap["Encoder_Zynq_V24:3684"] = {rtwname: "<S5>/HDL Reciprocal"};
	this.rtwnameHashMap["<S5>/Logical1"] = {sid: "Encoder_Zynq_V24:2189"};
	this.sidHashMap["Encoder_Zynq_V24:2189"] = {rtwname: "<S5>/Logical1"};
	this.rtwnameHashMap["<S5>/Logical2"] = {sid: "Encoder_Zynq_V24:2348"};
	this.sidHashMap["Encoder_Zynq_V24:2348"] = {rtwname: "<S5>/Logical2"};
	this.rtwnameHashMap["<S5>/Pipeline1"] = {sid: "Encoder_Zynq_V24:4079"};
	this.sidHashMap["Encoder_Zynq_V24:4079"] = {rtwname: "<S5>/Pipeline1"};
	this.rtwnameHashMap["<S5>/Pipeline2"] = {sid: "Encoder_Zynq_V24:4081"};
	this.sidHashMap["Encoder_Zynq_V24:4081"] = {rtwname: "<S5>/Pipeline2"};
	this.rtwnameHashMap["<S5>/Pipeline3"] = {sid: "Encoder_Zynq_V24:4082"};
	this.sidHashMap["Encoder_Zynq_V24:4082"] = {rtwname: "<S5>/Pipeline3"};
	this.rtwnameHashMap["<S5>/Pipeline4"] = {sid: "Encoder_Zynq_V24:4083"};
	this.sidHashMap["Encoder_Zynq_V24:4083"] = {rtwname: "<S5>/Pipeline4"};
	this.rtwnameHashMap["<S5>/Pipeline5"] = {sid: "Encoder_Zynq_V24:5056"};
	this.sidHashMap["Encoder_Zynq_V24:5056"] = {rtwname: "<S5>/Pipeline5"};
	this.rtwnameHashMap["<S5>/Pipeline6"] = {sid: "Encoder_Zynq_V24:4086"};
	this.sidHashMap["Encoder_Zynq_V24:4086"] = {rtwname: "<S5>/Pipeline6"};
	this.rtwnameHashMap["<S5>/Pipeline7"] = {sid: "Encoder_Zynq_V24:4105"};
	this.sidHashMap["Encoder_Zynq_V24:4105"] = {rtwname: "<S5>/Pipeline7"};
	this.rtwnameHashMap["<S5>/Product_theta_el"] = {sid: "Encoder_Zynq_V24:2190"};
	this.sidHashMap["Encoder_Zynq_V24:2190"] = {rtwname: "<S5>/Product_theta_el"};
	this.rtwnameHashMap["<S5>/Scope"] = {sid: "Encoder_Zynq_V24:4085"};
	this.sidHashMap["Encoder_Zynq_V24:4085"] = {rtwname: "<S5>/Scope"};
	this.rtwnameHashMap["<S5>/WaitSomeTime"] = {sid: "Encoder_Zynq_V24:4647"};
	this.sidHashMap["Encoder_Zynq_V24:4647"] = {rtwname: "<S5>/WaitSomeTime"};
	this.rtwnameHashMap["<S5>/hold1"] = {sid: "Encoder_Zynq_V24:2193"};
	this.sidHashMap["Encoder_Zynq_V24:2193"] = {rtwname: "<S5>/hold1"};
	this.rtwnameHashMap["<S5>/hold2"] = {sid: "Encoder_Zynq_V24:4090"};
	this.sidHashMap["Encoder_Zynq_V24:4090"] = {rtwname: "<S5>/hold2"};
	this.rtwnameHashMap["<S5>/hold3"] = {sid: "Encoder_Zynq_V24:4645"};
	this.sidHashMap["Encoder_Zynq_V24:4645"] = {rtwname: "<S5>/hold3"};
	this.rtwnameHashMap["<S5>/hold4"] = {sid: "Encoder_Zynq_V24:4091"};
	this.sidHashMap["Encoder_Zynq_V24:4091"] = {rtwname: "<S5>/hold4"};
	this.rtwnameHashMap["<S5>/hold5"] = {sid: "Encoder_Zynq_V24:4675"};
	this.sidHashMap["Encoder_Zynq_V24:4675"] = {rtwname: "<S5>/hold5"};
	this.rtwnameHashMap["<S5>/omega_by_count_lines"] = {sid: "Encoder_Zynq_V24:3723"};
	this.sidHashMap["Encoder_Zynq_V24:3723"] = {rtwname: "<S5>/omega_by_count_lines"};
	this.rtwnameHashMap["<S5>/omega_by_measure_time"] = {sid: "Encoder_Zynq_V24:2194"};
	this.sidHashMap["Encoder_Zynq_V24:2194"] = {rtwname: "<S5>/omega_by_measure_time"};
	this.rtwnameHashMap["<S5>/omega_by_measure_time_old"] = {sid: "Encoder_Zynq_V24:4199"};
	this.sidHashMap["Encoder_Zynq_V24:4199"] = {rtwname: "<S5>/omega_by_measure_time_old"};
	this.rtwnameHashMap["<S5>/omega"] = {sid: "Encoder_Zynq_V24:2221"};
	this.sidHashMap["Encoder_Zynq_V24:2221"] = {rtwname: "<S5>/omega"};
	this.rtwnameHashMap["<S5>/omega_AXI4"] = {sid: "Encoder_Zynq_V24:2913"};
	this.sidHashMap["Encoder_Zynq_V24:2913"] = {rtwname: "<S5>/omega_AXI4"};
	this.rtwnameHashMap["<S5>/theta_el"] = {sid: "Encoder_Zynq_V24:2225"};
	this.sidHashMap["Encoder_Zynq_V24:2225"] = {rtwname: "<S5>/theta_el"};
	this.rtwnameHashMap["<S5>/theta_el_AXI4"] = {sid: "Encoder_Zynq_V24:2912"};
	this.sidHashMap["Encoder_Zynq_V24:2912"] = {rtwname: "<S5>/theta_el_AXI4"};
	this.rtwnameHashMap["<S5>/position"] = {sid: "Encoder_Zynq_V24:4047"};
	this.sidHashMap["Encoder_Zynq_V24:4047"] = {rtwname: "<S5>/position"};
	this.rtwnameHashMap["<S5>/position_AXI4"] = {sid: "Encoder_Zynq_V24:2226"};
	this.sidHashMap["Encoder_Zynq_V24:2226"] = {rtwname: "<S5>/position_AXI4"};
	this.rtwnameHashMap["<S5>/edge"] = {sid: "Encoder_Zynq_V24:2223"};
	this.sidHashMap["Encoder_Zynq_V24:2223"] = {rtwname: "<S5>/edge"};
	this.rtwnameHashMap["<S5>/count"] = {sid: "Encoder_Zynq_V24:2224"};
	this.sidHashMap["Encoder_Zynq_V24:2224"] = {rtwname: "<S5>/count"};
	this.rtwnameHashMap["<S5>/direction_AXI4"] = {sid: "Encoder_Zynq_V24:2222"};
	this.sidHashMap["Encoder_Zynq_V24:2222"] = {rtwname: "<S5>/direction_AXI4"};
	this.rtwnameHashMap["<S5>/countPerPeriod_AXI4"] = {sid: "Encoder_Zynq_V24:3774"};
	this.sidHashMap["Encoder_Zynq_V24:3774"] = {rtwname: "<S5>/countPerPeriod_AXI4"};
	this.rtwnameHashMap["<S5>/OverSamplFactor"] = {sid: "Encoder_Zynq_V24:5055"};
	this.sidHashMap["Encoder_Zynq_V24:5055"] = {rtwname: "<S5>/OverSamplFactor"};
	this.rtwnameHashMap["<S33>/OmegaIn "] = {sid: "Encoder_Zynq_V24:4112"};
	this.sidHashMap["Encoder_Zynq_V24:4112"] = {rtwname: "<S33>/OmegaIn "};
	this.rtwnameHashMap["<S33>/NewMeasurement"] = {sid: "Encoder_Zynq_V24:4113"};
	this.sidHashMap["Encoder_Zynq_V24:4113"] = {rtwname: "<S33>/NewMeasurement"};
	this.rtwnameHashMap["<S33>/SamplingFactorInv"] = {sid: "Encoder_Zynq_V24:4114"};
	this.sidHashMap["Encoder_Zynq_V24:4114"] = {rtwname: "<S33>/SamplingFactorInv"};
	this.rtwnameHashMap["<S33>/SamplingFactor"] = {sid: "Encoder_Zynq_V24:4115"};
	this.sidHashMap["Encoder_Zynq_V24:4115"] = {rtwname: "<S33>/SamplingFactor"};
	this.rtwnameHashMap["<S33>/Add"] = {sid: "Encoder_Zynq_V24:4116"};
	this.sidHashMap["Encoder_Zynq_V24:4116"] = {rtwname: "<S33>/Add"};
	this.rtwnameHashMap["<S33>/Ask1"] = {sid: "Encoder_Zynq_V24:4117"};
	this.sidHashMap["Encoder_Zynq_V24:4117"] = {rtwname: "<S33>/Ask1"};
	this.rtwnameHashMap["<S33>/Ask2"] = {sid: "Encoder_Zynq_V24:4118"};
	this.sidHashMap["Encoder_Zynq_V24:4118"] = {rtwname: "<S33>/Ask2"};
	this.rtwnameHashMap["<S33>/Ask3"] = {sid: "Encoder_Zynq_V24:4119"};
	this.sidHashMap["Encoder_Zynq_V24:4119"] = {rtwname: "<S33>/Ask3"};
	this.rtwnameHashMap["<S33>/Ask4"] = {sid: "Encoder_Zynq_V24:4120"};
	this.sidHashMap["Encoder_Zynq_V24:4120"] = {rtwname: "<S33>/Ask4"};
	this.rtwnameHashMap["<S33>/Ask5"] = {sid: "Encoder_Zynq_V24:4121"};
	this.sidHashMap["Encoder_Zynq_V24:4121"] = {rtwname: "<S33>/Ask5"};
	this.rtwnameHashMap["<S33>/Constant1"] = {sid: "Encoder_Zynq_V24:4122"};
	this.sidHashMap["Encoder_Zynq_V24:4122"] = {rtwname: "<S33>/Constant1"};
	this.rtwnameHashMap["<S33>/NewSpeed"] = {sid: "Encoder_Zynq_V24:4123"};
	this.sidHashMap["Encoder_Zynq_V24:4123"] = {rtwname: "<S33>/NewSpeed"};
	this.rtwnameHashMap["<S33>/NewSpeed1"] = {sid: "Encoder_Zynq_V24:4124"};
	this.sidHashMap["Encoder_Zynq_V24:4124"] = {rtwname: "<S33>/NewSpeed1"};
	this.rtwnameHashMap["<S33>/NewSpeed2"] = {sid: "Encoder_Zynq_V24:4125"};
	this.sidHashMap["Encoder_Zynq_V24:4125"] = {rtwname: "<S33>/NewSpeed2"};
	this.rtwnameHashMap["<S33>/NewSpeed3"] = {sid: "Encoder_Zynq_V24:4126"};
	this.sidHashMap["Encoder_Zynq_V24:4126"] = {rtwname: "<S33>/NewSpeed3"};
	this.rtwnameHashMap["<S33>/NewSpeed4"] = {sid: "Encoder_Zynq_V24:4127"};
	this.sidHashMap["Encoder_Zynq_V24:4127"] = {rtwname: "<S33>/NewSpeed4"};
	this.rtwnameHashMap["<S33>/Product"] = {sid: "Encoder_Zynq_V24:4128"};
	this.sidHashMap["Encoder_Zynq_V24:4128"] = {rtwname: "<S33>/Product"};
	this.rtwnameHashMap["<S33>/hold"] = {sid: "Encoder_Zynq_V24:4129"};
	this.sidHashMap["Encoder_Zynq_V24:4129"] = {rtwname: "<S33>/hold"};
	this.rtwnameHashMap["<S33>/hold1"] = {sid: "Encoder_Zynq_V24:4130"};
	this.sidHashMap["Encoder_Zynq_V24:4130"] = {rtwname: "<S33>/hold1"};
	this.rtwnameHashMap["<S33>/hold2"] = {sid: "Encoder_Zynq_V24:4131"};
	this.sidHashMap["Encoder_Zynq_V24:4131"] = {rtwname: "<S33>/hold2"};
	this.rtwnameHashMap["<S33>/hold3"] = {sid: "Encoder_Zynq_V24:4132"};
	this.sidHashMap["Encoder_Zynq_V24:4132"] = {rtwname: "<S33>/hold3"};
	this.rtwnameHashMap["<S33>/hold4"] = {sid: "Encoder_Zynq_V24:4133"};
	this.sidHashMap["Encoder_Zynq_V24:4133"] = {rtwname: "<S33>/hold4"};
	this.rtwnameHashMap["<S33>/OmegaOut"] = {sid: "Encoder_Zynq_V24:4134"};
	this.sidHashMap["Encoder_Zynq_V24:4134"] = {rtwname: "<S33>/OmegaOut"};
	this.rtwnameHashMap["<S34>/OmegaIn "] = {sid: "Encoder_Zynq_V24:3650"};
	this.sidHashMap["Encoder_Zynq_V24:3650"] = {rtwname: "<S34>/OmegaIn "};
	this.rtwnameHashMap["<S34>/NewMeasurement"] = {sid: "Encoder_Zynq_V24:3651"};
	this.sidHashMap["Encoder_Zynq_V24:3651"] = {rtwname: "<S34>/NewMeasurement"};
	this.rtwnameHashMap["<S34>/SamplingFactorInv"] = {sid: "Encoder_Zynq_V24:3654"};
	this.sidHashMap["Encoder_Zynq_V24:3654"] = {rtwname: "<S34>/SamplingFactorInv"};
	this.rtwnameHashMap["<S34>/SamplingFactor"] = {sid: "Encoder_Zynq_V24:3678"};
	this.sidHashMap["Encoder_Zynq_V24:3678"] = {rtwname: "<S34>/SamplingFactor"};
	this.rtwnameHashMap["<S34>/Add1"] = {sid: "Encoder_Zynq_V24:4170"};
	this.sidHashMap["Encoder_Zynq_V24:4170"] = {rtwname: "<S34>/Add1"};
	this.rtwnameHashMap["<S34>/Compare To Constant"] = {sid: "Encoder_Zynq_V24:4196"};
	this.sidHashMap["Encoder_Zynq_V24:4196"] = {rtwname: "<S34>/Compare To Constant"};
	this.rtwnameHashMap["<S34>/Constant2"] = {sid: "Encoder_Zynq_V24:4175"};
	this.sidHashMap["Encoder_Zynq_V24:4175"] = {rtwname: "<S34>/Constant2"};
	this.rtwnameHashMap["<S34>/Constant3"] = {sid: "Encoder_Zynq_V24:4198"};
	this.sidHashMap["Encoder_Zynq_V24:4198"] = {rtwname: "<S34>/Constant3"};
	this.rtwnameHashMap["<S34>/Data Type Conversion2"] = {sid: "Encoder_Zynq_V24:4177"};
	this.sidHashMap["Encoder_Zynq_V24:4177"] = {rtwname: "<S34>/Data Type Conversion2"};
	this.rtwnameHashMap["<S34>/Data Type Conversion3"] = {sid: "Encoder_Zynq_V24:4183"};
	this.sidHashMap["Encoder_Zynq_V24:4183"] = {rtwname: "<S34>/Data Type Conversion3"};
	this.rtwnameHashMap["<S34>/Data Type Conversion4"] = {sid: "Encoder_Zynq_V24:4193"};
	this.sidHashMap["Encoder_Zynq_V24:4193"] = {rtwname: "<S34>/Data Type Conversion4"};
	this.rtwnameHashMap["<S34>/Default"] = {sid: "Encoder_Zynq_V24:4148"};
	this.sidHashMap["Encoder_Zynq_V24:4148"] = {rtwname: "<S34>/Default"};
	this.rtwnameHashMap["<S34>/Delay1"] = {sid: "Encoder_Zynq_V24:4171"};
	this.sidHashMap["Encoder_Zynq_V24:4171"] = {rtwname: "<S34>/Delay1"};
	this.rtwnameHashMap["<S34>/Delay2"] = {sid: "Encoder_Zynq_V24:4136"};
	this.sidHashMap["Encoder_Zynq_V24:4136"] = {rtwname: "<S34>/Delay2"};
	this.rtwnameHashMap["<S34>/Delay3"] = {sid: "Encoder_Zynq_V24:4189"};
	this.sidHashMap["Encoder_Zynq_V24:4189"] = {rtwname: "<S34>/Delay3"};
	this.rtwnameHashMap["<S34>/Delay4"] = {sid: "Encoder_Zynq_V24:4191"};
	this.sidHashMap["Encoder_Zynq_V24:4191"] = {rtwname: "<S34>/Delay4"};
	this.rtwnameHashMap["<S34>/Delay48"] = {sid: "Encoder_Zynq_V24:4137"};
	this.sidHashMap["Encoder_Zynq_V24:4137"] = {rtwname: "<S34>/Delay48"};
	this.rtwnameHashMap["<S34>/Delay49"] = {sid: "Encoder_Zynq_V24:4138"};
	this.sidHashMap["Encoder_Zynq_V24:4138"] = {rtwname: "<S34>/Delay49"};
	this.rtwnameHashMap["<S34>/Delay5"] = {sid: "Encoder_Zynq_V24:4139"};
	this.sidHashMap["Encoder_Zynq_V24:4139"] = {rtwname: "<S34>/Delay5"};
	this.rtwnameHashMap["<S34>/DelayComp_i1"] = {sid: "Encoder_Zynq_V24:4140"};
	this.sidHashMap["Encoder_Zynq_V24:4140"] = {rtwname: "<S34>/DelayComp_i1"};
	this.rtwnameHashMap["<S34>/DelayToCompensate_Divide"] = {sid: "Encoder_Zynq_V24:4176"};
	this.sidHashMap["Encoder_Zynq_V24:4176"] = {rtwname: "<S34>/DelayToCompensate_Divide"};
	this.rtwnameHashMap["<S34>/Delta_i1"] = {sid: "Encoder_Zynq_V24:4141"};
	this.sidHashMap["Encoder_Zynq_V24:4141"] = {rtwname: "<S34>/Delta_i1"};
	this.rtwnameHashMap["<S34>/Div_i1"] = {sid: "Encoder_Zynq_V24:4142"};
	this.sidHashMap["Encoder_Zynq_V24:4142"] = {rtwname: "<S34>/Div_i1"};
	this.rtwnameHashMap["<S34>/IfFinalState"] = {sid: "Encoder_Zynq_V24:4173"};
	this.sidHashMap["Encoder_Zynq_V24:4173"] = {rtwname: "<S34>/IfFinalState"};
	this.rtwnameHashMap["<S34>/Inverse0"] = {sid: "Encoder_Zynq_V24:4149"};
	this.sidHashMap["Encoder_Zynq_V24:4149"] = {rtwname: "<S34>/Inverse0"};
	this.rtwnameHashMap["<S34>/Inverse1"] = {sid: "Encoder_Zynq_V24:4150"};
	this.sidHashMap["Encoder_Zynq_V24:4150"] = {rtwname: "<S34>/Inverse1"};
	this.rtwnameHashMap["<S34>/Inverse10"] = {sid: "Encoder_Zynq_V24:4151"};
	this.sidHashMap["Encoder_Zynq_V24:4151"] = {rtwname: "<S34>/Inverse10"};
	this.rtwnameHashMap["<S34>/Inverse11"] = {sid: "Encoder_Zynq_V24:4152"};
	this.sidHashMap["Encoder_Zynq_V24:4152"] = {rtwname: "<S34>/Inverse11"};
	this.rtwnameHashMap["<S34>/Inverse12"] = {sid: "Encoder_Zynq_V24:4153"};
	this.sidHashMap["Encoder_Zynq_V24:4153"] = {rtwname: "<S34>/Inverse12"};
	this.rtwnameHashMap["<S34>/Inverse13"] = {sid: "Encoder_Zynq_V24:4154"};
	this.sidHashMap["Encoder_Zynq_V24:4154"] = {rtwname: "<S34>/Inverse13"};
	this.rtwnameHashMap["<S34>/Inverse14"] = {sid: "Encoder_Zynq_V24:4155"};
	this.sidHashMap["Encoder_Zynq_V24:4155"] = {rtwname: "<S34>/Inverse14"};
	this.rtwnameHashMap["<S34>/Inverse15"] = {sid: "Encoder_Zynq_V24:4156"};
	this.sidHashMap["Encoder_Zynq_V24:4156"] = {rtwname: "<S34>/Inverse15"};
	this.rtwnameHashMap["<S34>/Inverse16"] = {sid: "Encoder_Zynq_V24:4157"};
	this.sidHashMap["Encoder_Zynq_V24:4157"] = {rtwname: "<S34>/Inverse16"};
	this.rtwnameHashMap["<S34>/Inverse17"] = {sid: "Encoder_Zynq_V24:4158"};
	this.sidHashMap["Encoder_Zynq_V24:4158"] = {rtwname: "<S34>/Inverse17"};
	this.rtwnameHashMap["<S34>/Inverse18"] = {sid: "Encoder_Zynq_V24:4159"};
	this.sidHashMap["Encoder_Zynq_V24:4159"] = {rtwname: "<S34>/Inverse18"};
	this.rtwnameHashMap["<S34>/Inverse19"] = {sid: "Encoder_Zynq_V24:4160"};
	this.sidHashMap["Encoder_Zynq_V24:4160"] = {rtwname: "<S34>/Inverse19"};
	this.rtwnameHashMap["<S34>/Inverse2"] = {sid: "Encoder_Zynq_V24:4161"};
	this.sidHashMap["Encoder_Zynq_V24:4161"] = {rtwname: "<S34>/Inverse2"};
	this.rtwnameHashMap["<S34>/Inverse3"] = {sid: "Encoder_Zynq_V24:4162"};
	this.sidHashMap["Encoder_Zynq_V24:4162"] = {rtwname: "<S34>/Inverse3"};
	this.rtwnameHashMap["<S34>/Inverse4"] = {sid: "Encoder_Zynq_V24:4163"};
	this.sidHashMap["Encoder_Zynq_V24:4163"] = {rtwname: "<S34>/Inverse4"};
	this.rtwnameHashMap["<S34>/Inverse5"] = {sid: "Encoder_Zynq_V24:4164"};
	this.sidHashMap["Encoder_Zynq_V24:4164"] = {rtwname: "<S34>/Inverse5"};
	this.rtwnameHashMap["<S34>/Inverse6"] = {sid: "Encoder_Zynq_V24:4165"};
	this.sidHashMap["Encoder_Zynq_V24:4165"] = {rtwname: "<S34>/Inverse6"};
	this.rtwnameHashMap["<S34>/Inverse7"] = {sid: "Encoder_Zynq_V24:4166"};
	this.sidHashMap["Encoder_Zynq_V24:4166"] = {rtwname: "<S34>/Inverse7"};
	this.rtwnameHashMap["<S34>/Inverse8"] = {sid: "Encoder_Zynq_V24:4167"};
	this.sidHashMap["Encoder_Zynq_V24:4167"] = {rtwname: "<S34>/Inverse8"};
	this.rtwnameHashMap["<S34>/Inverse9"] = {sid: "Encoder_Zynq_V24:4168"};
	this.sidHashMap["Encoder_Zynq_V24:4168"] = {rtwname: "<S34>/Inverse9"};
	this.rtwnameHashMap["<S34>/Reset"] = {sid: "Encoder_Zynq_V24:4172"};
	this.sidHashMap["Encoder_Zynq_V24:4172"] = {rtwname: "<S34>/Reset"};
	this.rtwnameHashMap["<S34>/Reset1"] = {sid: "Encoder_Zynq_V24:4190"};
	this.sidHashMap["Encoder_Zynq_V24:4190"] = {rtwname: "<S34>/Reset1"};
	this.rtwnameHashMap["<S34>/Reset_omega_old_with_0"] = {sid: "Encoder_Zynq_V24:4186"};
	this.sidHashMap["Encoder_Zynq_V24:4186"] = {rtwname: "<S34>/Reset_omega_old_with_0"};
	this.rtwnameHashMap["<S34>/Scope1"] = {sid: "Encoder_Zynq_V24:4178"};
	this.sidHashMap["Encoder_Zynq_V24:4178"] = {rtwname: "<S34>/Scope1"};
	this.rtwnameHashMap["<S34>/Scope2"] = {sid: "Encoder_Zynq_V24:4187"};
	this.sidHashMap["Encoder_Zynq_V24:4187"] = {rtwname: "<S34>/Scope2"};
	this.rtwnameHashMap["<S34>/Switch_idx3"] = {sid: "Encoder_Zynq_V24:4169"};
	this.sidHashMap["Encoder_Zynq_V24:4169"] = {rtwname: "<S34>/Switch_idx3"};
	this.rtwnameHashMap["<S34>/Switch_on2"] = {sid: "Encoder_Zynq_V24:4143"};
	this.sidHashMap["Encoder_Zynq_V24:4143"] = {rtwname: "<S34>/Switch_on2"};
	this.rtwnameHashMap["<S34>/Switch_on4"] = {sid: "Encoder_Zynq_V24:4185"};
	this.sidHashMap["Encoder_Zynq_V24:4185"] = {rtwname: "<S34>/Switch_on4"};
	this.rtwnameHashMap["<S34>/Switch_on5"] = {sid: "Encoder_Zynq_V24:4145"};
	this.sidHashMap["Encoder_Zynq_V24:4145"] = {rtwname: "<S34>/Switch_on5"};
	this.rtwnameHashMap["<S34>/m_act_i1"] = {sid: "Encoder_Zynq_V24:4146"};
	this.sidHashMap["Encoder_Zynq_V24:4146"] = {rtwname: "<S34>/m_act_i1"};
	this.rtwnameHashMap["<S34>/omega->n1"] = {sid: "Encoder_Zynq_V24:4184"};
	this.sidHashMap["Encoder_Zynq_V24:4184"] = {rtwname: "<S34>/omega->n1"};
	this.rtwnameHashMap["<S34>/omega->n2"] = {sid: "Encoder_Zynq_V24:4194"};
	this.sidHashMap["Encoder_Zynq_V24:4194"] = {rtwname: "<S34>/omega->n2"};
	this.rtwnameHashMap["<S35>/A"] = {sid: "Encoder_Zynq_V24:2053"};
	this.sidHashMap["Encoder_Zynq_V24:2053"] = {rtwname: "<S35>/A"};
	this.rtwnameHashMap["<S35>/B"] = {sid: "Encoder_Zynq_V24:2054"};
	this.sidHashMap["Encoder_Zynq_V24:2054"] = {rtwname: "<S35>/B"};
	this.rtwnameHashMap["<S35>/Add"] = {sid: "Encoder_Zynq_V24:2055"};
	this.sidHashMap["Encoder_Zynq_V24:2055"] = {rtwname: "<S35>/Add"};
	this.rtwnameHashMap["<S35>/Add1"] = {sid: "Encoder_Zynq_V24:2056"};
	this.sidHashMap["Encoder_Zynq_V24:2056"] = {rtwname: "<S35>/Add1"};
	this.rtwnameHashMap["<S35>/Add2"] = {sid: "Encoder_Zynq_V24:2057"};
	this.sidHashMap["Encoder_Zynq_V24:2057"] = {rtwname: "<S35>/Add2"};
	this.rtwnameHashMap["<S35>/Change"] = {sid: "Encoder_Zynq_V24:2058"};
	this.sidHashMap["Encoder_Zynq_V24:2058"] = {rtwname: "<S35>/Change"};
	this.rtwnameHashMap["<S35>/Change1"] = {sid: "Encoder_Zynq_V24:2059"};
	this.sidHashMap["Encoder_Zynq_V24:2059"] = {rtwname: "<S35>/Change1"};
	this.rtwnameHashMap["<S35>/Save_Direction"] = {sid: "Encoder_Zynq_V24:2060"};
	this.sidHashMap["Encoder_Zynq_V24:2060"] = {rtwname: "<S35>/Save_Direction"};
	this.rtwnameHashMap["<S35>/T_count_switch"] = {sid: "Encoder_Zynq_V24:2062"};
	this.sidHashMap["Encoder_Zynq_V24:2062"] = {rtwname: "<S35>/T_count_switch"};
	this.rtwnameHashMap["<S35>/TrnLeft"] = {sid: "Encoder_Zynq_V24:2063"};
	this.sidHashMap["Encoder_Zynq_V24:2063"] = {rtwname: "<S35>/TrnLeft"};
	this.rtwnameHashMap["<S35>/TrnRight"] = {sid: "Encoder_Zynq_V24:2064"};
	this.sidHashMap["Encoder_Zynq_V24:2064"] = {rtwname: "<S35>/TrnRight"};
	this.rtwnameHashMap["<S35>/TurnLeft"] = {sid: "Encoder_Zynq_V24:2065"};
	this.sidHashMap["Encoder_Zynq_V24:2065"] = {rtwname: "<S35>/TurnLeft"};
	this.rtwnameHashMap["<S35>/TurnRight"] = {sid: "Encoder_Zynq_V24:2066"};
	this.sidHashMap["Encoder_Zynq_V24:2066"] = {rtwname: "<S35>/TurnRight"};
	this.rtwnameHashMap["<S35>/count_old"] = {sid: "Encoder_Zynq_V24:2067"};
	this.sidHashMap["Encoder_Zynq_V24:2067"] = {rtwname: "<S35>/count_old"};
	this.rtwnameHashMap["<S35>/count_old1"] = {sid: "Encoder_Zynq_V24:2068"};
	this.sidHashMap["Encoder_Zynq_V24:2068"] = {rtwname: "<S35>/count_old1"};
	this.rtwnameHashMap["<S35>/count_old2"] = {sid: "Encoder_Zynq_V24:2069"};
	this.sidHashMap["Encoder_Zynq_V24:2069"] = {rtwname: "<S35>/count_old2"};
	this.rtwnameHashMap["<S35>/count_old_switch"] = {sid: "Encoder_Zynq_V24:2070"};
	this.sidHashMap["Encoder_Zynq_V24:2070"] = {rtwname: "<S35>/count_old_switch"};
	this.rtwnameHashMap["<S35>/count_old_switch1"] = {sid: "Encoder_Zynq_V24:2071"};
	this.sidHashMap["Encoder_Zynq_V24:2071"] = {rtwname: "<S35>/count_old_switch1"};
	this.rtwnameHashMap["<S35>/new_value"] = {sid: "Encoder_Zynq_V24:2072"};
	this.sidHashMap["Encoder_Zynq_V24:2072"] = {rtwname: "<S35>/new_value"};
	this.rtwnameHashMap["<S35>/nothing"] = {sid: "Encoder_Zynq_V24:2073"};
	this.sidHashMap["Encoder_Zynq_V24:2073"] = {rtwname: "<S35>/nothing"};
	this.rtwnameHashMap["<S35>/nothing1"] = {sid: "Encoder_Zynq_V24:2074"};
	this.sidHashMap["Encoder_Zynq_V24:2074"] = {rtwname: "<S35>/nothing1"};
	this.rtwnameHashMap["<S35>/speed_old_switch"] = {sid: "Encoder_Zynq_V24:2075"};
	this.sidHashMap["Encoder_Zynq_V24:2075"] = {rtwname: "<S35>/speed_old_switch"};
	this.rtwnameHashMap["<S35>/Dir"] = {sid: "Encoder_Zynq_V24:2076"};
	this.sidHashMap["Encoder_Zynq_V24:2076"] = {rtwname: "<S35>/Dir"};
	this.rtwnameHashMap["<S36>/Enable_ctr"] = {sid: "Encoder_Zynq_V24:3597"};
	this.sidHashMap["Encoder_Zynq_V24:3597"] = {rtwname: "<S36>/Enable_ctr"};
	this.rtwnameHashMap["<S36>/I_line"] = {sid: "Encoder_Zynq_V24:3598"};
	this.sidHashMap["Encoder_Zynq_V24:3598"] = {rtwname: "<S36>/I_line"};
	this.rtwnameHashMap["<S36>/Detec_edge"] = {sid: "Encoder_Zynq_V24:3606"};
	this.sidHashMap["Encoder_Zynq_V24:3606"] = {rtwname: "<S36>/Detec_edge"};
	this.rtwnameHashMap["<S36>/Reset_flag"] = {sid: "Encoder_Zynq_V24:3609"};
	this.sidHashMap["Encoder_Zynq_V24:3609"] = {rtwname: "<S36>/Reset_flag"};
	this.rtwnameHashMap["<S36>/reset_old"] = {sid: "Encoder_Zynq_V24:3614"};
	this.sidHashMap["Encoder_Zynq_V24:3614"] = {rtwname: "<S36>/reset_old"};
	this.rtwnameHashMap["<S36>/reset_old1"] = {sid: "Encoder_Zynq_V24:3615"};
	this.sidHashMap["Encoder_Zynq_V24:3615"] = {rtwname: "<S36>/reset_old1"};
	this.rtwnameHashMap["<S36>/reset_old_switch"] = {sid: "Encoder_Zynq_V24:3620"};
	this.sidHashMap["Encoder_Zynq_V24:3620"] = {rtwname: "<S36>/reset_old_switch"};
	this.rtwnameHashMap["<S36>/reset_old_switch1"] = {sid: "Encoder_Zynq_V24:3621"};
	this.sidHashMap["Encoder_Zynq_V24:3621"] = {rtwname: "<S36>/reset_old_switch1"};
	this.rtwnameHashMap["<S36>/Edge"] = {sid: "Encoder_Zynq_V24:3624"};
	this.sidHashMap["Encoder_Zynq_V24:3624"] = {rtwname: "<S36>/Edge"};
	this.rtwnameHashMap["<S36>/Reset"] = {sid: "Encoder_Zynq_V24:3633"};
	this.sidHashMap["Encoder_Zynq_V24:3633"] = {rtwname: "<S36>/Reset"};
	this.rtwnameHashMap["<S37>/Edge"] = {sid: "Encoder_Zynq_V24:3635"};
	this.sidHashMap["Encoder_Zynq_V24:3635"] = {rtwname: "<S37>/Edge"};
	this.rtwnameHashMap["<S37>/Reset"] = {sid: "Encoder_Zynq_V24:2111"};
	this.sidHashMap["Encoder_Zynq_V24:2111"] = {rtwname: "<S37>/Reset"};
	this.rtwnameHashMap["<S37>/IncsPerTurn"] = {sid: "Encoder_Zynq_V24:2112"};
	this.sidHashMap["Encoder_Zynq_V24:2112"] = {rtwname: "<S37>/IncsPerTurn"};
	this.rtwnameHashMap["<S37>/Dir"] = {sid: "Encoder_Zynq_V24:2113"};
	this.sidHashMap["Encoder_Zynq_V24:2113"] = {rtwname: "<S37>/Dir"};
	this.rtwnameHashMap["<S37>/Add1"] = {sid: "Encoder_Zynq_V24:2114"};
	this.sidHashMap["Encoder_Zynq_V24:2114"] = {rtwname: "<S37>/Add1"};
	this.rtwnameHashMap["<S37>/Constant1"] = {sid: "Encoder_Zynq_V24:2115"};
	this.sidHashMap["Encoder_Zynq_V24:2115"] = {rtwname: "<S37>/Constant1"};
	this.rtwnameHashMap["<S37>/Constant2"] = {sid: "Encoder_Zynq_V24:2116"};
	this.sidHashMap["Encoder_Zynq_V24:2116"] = {rtwname: "<S37>/Constant2"};
	this.rtwnameHashMap["<S37>/Constant3"] = {sid: "Encoder_Zynq_V24:2117"};
	this.sidHashMap["Encoder_Zynq_V24:2117"] = {rtwname: "<S37>/Constant3"};
	this.rtwnameHashMap["<S37>/Constant5"] = {sid: "Encoder_Zynq_V24:2119"};
	this.sidHashMap["Encoder_Zynq_V24:2119"] = {rtwname: "<S37>/Constant5"};
	this.rtwnameHashMap["<S37>/Relational Operator"] = {sid: "Encoder_Zynq_V24:2121"};
	this.sidHashMap["Encoder_Zynq_V24:2121"] = {rtwname: "<S37>/Relational Operator"};
	this.rtwnameHashMap["<S37>/Relational Operator1"] = {sid: "Encoder_Zynq_V24:2122"};
	this.sidHashMap["Encoder_Zynq_V24:2122"] = {rtwname: "<S37>/Relational Operator1"};
	this.rtwnameHashMap["<S37>/Resett_switch1"] = {sid: "Encoder_Zynq_V24:2125"};
	this.sidHashMap["Encoder_Zynq_V24:2125"] = {rtwname: "<S37>/Resett_switch1"};
	this.rtwnameHashMap["<S37>/Resett_switch2"] = {sid: "Encoder_Zynq_V24:2126"};
	this.sidHashMap["Encoder_Zynq_V24:2126"] = {rtwname: "<S37>/Resett_switch2"};
	this.rtwnameHashMap["<S37>/Resett_switch3"] = {sid: "Encoder_Zynq_V24:2127"};
	this.sidHashMap["Encoder_Zynq_V24:2127"] = {rtwname: "<S37>/Resett_switch3"};
	this.rtwnameHashMap["<S37>/only_use_for_neg_speed"] = {sid: "Encoder_Zynq_V24:2128"};
	this.sidHashMap["Encoder_Zynq_V24:2128"] = {rtwname: "<S37>/only_use_for_neg_speed"};
	this.rtwnameHashMap["<S37>/reset_old1"] = {sid: "Encoder_Zynq_V24:4052"};
	this.sidHashMap["Encoder_Zynq_V24:4052"] = {rtwname: "<S37>/reset_old1"};
	this.rtwnameHashMap["<S37>/reset_old2"] = {sid: "Encoder_Zynq_V24:2131"};
	this.sidHashMap["Encoder_Zynq_V24:2131"] = {rtwname: "<S37>/reset_old2"};
	this.rtwnameHashMap["<S37>/reset_old3"] = {sid: "Encoder_Zynq_V24:2132"};
	this.sidHashMap["Encoder_Zynq_V24:2132"] = {rtwname: "<S37>/reset_old3"};
	this.rtwnameHashMap["<S37>/reset_old4"] = {sid: "Encoder_Zynq_V24:2133"};
	this.sidHashMap["Encoder_Zynq_V24:2133"] = {rtwname: "<S37>/reset_old4"};
	this.rtwnameHashMap["<S37>/reset_old5"] = {sid: "Encoder_Zynq_V24:2134"};
	this.sidHashMap["Encoder_Zynq_V24:2134"] = {rtwname: "<S37>/reset_old5"};
	this.rtwnameHashMap["<S37>/reset_old6"] = {sid: "Encoder_Zynq_V24:2135"};
	this.sidHashMap["Encoder_Zynq_V24:2135"] = {rtwname: "<S37>/reset_old6"};
	this.rtwnameHashMap["<S37>/speed_old_switch1"] = {sid: "Encoder_Zynq_V24:2138"};
	this.sidHashMap["Encoder_Zynq_V24:2138"] = {rtwname: "<S37>/speed_old_switch1"};
	this.rtwnameHashMap["<S37>/speed_old_switch2"] = {sid: "Encoder_Zynq_V24:2139"};
	this.sidHashMap["Encoder_Zynq_V24:2139"] = {rtwname: "<S37>/speed_old_switch2"};
	this.rtwnameHashMap["<S37>/count"] = {sid: "Encoder_Zynq_V24:2140"};
	this.sidHashMap["Encoder_Zynq_V24:2140"] = {rtwname: "<S37>/count"};
	this.rtwnameHashMap["<S38>/Edge"] = {sid: "Encoder_Zynq_V24:3636"};
	this.sidHashMap["Encoder_Zynq_V24:3636"] = {rtwname: "<S38>/Edge"};
	this.rtwnameHashMap["<S38>/Reset"] = {sid: "Encoder_Zynq_V24:2151"};
	this.sidHashMap["Encoder_Zynq_V24:2151"] = {rtwname: "<S38>/Reset"};
	this.rtwnameHashMap["<S38>/IncsPerTurn"] = {sid: "Encoder_Zynq_V24:2152"};
	this.sidHashMap["Encoder_Zynq_V24:2152"] = {rtwname: "<S38>/IncsPerTurn"};
	this.rtwnameHashMap["<S38>/Dir"] = {sid: "Encoder_Zynq_V24:2153"};
	this.sidHashMap["Encoder_Zynq_V24:2153"] = {rtwname: "<S38>/Dir"};
	this.rtwnameHashMap["<S38>/Add1"] = {sid: "Encoder_Zynq_V24:2154"};
	this.sidHashMap["Encoder_Zynq_V24:2154"] = {rtwname: "<S38>/Add1"};
	this.rtwnameHashMap["<S38>/Constant1"] = {sid: "Encoder_Zynq_V24:2155"};
	this.sidHashMap["Encoder_Zynq_V24:2155"] = {rtwname: "<S38>/Constant1"};
	this.rtwnameHashMap["<S38>/Constant2"] = {sid: "Encoder_Zynq_V24:2156"};
	this.sidHashMap["Encoder_Zynq_V24:2156"] = {rtwname: "<S38>/Constant2"};
	this.rtwnameHashMap["<S38>/Constant3"] = {sid: "Encoder_Zynq_V24:2157"};
	this.sidHashMap["Encoder_Zynq_V24:2157"] = {rtwname: "<S38>/Constant3"};
	this.rtwnameHashMap["<S38>/Constant5"] = {sid: "Encoder_Zynq_V24:2158"};
	this.sidHashMap["Encoder_Zynq_V24:2158"] = {rtwname: "<S38>/Constant5"};
	this.rtwnameHashMap["<S38>/Relational Operator"] = {sid: "Encoder_Zynq_V24:2160"};
	this.sidHashMap["Encoder_Zynq_V24:2160"] = {rtwname: "<S38>/Relational Operator"};
	this.rtwnameHashMap["<S38>/Relational Operator1"] = {sid: "Encoder_Zynq_V24:2161"};
	this.sidHashMap["Encoder_Zynq_V24:2161"] = {rtwname: "<S38>/Relational Operator1"};
	this.rtwnameHashMap["<S38>/Reset_switch3"] = {sid: "Encoder_Zynq_V24:2165"};
	this.sidHashMap["Encoder_Zynq_V24:2165"] = {rtwname: "<S38>/Reset_switch3"};
	this.rtwnameHashMap["<S38>/Resett_switch1"] = {sid: "Encoder_Zynq_V24:2163"};
	this.sidHashMap["Encoder_Zynq_V24:2163"] = {rtwname: "<S38>/Resett_switch1"};
	this.rtwnameHashMap["<S38>/Resett_switch2"] = {sid: "Encoder_Zynq_V24:2164"};
	this.sidHashMap["Encoder_Zynq_V24:2164"] = {rtwname: "<S38>/Resett_switch2"};
	this.rtwnameHashMap["<S38>/only_use_for_neg_speed"] = {sid: "Encoder_Zynq_V24:2166"};
	this.sidHashMap["Encoder_Zynq_V24:2166"] = {rtwname: "<S38>/only_use_for_neg_speed"};
	this.rtwnameHashMap["<S38>/reset_old1"] = {sid: "Encoder_Zynq_V24:4050"};
	this.sidHashMap["Encoder_Zynq_V24:4050"] = {rtwname: "<S38>/reset_old1"};
	this.rtwnameHashMap["<S38>/reset_old2"] = {sid: "Encoder_Zynq_V24:2169"};
	this.sidHashMap["Encoder_Zynq_V24:2169"] = {rtwname: "<S38>/reset_old2"};
	this.rtwnameHashMap["<S38>/reset_old3"] = {sid: "Encoder_Zynq_V24:2170"};
	this.sidHashMap["Encoder_Zynq_V24:2170"] = {rtwname: "<S38>/reset_old3"};
	this.rtwnameHashMap["<S38>/reset_old4"] = {sid: "Encoder_Zynq_V24:2171"};
	this.sidHashMap["Encoder_Zynq_V24:2171"] = {rtwname: "<S38>/reset_old4"};
	this.rtwnameHashMap["<S38>/reset_old5"] = {sid: "Encoder_Zynq_V24:2172"};
	this.sidHashMap["Encoder_Zynq_V24:2172"] = {rtwname: "<S38>/reset_old5"};
	this.rtwnameHashMap["<S38>/reset_old6"] = {sid: "Encoder_Zynq_V24:4053"};
	this.sidHashMap["Encoder_Zynq_V24:4053"] = {rtwname: "<S38>/reset_old6"};
	this.rtwnameHashMap["<S38>/speed_old_switch1"] = {sid: "Encoder_Zynq_V24:2175"};
	this.sidHashMap["Encoder_Zynq_V24:2175"] = {rtwname: "<S38>/speed_old_switch1"};
	this.rtwnameHashMap["<S38>/speed_old_switch2"] = {sid: "Encoder_Zynq_V24:2176"};
	this.sidHashMap["Encoder_Zynq_V24:2176"] = {rtwname: "<S38>/speed_old_switch2"};
	this.rtwnameHashMap["<S38>/count"] = {sid: "Encoder_Zynq_V24:2177"};
	this.sidHashMap["Encoder_Zynq_V24:2177"] = {rtwname: "<S38>/count"};
	this.rtwnameHashMap["<S39>/Edge"] = {sid: "Encoder_Zynq_V24:3740"};
	this.sidHashMap["Encoder_Zynq_V24:3740"] = {rtwname: "<S39>/Edge"};
	this.rtwnameHashMap["<S39>/Reset"] = {sid: "Encoder_Zynq_V24:3741"};
	this.sidHashMap["Encoder_Zynq_V24:3741"] = {rtwname: "<S39>/Reset"};
	this.rtwnameHashMap["<S39>/IncsPerTurn"] = {sid: "Encoder_Zynq_V24:3742"};
	this.sidHashMap["Encoder_Zynq_V24:3742"] = {rtwname: "<S39>/IncsPerTurn"};
	this.rtwnameHashMap["<S39>/Add1"] = {sid: "Encoder_Zynq_V24:3744"};
	this.sidHashMap["Encoder_Zynq_V24:3744"] = {rtwname: "<S39>/Add1"};
	this.rtwnameHashMap["<S39>/Constant1"] = {sid: "Encoder_Zynq_V24:3745"};
	this.sidHashMap["Encoder_Zynq_V24:3745"] = {rtwname: "<S39>/Constant1"};
	this.rtwnameHashMap["<S39>/Constant2"] = {sid: "Encoder_Zynq_V24:3746"};
	this.sidHashMap["Encoder_Zynq_V24:3746"] = {rtwname: "<S39>/Constant2"};
	this.rtwnameHashMap["<S39>/Constant3"] = {sid: "Encoder_Zynq_V24:3747"};
	this.sidHashMap["Encoder_Zynq_V24:3747"] = {rtwname: "<S39>/Constant3"};
	this.rtwnameHashMap["<S39>/Constant4"] = {sid: "Encoder_Zynq_V24:3748"};
	this.sidHashMap["Encoder_Zynq_V24:3748"] = {rtwname: "<S39>/Constant4"};
	this.rtwnameHashMap["<S39>/Relational Operator"] = {sid: "Encoder_Zynq_V24:3750"};
	this.sidHashMap["Encoder_Zynq_V24:3750"] = {rtwname: "<S39>/Relational Operator"};
	this.rtwnameHashMap["<S39>/Reset_when_overflow"] = {sid: "Encoder_Zynq_V24:3752"};
	this.sidHashMap["Encoder_Zynq_V24:3752"] = {rtwname: "<S39>/Reset_when_overflow"};
	this.rtwnameHashMap["<S39>/Resett_switch1"] = {sid: "Encoder_Zynq_V24:3753"};
	this.sidHashMap["Encoder_Zynq_V24:3753"] = {rtwname: "<S39>/Resett_switch1"};
	this.rtwnameHashMap["<S39>/Resett_switch2"] = {sid: "Encoder_Zynq_V24:3754"};
	this.sidHashMap["Encoder_Zynq_V24:3754"] = {rtwname: "<S39>/Resett_switch2"};
	this.rtwnameHashMap["<S39>/reset_old1"] = {sid: "Encoder_Zynq_V24:3780"};
	this.sidHashMap["Encoder_Zynq_V24:3780"] = {rtwname: "<S39>/reset_old1"};
	this.rtwnameHashMap["<S39>/reset_old3"] = {sid: "Encoder_Zynq_V24:3758"};
	this.sidHashMap["Encoder_Zynq_V24:3758"] = {rtwname: "<S39>/reset_old3"};
	this.rtwnameHashMap["<S39>/reset_old4"] = {sid: "Encoder_Zynq_V24:3759"};
	this.sidHashMap["Encoder_Zynq_V24:3759"] = {rtwname: "<S39>/reset_old4"};
	this.rtwnameHashMap["<S39>/reset_old5"] = {sid: "Encoder_Zynq_V24:3760"};
	this.sidHashMap["Encoder_Zynq_V24:3760"] = {rtwname: "<S39>/reset_old5"};
	this.rtwnameHashMap["<S39>/reset_old_switch1"] = {sid: "Encoder_Zynq_V24:3781"};
	this.sidHashMap["Encoder_Zynq_V24:3781"] = {rtwname: "<S39>/reset_old_switch1"};
	this.rtwnameHashMap["<S39>/speed_old_switch1"] = {sid: "Encoder_Zynq_V24:3762"};
	this.sidHashMap["Encoder_Zynq_V24:3762"] = {rtwname: "<S39>/speed_old_switch1"};
	this.rtwnameHashMap["<S39>/count"] = {sid: "Encoder_Zynq_V24:3764"};
	this.sidHashMap["Encoder_Zynq_V24:3764"] = {rtwname: "<S39>/count"};
	this.rtwnameHashMap["<S40>/Edge"] = {sid: "Encoder_Zynq_V24:3355"};
	this.sidHashMap["Encoder_Zynq_V24:3355"] = {rtwname: "<S40>/Edge"};
	this.rtwnameHashMap["<S40>/T"] = {sid: "Encoder_Zynq_V24:2197"};
	this.sidHashMap["Encoder_Zynq_V24:2197"] = {rtwname: "<S40>/T"};
	this.rtwnameHashMap["<S40>/Dir"] = {sid: "Encoder_Zynq_V24:4092"};
	this.sidHashMap["Encoder_Zynq_V24:4092"] = {rtwname: "<S40>/Dir"};
	this.rtwnameHashMap["<S40>/ReadBack_Omega"] = {sid: "Encoder_Zynq_V24:4646"};
	this.sidHashMap["Encoder_Zynq_V24:4646"] = {rtwname: "<S40>/ReadBack_Omega"};
	this.rtwnameHashMap["<S40>/OverSamplOmega"] = {sid: "Encoder_Zynq_V24:4650"};
	this.sidHashMap["Encoder_Zynq_V24:4650"] = {rtwname: "<S40>/OverSamplOmega"};
	this.rtwnameHashMap["<S40>/ABS"] = {sid: "Encoder_Zynq_V24:4097"};
	this.sidHashMap["Encoder_Zynq_V24:4097"] = {rtwname: "<S40>/ABS"};
	this.rtwnameHashMap["<S40>/Abs"] = {sid: "Encoder_Zynq_V24:4676"};
	this.sidHashMap["Encoder_Zynq_V24:4676"] = {rtwname: "<S40>/Abs"};
	this.rtwnameHashMap["<S40>/Add1"] = {sid: "Encoder_Zynq_V24:2199"};
	this.sidHashMap["Encoder_Zynq_V24:2199"] = {rtwname: "<S40>/Add1"};
	this.rtwnameHashMap["<S40>/Add2"] = {sid: "Encoder_Zynq_V24:4096"};
	this.sidHashMap["Encoder_Zynq_V24:4096"] = {rtwname: "<S40>/Add2"};
	this.rtwnameHashMap["<S40>/Add3"] = {sid: "Encoder_Zynq_V24:4235"};
	this.sidHashMap["Encoder_Zynq_V24:4235"] = {rtwname: "<S40>/Add3"};
	this.rtwnameHashMap["<S40>/Constant"] = {sid: "Encoder_Zynq_V24:2200"};
	this.sidHashMap["Encoder_Zynq_V24:2200"] = {rtwname: "<S40>/Constant"};
	this.rtwnameHashMap["<S40>/Constant1"] = {sid: "Encoder_Zynq_V24:2201"};
	this.sidHashMap["Encoder_Zynq_V24:2201"] = {rtwname: "<S40>/Constant1"};
	this.rtwnameHashMap["<S40>/Constant2"] = {sid: "Encoder_Zynq_V24:2202"};
	this.sidHashMap["Encoder_Zynq_V24:2202"] = {rtwname: "<S40>/Constant2"};
	this.rtwnameHashMap["<S40>/Constant3"] = {sid: "Encoder_Zynq_V24:3558"};
	this.sidHashMap["Encoder_Zynq_V24:3558"] = {rtwname: "<S40>/Constant3"};
	this.rtwnameHashMap["<S40>/Constant4"] = {sid: "Encoder_Zynq_V24:4237"};
	this.sidHashMap["Encoder_Zynq_V24:4237"] = {rtwname: "<S40>/Constant4"};
	this.rtwnameHashMap["<S40>/Constant5"] = {sid: "Encoder_Zynq_V24:4238"};
	this.sidHashMap["Encoder_Zynq_V24:4238"] = {rtwname: "<S40>/Constant5"};
	this.rtwnameHashMap["<S40>/Count"] = {sid: "Encoder_Zynq_V24:4666"};
	this.sidHashMap["Encoder_Zynq_V24:4666"] = {rtwname: "<S40>/Count"};
	this.rtwnameHashMap["<S40>/Data Type Conversion1"] = {sid: "Encoder_Zynq_V24:4672"};
	this.sidHashMap["Encoder_Zynq_V24:4672"] = {rtwname: "<S40>/Data Type Conversion1"};
	this.rtwnameHashMap["<S40>/Delay1"] = {sid: "Encoder_Zynq_V24:4239"};
	this.sidHashMap["Encoder_Zynq_V24:4239"] = {rtwname: "<S40>/Delay1"};
	this.rtwnameHashMap["<S40>/Delay2"] = {sid: "Encoder_Zynq_V24:4652"};
	this.sidHashMap["Encoder_Zynq_V24:4652"] = {rtwname: "<S40>/Delay2"};
	this.rtwnameHashMap["<S40>/Delay3"] = {sid: "Encoder_Zynq_V24:5053"};
	this.sidHashMap["Encoder_Zynq_V24:5053"] = {rtwname: "<S40>/Delay3"};
	this.rtwnameHashMap["<S40>/Delay4"] = {sid: "Encoder_Zynq_V24:4674"};
	this.sidHashMap["Encoder_Zynq_V24:4674"] = {rtwname: "<S40>/Delay4"};
	this.rtwnameHashMap["<S40>/Delay5"] = {sid: "Encoder_Zynq_V24:5054"};
	this.sidHashMap["Encoder_Zynq_V24:5054"] = {rtwname: "<S40>/Delay5"};
	this.rtwnameHashMap["<S40>/Delay6"] = {sid: "Encoder_Zynq_V24:4667"};
	this.sidHashMap["Encoder_Zynq_V24:4667"] = {rtwname: "<S40>/Delay6"};
	this.rtwnameHashMap["<S40>/Delay7"] = {sid: "Encoder_Zynq_V24:4668"};
	this.sidHashMap["Encoder_Zynq_V24:4668"] = {rtwname: "<S40>/Delay7"};
	this.rtwnameHashMap["<S40>/Delay8"] = {sid: "Encoder_Zynq_V24:4670"};
	this.sidHashMap["Encoder_Zynq_V24:4670"] = {rtwname: "<S40>/Delay8"};
	this.rtwnameHashMap["<S40>/Detec_edge"] = {sid: "Encoder_Zynq_V24:2088"};
	this.sidHashMap["Encoder_Zynq_V24:2088"] = {rtwname: "<S40>/Detec_edge"};
	this.rtwnameHashMap["<S40>/IfFinalState"] = {sid: "Encoder_Zynq_V24:4240"};
	this.sidHashMap["Encoder_Zynq_V24:4240"] = {rtwname: "<S40>/IfFinalState"};
	this.rtwnameHashMap["<S40>/OnlyAllowInNewPeriod"] = {sid: "Encoder_Zynq_V24:4651"};
	this.sidHashMap["Encoder_Zynq_V24:4651"] = {rtwname: "<S40>/OnlyAllowInNewPeriod"};
	this.rtwnameHashMap["<S40>/Pipeline"] = {sid: "Encoder_Zynq_V24:4098"};
	this.sidHashMap["Encoder_Zynq_V24:4098"] = {rtwname: "<S40>/Pipeline"};
	this.rtwnameHashMap["<S40>/Pipeline1"] = {sid: "Encoder_Zynq_V24:4099"};
	this.sidHashMap["Encoder_Zynq_V24:4099"] = {rtwname: "<S40>/Pipeline1"};
	this.rtwnameHashMap["<S40>/ProtectOverflow_T_count"] = {sid: "Encoder_Zynq_V24:2205"};
	this.sidHashMap["Encoder_Zynq_V24:2205"] = {rtwname: "<S40>/ProtectOverflow_T_count"};
	this.rtwnameHashMap["<S40>/ProtectOverflow_rps"] = {sid: "Encoder_Zynq_V24:2206"};
	this.sidHashMap["Encoder_Zynq_V24:2206"] = {rtwname: "<S40>/ProtectOverflow_rps"};
	this.rtwnameHashMap["<S40>/ProtectToUseOmegaIfThetaJump1"] = {sid: "Encoder_Zynq_V24:3557"};
	this.sidHashMap["Encoder_Zynq_V24:3557"] = {rtwname: "<S40>/ProtectToUseOmegaIfThetaJump1"};
	this.rtwnameHashMap["<S40>/Reset"] = {sid: "Encoder_Zynq_V24:4241"};
	this.sidHashMap["Encoder_Zynq_V24:4241"] = {rtwname: "<S40>/Reset"};
	this.rtwnameHashMap["<S40>/Reset1"] = {sid: "Encoder_Zynq_V24:4673"};
	this.sidHashMap["Encoder_Zynq_V24:4673"] = {rtwname: "<S40>/Reset1"};
	this.rtwnameHashMap["<S40>/Scope"] = {sid: "Encoder_Zynq_V24:3683"};
	this.sidHashMap["Encoder_Zynq_V24:3683"] = {rtwname: "<S40>/Scope"};
	this.rtwnameHashMap["<S40>/T_count"] = {sid: "Encoder_Zynq_V24:2207"};
	this.sidHashMap["Encoder_Zynq_V24:2207"] = {rtwname: "<S40>/T_count"};
	this.rtwnameHashMap["<S40>/T_count_switch"] = {sid: "Encoder_Zynq_V24:2208"};
	this.sidHashMap["Encoder_Zynq_V24:2208"] = {rtwname: "<S40>/T_count_switch"};
	this.rtwnameHashMap["<S40>/count_old1"] = {sid: "Encoder_Zynq_V24:3555"};
	this.sidHashMap["Encoder_Zynq_V24:3555"] = {rtwname: "<S40>/count_old1"};
	this.rtwnameHashMap["<S40>/reset_old1"] = {sid: "Encoder_Zynq_V24:2095"};
	this.sidHashMap["Encoder_Zynq_V24:2095"] = {rtwname: "<S40>/reset_old1"};
	this.rtwnameHashMap["<S40>/reset_old_switch1"] = {sid: "Encoder_Zynq_V24:2099"};
	this.sidHashMap["Encoder_Zynq_V24:2099"] = {rtwname: "<S40>/reset_old_switch1"};
	this.rtwnameHashMap["<S40>/speed_old"] = {sid: "Encoder_Zynq_V24:2211"};
	this.sidHashMap["Encoder_Zynq_V24:2211"] = {rtwname: "<S40>/speed_old"};
	this.rtwnameHashMap["<S40>/speed_old_switch"] = {sid: "Encoder_Zynq_V24:2212"};
	this.sidHashMap["Encoder_Zynq_V24:2212"] = {rtwname: "<S40>/speed_old_switch"};
	this.rtwnameHashMap["<S40>/speed_old_switch2"] = {sid: "Encoder_Zynq_V24:4095"};
	this.sidHashMap["Encoder_Zynq_V24:4095"] = {rtwname: "<S40>/speed_old_switch2"};
	this.rtwnameHashMap["<S40>/one_Omega"] = {sid: "Encoder_Zynq_V24:2213"};
	this.sidHashMap["Encoder_Zynq_V24:2213"] = {rtwname: "<S40>/one_Omega"};
	this.rtwnameHashMap["<S40>/OverSamplingFactor"] = {sid: "Encoder_Zynq_V24:4644"};
	this.sidHashMap["Encoder_Zynq_V24:4644"] = {rtwname: "<S40>/OverSamplingFactor"};
	this.rtwnameHashMap["<S40>/NewMeasurement"] = {sid: "Encoder_Zynq_V24:3647"};
	this.sidHashMap["Encoder_Zynq_V24:3647"] = {rtwname: "<S40>/NewMeasurement"};
	this.rtwnameHashMap["<S41>/Edge"] = {sid: "Encoder_Zynq_V24:4200"};
	this.sidHashMap["Encoder_Zynq_V24:4200"] = {rtwname: "<S41>/Edge"};
	this.rtwnameHashMap["<S41>/T"] = {sid: "Encoder_Zynq_V24:4201"};
	this.sidHashMap["Encoder_Zynq_V24:4201"] = {rtwname: "<S41>/T"};
	this.rtwnameHashMap["<S41>/Dir"] = {sid: "Encoder_Zynq_V24:4202"};
	this.sidHashMap["Encoder_Zynq_V24:4202"] = {rtwname: "<S41>/Dir"};
	this.rtwnameHashMap["<S41>/ABS"] = {sid: "Encoder_Zynq_V24:4203"};
	this.sidHashMap["Encoder_Zynq_V24:4203"] = {rtwname: "<S41>/ABS"};
	this.rtwnameHashMap["<S41>/Add1"] = {sid: "Encoder_Zynq_V24:4204"};
	this.sidHashMap["Encoder_Zynq_V24:4204"] = {rtwname: "<S41>/Add1"};
	this.rtwnameHashMap["<S41>/Add2"] = {sid: "Encoder_Zynq_V24:4205"};
	this.sidHashMap["Encoder_Zynq_V24:4205"] = {rtwname: "<S41>/Add2"};
	this.rtwnameHashMap["<S41>/Constant"] = {sid: "Encoder_Zynq_V24:4206"};
	this.sidHashMap["Encoder_Zynq_V24:4206"] = {rtwname: "<S41>/Constant"};
	this.rtwnameHashMap["<S41>/Constant1"] = {sid: "Encoder_Zynq_V24:4207"};
	this.sidHashMap["Encoder_Zynq_V24:4207"] = {rtwname: "<S41>/Constant1"};
	this.rtwnameHashMap["<S41>/Constant2"] = {sid: "Encoder_Zynq_V24:4208"};
	this.sidHashMap["Encoder_Zynq_V24:4208"] = {rtwname: "<S41>/Constant2"};
	this.rtwnameHashMap["<S41>/Constant3"] = {sid: "Encoder_Zynq_V24:4209"};
	this.sidHashMap["Encoder_Zynq_V24:4209"] = {rtwname: "<S41>/Constant3"};
	this.rtwnameHashMap["<S41>/Detec_edge"] = {sid: "Encoder_Zynq_V24:4210"};
	this.sidHashMap["Encoder_Zynq_V24:4210"] = {rtwname: "<S41>/Detec_edge"};
	this.rtwnameHashMap["<S41>/Pipeline"] = {sid: "Encoder_Zynq_V24:4211"};
	this.sidHashMap["Encoder_Zynq_V24:4211"] = {rtwname: "<S41>/Pipeline"};
	this.rtwnameHashMap["<S41>/Pipeline1"] = {sid: "Encoder_Zynq_V24:4212"};
	this.sidHashMap["Encoder_Zynq_V24:4212"] = {rtwname: "<S41>/Pipeline1"};
	this.rtwnameHashMap["<S41>/ProtectOverflow_T_count"] = {sid: "Encoder_Zynq_V24:4213"};
	this.sidHashMap["Encoder_Zynq_V24:4213"] = {rtwname: "<S41>/ProtectOverflow_T_count"};
	this.rtwnameHashMap["<S41>/ProtectOverflow_rps"] = {sid: "Encoder_Zynq_V24:4214"};
	this.sidHashMap["Encoder_Zynq_V24:4214"] = {rtwname: "<S41>/ProtectOverflow_rps"};
	this.rtwnameHashMap["<S41>/ProtectToUseOmegaIfThetaJump1"] = {sid: "Encoder_Zynq_V24:4215"};
	this.sidHashMap["Encoder_Zynq_V24:4215"] = {rtwname: "<S41>/ProtectToUseOmegaIfThetaJump1"};
	this.rtwnameHashMap["<S41>/Scope"] = {sid: "Encoder_Zynq_V24:4216"};
	this.sidHashMap["Encoder_Zynq_V24:4216"] = {rtwname: "<S41>/Scope"};
	this.rtwnameHashMap["<S41>/T_count"] = {sid: "Encoder_Zynq_V24:4217"};
	this.sidHashMap["Encoder_Zynq_V24:4217"] = {rtwname: "<S41>/T_count"};
	this.rtwnameHashMap["<S41>/T_count_switch"] = {sid: "Encoder_Zynq_V24:4218"};
	this.sidHashMap["Encoder_Zynq_V24:4218"] = {rtwname: "<S41>/T_count_switch"};
	this.rtwnameHashMap["<S41>/count_old1"] = {sid: "Encoder_Zynq_V24:4219"};
	this.sidHashMap["Encoder_Zynq_V24:4219"] = {rtwname: "<S41>/count_old1"};
	this.rtwnameHashMap["<S41>/reset_old1"] = {sid: "Encoder_Zynq_V24:4220"};
	this.sidHashMap["Encoder_Zynq_V24:4220"] = {rtwname: "<S41>/reset_old1"};
	this.rtwnameHashMap["<S41>/reset_old_switch1"] = {sid: "Encoder_Zynq_V24:4221"};
	this.sidHashMap["Encoder_Zynq_V24:4221"] = {rtwname: "<S41>/reset_old_switch1"};
	this.rtwnameHashMap["<S41>/speed_old"] = {sid: "Encoder_Zynq_V24:4222"};
	this.sidHashMap["Encoder_Zynq_V24:4222"] = {rtwname: "<S41>/speed_old"};
	this.rtwnameHashMap["<S41>/speed_old_switch"] = {sid: "Encoder_Zynq_V24:4223"};
	this.sidHashMap["Encoder_Zynq_V24:4223"] = {rtwname: "<S41>/speed_old_switch"};
	this.rtwnameHashMap["<S41>/speed_old_switch2"] = {sid: "Encoder_Zynq_V24:4224"};
	this.sidHashMap["Encoder_Zynq_V24:4224"] = {rtwname: "<S41>/speed_old_switch2"};
	this.rtwnameHashMap["<S41>/one_Omega"] = {sid: "Encoder_Zynq_V24:4225"};
	this.sidHashMap["Encoder_Zynq_V24:4225"] = {rtwname: "<S41>/one_Omega"};
	this.rtwnameHashMap["<S41>/NewMeasurement"] = {sid: "Encoder_Zynq_V24:4226"};
	this.sidHashMap["Encoder_Zynq_V24:4226"] = {rtwname: "<S41>/NewMeasurement"};
	this.rtwnameHashMap["<S42>/u"] = {sid: "Encoder_Zynq_V24:4196:1"};
	this.sidHashMap["Encoder_Zynq_V24:4196:1"] = {rtwname: "<S42>/u"};
	this.rtwnameHashMap["<S42>/Compare"] = {sid: "Encoder_Zynq_V24:4196:2"};
	this.sidHashMap["Encoder_Zynq_V24:4196:2"] = {rtwname: "<S42>/Compare"};
	this.rtwnameHashMap["<S42>/Constant"] = {sid: "Encoder_Zynq_V24:4196:3"};
	this.sidHashMap["Encoder_Zynq_V24:4196:3"] = {rtwname: "<S42>/Constant"};
	this.rtwnameHashMap["<S42>/y"] = {sid: "Encoder_Zynq_V24:4196:4"};
	this.sidHashMap["Encoder_Zynq_V24:4196:4"] = {rtwname: "<S42>/y"};
	this.rtwnameHashMap["<S43>:1"] = {sid: "Encoder_Zynq_V24:4666:1"};
	this.sidHashMap["Encoder_Zynq_V24:4666:1"] = {rtwname: "<S43>:1"};
	this.rtwnameHashMap["<S43>:1:20"] = {sid: "Encoder_Zynq_V24:4666:1:20"};
	this.sidHashMap["Encoder_Zynq_V24:4666:1:20"] = {rtwname: "<S43>:1:20"};
	this.rtwnameHashMap["<S43>:1:21"] = {sid: "Encoder_Zynq_V24:4666:1:21"};
	this.sidHashMap["Encoder_Zynq_V24:4666:1:21"] = {rtwname: "<S43>:1:21"};
	this.rtwnameHashMap["<S43>:1:22"] = {sid: "Encoder_Zynq_V24:4666:1:22"};
	this.sidHashMap["Encoder_Zynq_V24:4666:1:22"] = {rtwname: "<S43>:1:22"};
	this.rtwnameHashMap["<S43>:1:23"] = {sid: "Encoder_Zynq_V24:4666:1:23"};
	this.sidHashMap["Encoder_Zynq_V24:4666:1:23"] = {rtwname: "<S43>:1:23"};
	this.rtwnameHashMap["<S43>:1:24"] = {sid: "Encoder_Zynq_V24:4666:1:24"};
	this.sidHashMap["Encoder_Zynq_V24:4666:1:24"] = {rtwname: "<S43>:1:24"};
	this.rtwnameHashMap["<S43>:1:25"] = {sid: "Encoder_Zynq_V24:4666:1:25"};
	this.sidHashMap["Encoder_Zynq_V24:4666:1:25"] = {rtwname: "<S43>:1:25"};
	this.rtwnameHashMap["<S43>:1:27"] = {sid: "Encoder_Zynq_V24:4666:1:27"};
	this.sidHashMap["Encoder_Zynq_V24:4666:1:27"] = {rtwname: "<S43>:1:27"};
	this.rtwnameHashMap["<S43>:1:28"] = {sid: "Encoder_Zynq_V24:4666:1:28"};
	this.sidHashMap["Encoder_Zynq_V24:4666:1:28"] = {rtwname: "<S43>:1:28"};
	this.rtwnameHashMap["<S43>:1:32"] = {sid: "Encoder_Zynq_V24:4666:1:32"};
	this.sidHashMap["Encoder_Zynq_V24:4666:1:32"] = {rtwname: "<S43>:1:32"};
	this.rtwnameHashMap["<S43>:1:33"] = {sid: "Encoder_Zynq_V24:4666:1:33"};
	this.sidHashMap["Encoder_Zynq_V24:4666:1:33"] = {rtwname: "<S43>:1:33"};
	this.getSID = function(rtwname) { return this.rtwnameHashMap[rtwname];}
	this.getRtwname = function(sid) { return this.sidHashMap[sid];}
}
RTW_rtwnameSIDMap.instance = new RTW_rtwnameSIDMap();
