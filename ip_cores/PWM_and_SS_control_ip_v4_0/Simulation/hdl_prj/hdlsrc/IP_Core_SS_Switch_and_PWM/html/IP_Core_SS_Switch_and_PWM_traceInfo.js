function RTW_Sid2UrlHash() {
	this.urlHashMap = new Array();
	/* <S1>/Mode_AXI */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:194"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:194";
	/* <S1>/GenPWM */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:371"] = "PWM_and_SS_control_V4_ip_src_PWM_and_Switching_Signal_Control.vhd:228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259";
	/* <S1>/Switch7 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:221"] = "PWM_and_SS_control_V4_ip_src_PWM_and_Switching_Signal_Control.vhd:296,297";
	/* <S1>/Switch8 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:222"] = "PWM_and_SS_control_V4_ip_src_PWM_and_Switching_Signal_Control.vhd:304,305";
	/* <S1>/Switch9 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:223"] = "PWM_and_SS_control_V4_ip_src_PWM_and_Switching_Signal_Control.vhd:312,313";
	/* <S1>/VSI Control Signal Switch */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:8"] = "PWM_and_SS_control_V4_ip_src_PWM_and_Switching_Signal_Control.vhd:261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287";
	/* <S3>/PWM_en_AXI */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:372"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:372";
	/* <S3>/f_carrier_kHz_AXI */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:373"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:373";
	/* <S3>/T_carrier_us_AXI */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:374"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:374";
	/* <S3>/min_pulse_width_AXI */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:375"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:375";
	/* <S3>/Constant */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:379"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:798";
	/* <S3>/Constant1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:380"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:498,499";
	/* <S3>/Constant10 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:381"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:341,342";
	/* <S3>/Constant11 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:382"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:800";
	/* <S3>/Constant2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:383"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:501,502";
	/* <S3>/Constant4 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:384"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:600,601";
	/* <S3>/Constant5 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:385"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:603,604";
	/* <S3>/Constant7 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:386"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:702,703";
	/* <S3>/Constant8 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:387"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:705,706";
	/* <S3>/Counter Ctrl */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:388"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:267,268,269,270,271,272,273,274,275,276,277,278";
	/* <S3>/Delay */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:417"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:456,457,458,459,460,461,462,463,464,465";
	/* <S3>/Delay1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:418"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:572,573,574,575,576,577,578,579,580,581";
	/* <S3>/Delay2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:419"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:674,675,676,677,678,679,680,681,682,683";
	/* <S3>/Delay4 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:612"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:408,409,410,411,412,413,414,415,416,417,418,420";
	/* <S3>/Demux */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:420"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:512";
	/* <S3>/Demux1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:421"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:614";
	/* <S3>/Demux2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:422"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:716";
	/* <S3>/HDL Counter2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:387,388,389,390,391,392,393,394,395,396,398,400,401,402";
	/* <S3>/LimitPulseWidth */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:426"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:289,290,291,292,293";
	/* <S3>/LimitPulseWidth1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:441"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:304,305,306,307,308";
	/* <S3>/LimitPulseWidth2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:456"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:319,320,321,322,323";
	/* <S3>/MinOrMax */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:550"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:440";
	/* <S3>/Relational
Operator */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:471"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:491,492";
	/* <S3>/Relational
Operator1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:472"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:593,594";
	/* <S3>/Relational
Operator2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:473"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:695,696";
	/* <S3>/Scope */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:475"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:475";
	/* <S3>/Scope10 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:476"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:476";
	/* <S3>/Scope2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:477"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:477";
	/* <S3>/Scope4 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:479"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:479";
	/* <S3>/Switch */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:480"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:469,470";
	/* <S3>/Switch1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:481"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:585,586";
	/* <S3>/Switch10 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:563"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:423,424";
	/* <S3>/Switch2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:482"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:687,688";
	/* <S3>/Switch3 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:483"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:509,510";
	/* <S3>/Switch4 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:484"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:611,612";
	/* <S3>/Switch5 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:485"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:713,714";
	/* <S3>/Switch6 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:486"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:803,804";
	/* <S3>/Switch7 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:487"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:505,506";
	/* <S3>/Switch8 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:488"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:607,608";
	/* <S3>/Switch9 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:489"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:709,710";
	/* <S3>/Triangle shift */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1733"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:280,281,282,283,284,285,286,287";
	/* <S3>/Triangle shift1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3639"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:295,296,297,298,299,300,301,302";
	/* <S3>/Triangle shift2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3718"] = "PWM_and_SS_control_V4_ip_src_GenPWM.vhd:310,311,312,313,314,315,316,317";
	/* <S4>/Constant */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:531"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:155";
	/* <S4>/Switch */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:152,153";
	/* <S4>/Switch1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:2"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:180,181";
	/* <S4>/Switch10 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:529"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:294,295";
	/* <S4>/Switch11 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:530"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:320,321";
	/* <S4>/Switch2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:222,223";
	/* <S4>/Switch3 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:4"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:248,249";
	/* <S4>/Switch4 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:5"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:290,291";
	/* <S4>/Switch5 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:6"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:316,317";
	/* <S4>/Switch6 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:525"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:158,159";
	/* <S4>/Switch7 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:526"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:184,185";
	/* <S4>/Switch8 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:527"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:226,227";
	/* <S4>/Switch9 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:528"] = "PWM_and_SS_control_V4_ip_src_VSI_Control_Signal_Switch.vhd:252,253";
	/* <S5>/Constant1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:542"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:142";
	/* <S5>/Constant11 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:392"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:79";
	/* <S5>/Constant12 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:393"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:97";
	/* <S5>/Constant14 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:394"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:95";
	/* <S5>/Delay */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:395"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:121,122,123,124,125,126,127,128,129,130";
	/* <S5>/Delay6 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:396"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:103,104,105,106,107,108,109,110,111,112";
	/* <S5>/Logical
Operator */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:399"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:85";
	/* <S5>/Product */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:400"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:133,134";
	/* <S5>/Relational
Operator1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:510"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:139,140";
	/* <S5>/Relational
Operator2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:541"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:145,146";
	/* <S5>/Relational
Operator4 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:401"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:76,77";
	/* <S5>/Relational
Operator5 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:402"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:82,83";
	/* <S5>/Scope9 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:407"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:407";
	/* <S5>/Switch6 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:408"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:116,117";
	/* <S5>/Switch7 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:409"] = "PWM_and_SS_control_V4_ip_src_Counter_Ctrl.vhd:100,101";
	/* <S6>/Constant */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:212"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:212";
	/* <S6>/Count_reg */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:9"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:9";
	/* <S6>/DT_convert */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:10"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:10";
	/* <S6>/DT_convert1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:11"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:11";
	/* <S6>/Free_running_or_modulo */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:17"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:17";
	/* <S6>/From_value */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:19"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:19";
	/* <S6>/Init_value */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:18"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:18";
	/* <S6>/Relational
Operator */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:211"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:211";
	/* <S6>/Step_value */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:21"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:21";
	/* <S6>/Switch_dir */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:30"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:30";
	/* <S6>/Switch_enb */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:31"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:31";
	/* <S6>/Switch_load */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:32"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:32";
	/* <S6>/Switch_max */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:33"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:33";
	/* <S6>/Switch_reset */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:34"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:34";
	/* <S6>/Switch_type */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:35"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:35";
	/* <S6>/const_load */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:38"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:38";
	/* <S6>/const_load_val */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:39"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:39";
	/* <S6>/const_rst */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:40"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:40";
	/* <S7>/Add */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:429"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth.vhd:60";
	/* <S7>/Constant */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:430"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth.vhd:56";
	/* <S7>/Constant1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:431"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth.vhd:87";
	/* <S7>/Constant2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:432"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth.vhd:91";
	/* <S7>/Logical
Operator */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:433"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth.vhd:77";
	/* <S7>/Relational
Operator */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:434"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth.vhd:62,64,65";
	/* <S7>/Relational
Operator1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:435"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth.vhd:72,74,75";
	/* <S7>/Saturation */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:436"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth.vhd:68,69,70";
	/* <S7>/Switch1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:437"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth.vhd:94,95";
	/* <S7>/Switch6 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:438"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth.vhd:98,99";
	/* <S8>/Add */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:444"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth1.vhd:60";
	/* <S8>/Constant */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:445"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth1.vhd:56";
	/* <S8>/Constant1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:446"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth1.vhd:87";
	/* <S8>/Constant2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:447"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth1.vhd:91";
	/* <S8>/Logical
Operator */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:448"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth1.vhd:77";
	/* <S8>/Relational
Operator */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:449"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth1.vhd:62,64,65";
	/* <S8>/Relational
Operator1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:450"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth1.vhd:72,74,75";
	/* <S8>/Saturation */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:451"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth1.vhd:68,69,70";
	/* <S8>/Switch1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:452"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth1.vhd:94,95";
	/* <S8>/Switch6 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:453"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth1.vhd:98,99";
	/* <S9>/Add */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:459"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth2.vhd:60";
	/* <S9>/Constant */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:460"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth2.vhd:56";
	/* <S9>/Constant1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:461"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth2.vhd:87";
	/* <S9>/Constant2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:462"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth2.vhd:91";
	/* <S9>/Logical
Operator */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:463"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth2.vhd:77";
	/* <S9>/Relational
Operator */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:464"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth2.vhd:62,64,65";
	/* <S9>/Relational
Operator1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:465"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth2.vhd:72,74,75";
	/* <S9>/Saturation */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:466"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth2.vhd:68,69,70";
	/* <S9>/Switch1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:467"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth2.vhd:94,95";
	/* <S9>/Switch6 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:468"] = "PWM_and_SS_control_V4_ip_src_LimitPulseWidth2.vhd:98,99";
	/* <S10>/0 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:2372"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift.vhd:250";
	/* <S10>/Data Type Conversion */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1804"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift.vhd:218";
	/* <S10>/Data Type Conversion1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:2354"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift.vhd:214";
	/* <S10>/Delay1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:2358"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift.vhd:252,253,254,255,256,257,258,259,260,261";
	/* <S10>/Delay2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1739"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift.vhd:252,253,254,255,256,257,258,259,260,261";
	/* <S10>/Gain */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1802"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift.vhd:220,221";
	/* <S10>/Gain1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1805"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift.vhd:229,230";
	/* <S10>/Relational
Operator1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:2357"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift.vhd:265,266";
	/* <S10>/Relational
Operator7 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1749"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift.vhd:271,272";
	/* <S10>/Subsystem */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3593"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift.vhd:192,193,194,195,196,197,198,199,200,201,202,203";
	/* <S10>/Subsystem1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3603"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift.vhd:178,179,180,181,182,183,184,185,186,187,188,189,190";
	/* <S10>/Subsystem2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3615"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift.vhd:165,166,167,168,169,170,171,172,173,174,175,176";
	/* <S10>/Subsystem3 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3624"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift.vhd:205,206,207,208,209,210";
	/* <S10>/Sum10 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1784"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift.vhd:244";
	/* <S10>/or */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:2360"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift.vhd:289,290";
	/* <S10>/standardized peak of triangle */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1799"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift.vhd:227";
	/* <S10>/sw1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1769"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift.vhd:317,318";
	/* <S10>/sw2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1770"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift.vhd:307,308";
	/* <S10>/sw3 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1771"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift.vhd:303,304";
	/* <S10>/sw4 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1785"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift.vhd:247,248";
	/* <S11>/0 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3642"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift1.vhd:250";
	/* <S11>/Data Type Conversion */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3643"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift1.vhd:218";
	/* <S11>/Data Type Conversion1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3644"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift1.vhd:214";
	/* <S11>/Delay1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3645"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift1.vhd:252,253,254,255,256,257,258,259,260,261";
	/* <S11>/Delay2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3646"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift1.vhd:252,253,254,255,256,257,258,259,260,261";
	/* <S11>/Gain */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3647"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift1.vhd:220,221";
	/* <S11>/Gain1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3648"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift1.vhd:229,230";
	/* <S11>/Relational
Operator1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3649"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift1.vhd:265,266";
	/* <S11>/Relational
Operator7 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3650"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift1.vhd:271,272";
	/* <S11>/Subsystem */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3651"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift1.vhd:192,193,194,195,196,197,198,199,200,201,202,203";
	/* <S11>/Subsystem1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3665"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift1.vhd:178,179,180,181,182,183,184,185,186,187,188,189,190";
	/* <S11>/Subsystem2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3681"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift1.vhd:165,166,167,168,169,170,171,172,173,174,175,176";
	/* <S11>/Subsystem3 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3693"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift1.vhd:205,206,207,208,209,210";
	/* <S11>/Sum10 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3704"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift1.vhd:244";
	/* <S11>/or */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3705"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift1.vhd:289,290";
	/* <S11>/standardized peak of triangle */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3706"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift1.vhd:227";
	/* <S11>/sw1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3707"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift1.vhd:317,318";
	/* <S11>/sw2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3708"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift1.vhd:307,308";
	/* <S11>/sw3 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3709"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift1.vhd:303,304";
	/* <S11>/sw4 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3710"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift1.vhd:247,248";
	/* <S12>/0 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3721"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift2.vhd:250";
	/* <S12>/Data Type Conversion */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3722"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift2.vhd:218";
	/* <S12>/Data Type Conversion1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3723"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift2.vhd:214";
	/* <S12>/Delay1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3724"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift2.vhd:252,253,254,255,256,257,258,259,260,261";
	/* <S12>/Delay2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3725"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift2.vhd:252,253,254,255,256,257,258,259,260,261";
	/* <S12>/Gain */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3726"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift2.vhd:220,221";
	/* <S12>/Gain1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3727"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift2.vhd:229,230";
	/* <S12>/Relational
Operator1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3728"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift2.vhd:265,266";
	/* <S12>/Relational
Operator7 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3729"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift2.vhd:271,272";
	/* <S12>/Subsystem */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3730"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift2.vhd:192,193,194,195,196,197,198,199,200,201,202,203";
	/* <S12>/Subsystem1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3744"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift2.vhd:178,179,180,181,182,183,184,185,186,187,188,189,190";
	/* <S12>/Subsystem2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3760"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift2.vhd:165,166,167,168,169,170,171,172,173,174,175,176";
	/* <S12>/Subsystem3 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3772"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift2.vhd:205,206,207,208,209,210";
	/* <S12>/Sum10 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3783"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift2.vhd:244";
	/* <S12>/or */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3784"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift2.vhd:289,290";
	/* <S12>/standardized peak of triangle */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3785"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift2.vhd:227";
	/* <S12>/sw1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3786"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift2.vhd:317,318";
	/* <S12>/sw2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3787"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift2.vhd:307,308";
	/* <S12>/sw3 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3788"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift2.vhd:303,304";
	/* <S12>/sw4 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3789"] = "PWM_and_SS_control_V4_ip_src_Triangle_shift2.vhd:247,248";
	/* <S13>/Add */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:4"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:4";
	/* <S13>/Mod_value */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:5"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:5";
	/* <S13>/Switch_wrap */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:6"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:6";
	/* <S13>/Wrap */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:7"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:7";
	/* <S14>/Logical
Operator */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:14"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:14";
	/* <S14>/Pos_step */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:15"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:15";
	/* <S15>/Add */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:25"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:25";
	/* <S15>/Mod_value */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:26"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:26";
	/* <S15>/Switch_wrap */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:27"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:27";
	/* <S15>/Wrap */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:28"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:28";
	/* <S16>/Compare */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:215:2"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:215:2";
	/* <S16>/Constant */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:215:3"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:215:3";
	/* <S17>/Compare */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:213:2"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:213:2";
	/* <S17>/Constant */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:425:213:3"] = "msg=rtwMsg_notTraceable&block=IP_Core_SS_Switch_and_PWM:425:213:3";
	/* <S18>/Relational
Operator11 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1745"] = "PWM_and_SS_control_V4_ip_src_Subsystem.vhd:92,93";
	/* <S18>/Sum8 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1761"] = "PWM_and_SS_control_V4_ip_src_Subsystem.vhd:69,70,71";
	/* <S18>/Unary Minus */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1764"] = "PWM_and_SS_control_V4_ip_src_Subsystem.vhd:63,64,65";
	/* <S18>/and2_1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:2374"] = "PWM_and_SS_control_V4_ip_src_Subsystem.vhd:106,107";
	/* <S18>/and2_2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:2375"] = "PWM_and_SS_control_V4_ip_src_Subsystem.vhd:110,111";
	/* <S19>/Relational
Operator2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1746"] = "PWM_and_SS_control_V4_ip_src_Subsystem1.vhd:119,121,122";
	/* <S19>/Sum11 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:2361"] = "PWM_and_SS_control_V4_ip_src_Subsystem1.vhd:97,98,99";
	/* <S19>/Sum6 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1759"] = "PWM_and_SS_control_V4_ip_src_Subsystem1.vhd:87,88,89";
	/* <S19>/and1_1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:2370"] = "PWM_and_SS_control_V4_ip_src_Subsystem1.vhd:135,136";
	/* <S19>/and1_2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:2371"] = "PWM_and_SS_control_V4_ip_src_Subsystem1.vhd:139,140";
	/* <S20>/Relational
Operator9 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1751"] = "PWM_and_SS_control_V4_ip_src_Subsystem2.vhd:108,110,111";
	/* <S20>/Sum1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1754"] = "PWM_and_SS_control_V4_ip_src_Subsystem2.vhd:76,77,78";
	/* <S20>/Sum5 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1758"] = "PWM_and_SS_control_V4_ip_src_Subsystem2.vhd:86,87,88";
	/* <S20>/and */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:2376"] = "PWM_and_SS_control_V4_ip_src_Subsystem2.vhd:114,115";
	/* <S21>/Relational
Operator6 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1748"] = "PWM_and_SS_control_V4_ip_src_Subsystem3.vhd:61,62";
	/* <S21>/Sum2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1755"] = "PWM_and_SS_control_V4_ip_src_Subsystem3.vhd:76,77,78";
	/* <S21>/Sum3 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1756"] = "PWM_and_SS_control_V4_ip_src_Subsystem3.vhd:68,69,70";
	/* <S21>/Sum4 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1757"] = "PWM_and_SS_control_V4_ip_src_Subsystem3.vhd:72,73,74";
	/* <S21>/Switch3 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1763"] = "PWM_and_SS_control_V4_ip_src_Subsystem3.vhd:81,82";
	/* <S21>/sw */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:1768"] = "PWM_and_SS_control_V4_ip_src_Subsystem3.vhd:85,86";
	/* <S22>/Relational
Operator11 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3657"] = "PWM_and_SS_control_V4_ip_src_Subsystem_block.vhd:92,93";
	/* <S22>/Sum8 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3658"] = "PWM_and_SS_control_V4_ip_src_Subsystem_block.vhd:69,70,71";
	/* <S22>/Unary Minus */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3659"] = "PWM_and_SS_control_V4_ip_src_Subsystem_block.vhd:63,64,65";
	/* <S22>/and2_1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3660"] = "PWM_and_SS_control_V4_ip_src_Subsystem_block.vhd:106,107";
	/* <S22>/and2_2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3661"] = "PWM_and_SS_control_V4_ip_src_Subsystem_block.vhd:110,111";
	/* <S23>/Relational
Operator2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3672"] = "PWM_and_SS_control_V4_ip_src_Subsystem1_block.vhd:119,121,122";
	/* <S23>/Sum11 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3673"] = "PWM_and_SS_control_V4_ip_src_Subsystem1_block.vhd:97,98,99";
	/* <S23>/Sum6 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3674"] = "PWM_and_SS_control_V4_ip_src_Subsystem1_block.vhd:87,88,89";
	/* <S23>/and1_1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3675"] = "PWM_and_SS_control_V4_ip_src_Subsystem1_block.vhd:135,136";
	/* <S23>/and1_2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3676"] = "PWM_and_SS_control_V4_ip_src_Subsystem1_block.vhd:139,140";
	/* <S24>/Relational
Operator9 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3687"] = "PWM_and_SS_control_V4_ip_src_Subsystem2_block.vhd:108,110,111";
	/* <S24>/Sum1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3688"] = "PWM_and_SS_control_V4_ip_src_Subsystem2_block.vhd:76,77,78";
	/* <S24>/Sum5 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3689"] = "PWM_and_SS_control_V4_ip_src_Subsystem2_block.vhd:86,87,88";
	/* <S24>/and */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3690"] = "PWM_and_SS_control_V4_ip_src_Subsystem2_block.vhd:114,115";
	/* <S25>/Relational
Operator6 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3697"] = "PWM_and_SS_control_V4_ip_src_Subsystem3_block.vhd:61,62";
	/* <S25>/Sum2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3698"] = "PWM_and_SS_control_V4_ip_src_Subsystem3_block.vhd:76,77,78";
	/* <S25>/Sum3 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3699"] = "PWM_and_SS_control_V4_ip_src_Subsystem3_block.vhd:68,69,70";
	/* <S25>/Sum4 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3700"] = "PWM_and_SS_control_V4_ip_src_Subsystem3_block.vhd:72,73,74";
	/* <S25>/Switch3 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3701"] = "PWM_and_SS_control_V4_ip_src_Subsystem3_block.vhd:81,82";
	/* <S25>/sw */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3702"] = "PWM_and_SS_control_V4_ip_src_Subsystem3_block.vhd:85,86";
	/* <S26>/Relational
Operator11 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3736"] = "PWM_and_SS_control_V4_ip_src_Subsystem_block1.vhd:92,93";
	/* <S26>/Sum8 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3737"] = "PWM_and_SS_control_V4_ip_src_Subsystem_block1.vhd:69,70,71";
	/* <S26>/Unary Minus */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3738"] = "PWM_and_SS_control_V4_ip_src_Subsystem_block1.vhd:63,64,65";
	/* <S26>/and2_1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3739"] = "PWM_and_SS_control_V4_ip_src_Subsystem_block1.vhd:106,107";
	/* <S26>/and2_2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3740"] = "PWM_and_SS_control_V4_ip_src_Subsystem_block1.vhd:110,111";
	/* <S27>/Relational
Operator2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3751"] = "PWM_and_SS_control_V4_ip_src_Subsystem1_block1.vhd:119,121,122";
	/* <S27>/Sum11 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3752"] = "PWM_and_SS_control_V4_ip_src_Subsystem1_block1.vhd:97,98,99";
	/* <S27>/Sum6 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3753"] = "PWM_and_SS_control_V4_ip_src_Subsystem1_block1.vhd:87,88,89";
	/* <S27>/and1_1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3754"] = "PWM_and_SS_control_V4_ip_src_Subsystem1_block1.vhd:135,136";
	/* <S27>/and1_2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3755"] = "PWM_and_SS_control_V4_ip_src_Subsystem1_block1.vhd:139,140";
	/* <S28>/Relational
Operator9 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3766"] = "PWM_and_SS_control_V4_ip_src_Subsystem2_block1.vhd:108,110,111";
	/* <S28>/Sum1 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3767"] = "PWM_and_SS_control_V4_ip_src_Subsystem2_block1.vhd:76,77,78";
	/* <S28>/Sum5 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3768"] = "PWM_and_SS_control_V4_ip_src_Subsystem2_block1.vhd:86,87,88";
	/* <S28>/and */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3769"] = "PWM_and_SS_control_V4_ip_src_Subsystem2_block1.vhd:114,115";
	/* <S29>/Relational
Operator6 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3776"] = "PWM_and_SS_control_V4_ip_src_Subsystem3_block1.vhd:61,62";
	/* <S29>/Sum2 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3777"] = "PWM_and_SS_control_V4_ip_src_Subsystem3_block1.vhd:76,77,78";
	/* <S29>/Sum3 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3778"] = "PWM_and_SS_control_V4_ip_src_Subsystem3_block1.vhd:68,69,70";
	/* <S29>/Sum4 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3779"] = "PWM_and_SS_control_V4_ip_src_Subsystem3_block1.vhd:72,73,74";
	/* <S29>/Switch3 */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3780"] = "PWM_and_SS_control_V4_ip_src_Subsystem3_block1.vhd:81,82";
	/* <S29>/sw */
	this.urlHashMap["IP_Core_SS_Switch_and_PWM:3781"] = "PWM_and_SS_control_V4_ip_src_Subsystem3_block1.vhd:85,86";
	this.getUrlHash = function(sid) { return this.urlHashMap[sid];}
}
RTW_Sid2UrlHash.instance = new RTW_Sid2UrlHash();
function RTW_rtwnameSIDMap() {
	this.rtwnameHashMap = new Array();
	this.sidHashMap = new Array();
	this.rtwnameHashMap["<Root>"] = {sid: "IP_Core_SS_Switch_and_PWM"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM"] = {rtwname: "<Root>"};
	this.rtwnameHashMap["<S1>/PWM_en_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:193"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:193"] = {rtwname: "<S1>/PWM_en_AXI"};
	this.rtwnameHashMap["<S1>/Mode_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:194"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:194"] = {rtwname: "<S1>/Mode_AXI"};
	this.rtwnameHashMap["<S1>/Scal_f_carrier_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:195"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:195"] = {rtwname: "<S1>/Scal_f_carrier_AXI"};
	this.rtwnameHashMap["<S1>/Scal_T_carrier_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:502"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:502"] = {rtwname: "<S1>/Scal_T_carrier_AXI"};
	this.rtwnameHashMap["<S1>/PWM_min_pulse_width_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:196"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:196"] = {rtwname: "<S1>/PWM_min_pulse_width_AXI"};
	this.rtwnameHashMap["<S1>/m_u1_norm"] = {sid: "IP_Core_SS_Switch_and_PWM:197"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:197"] = {rtwname: "<S1>/m_u1_norm"};
	this.rtwnameHashMap["<S1>/m_u2_norm"] = {sid: "IP_Core_SS_Switch_and_PWM:198"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:198"] = {rtwname: "<S1>/m_u2_norm"};
	this.rtwnameHashMap["<S1>/m_u3_norm"] = {sid: "IP_Core_SS_Switch_and_PWM:199"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:199"] = {rtwname: "<S1>/m_u3_norm"};
	this.rtwnameHashMap["<S1>/m_u1_norm_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:201"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:201"] = {rtwname: "<S1>/m_u1_norm_AXI"};
	this.rtwnameHashMap["<S1>/m_u2_norm_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:203"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:203"] = {rtwname: "<S1>/m_u2_norm_AXI"};
	this.rtwnameHashMap["<S1>/m_u3_norm_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:205"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:205"] = {rtwname: "<S1>/m_u3_norm_AXI"};
	this.rtwnameHashMap["<S1>/SS0_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:200"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:200"] = {rtwname: "<S1>/SS0_IN_External"};
	this.rtwnameHashMap["<S1>/SS1_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:202"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:202"] = {rtwname: "<S1>/SS1_IN_External"};
	this.rtwnameHashMap["<S1>/SS2_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:204"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:204"] = {rtwname: "<S1>/SS2_IN_External"};
	this.rtwnameHashMap["<S1>/SS3_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:206"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:206"] = {rtwname: "<S1>/SS3_IN_External"};
	this.rtwnameHashMap["<S1>/SS4_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:207"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:207"] = {rtwname: "<S1>/SS4_IN_External"};
	this.rtwnameHashMap["<S1>/SS5_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:208"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:208"] = {rtwname: "<S1>/SS5_IN_External"};
	this.rtwnameHashMap["<S1>/TriState_HB1_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:514"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:514"] = {rtwname: "<S1>/TriState_HB1_AXI"};
	this.rtwnameHashMap["<S1>/TriState_HB2_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:515"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:515"] = {rtwname: "<S1>/TriState_HB2_AXI"};
	this.rtwnameHashMap["<S1>/TriState_HB3_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:516"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:516"] = {rtwname: "<S1>/TriState_HB3_AXI"};
	this.rtwnameHashMap["<S1>/triangle_in"] = {sid: "IP_Core_SS_Switch_and_PWM:566"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:566"] = {rtwname: "<S1>/triangle_in"};
	this.rtwnameHashMap["<S1>/count_src_ext_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:605"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:605"] = {rtwname: "<S1>/count_src_ext_AXI"};
	this.rtwnameHashMap["<S1>/triangle_shift_HB1_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:1781"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1781"] = {rtwname: "<S1>/triangle_shift_HB1_AXI"};
	this.rtwnameHashMap["<S1>/triangle_shift_HB2_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:2460"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:2460"] = {rtwname: "<S1>/triangle_shift_HB2_AXI"};
	this.rtwnameHashMap["<S1>/triangle_shift_HB3_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:2461"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:2461"] = {rtwname: "<S1>/triangle_shift_HB3_AXI"};
	this.rtwnameHashMap["<S1>/GenPWM"] = {sid: "IP_Core_SS_Switch_and_PWM:371"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:371"] = {rtwname: "<S1>/GenPWM"};
	this.rtwnameHashMap["<S1>/Switch7"] = {sid: "IP_Core_SS_Switch_and_PWM:221"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:221"] = {rtwname: "<S1>/Switch7"};
	this.rtwnameHashMap["<S1>/Switch8"] = {sid: "IP_Core_SS_Switch_and_PWM:222"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:222"] = {rtwname: "<S1>/Switch8"};
	this.rtwnameHashMap["<S1>/Switch9"] = {sid: "IP_Core_SS_Switch_and_PWM:223"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:223"] = {rtwname: "<S1>/Switch9"};
	this.rtwnameHashMap["<S1>/VSI Control Signal Switch"] = {sid: "IP_Core_SS_Switch_and_PWM:8"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:8"] = {rtwname: "<S1>/VSI Control Signal Switch"};
	this.rtwnameHashMap["<S1>/SS0_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:209"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:209"] = {rtwname: "<S1>/SS0_OUT"};
	this.rtwnameHashMap["<S1>/SS1_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:210"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:210"] = {rtwname: "<S1>/SS1_OUT"};
	this.rtwnameHashMap["<S1>/SS2_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:211"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:211"] = {rtwname: "<S1>/SS2_OUT"};
	this.rtwnameHashMap["<S1>/SS3_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:212"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:212"] = {rtwname: "<S1>/SS3_OUT"};
	this.rtwnameHashMap["<S1>/SS4_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:213"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:213"] = {rtwname: "<S1>/SS4_OUT"};
	this.rtwnameHashMap["<S1>/SS5_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:214"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:214"] = {rtwname: "<S1>/SS5_OUT"};
	this.rtwnameHashMap["<S1>/PWM_en_rd_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:215"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:215"] = {rtwname: "<S1>/PWM_en_rd_AXI"};
	this.rtwnameHashMap["<S1>/PWM_f_carrier_kHz_rd_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:216"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:216"] = {rtwname: "<S1>/PWM_f_carrier_kHz_rd_AXI"};
	this.rtwnameHashMap["<S1>/PWM_T_carrier_us_rd_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:501"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:501"] = {rtwname: "<S1>/PWM_T_carrier_us_rd_AXI"};
	this.rtwnameHashMap["<S1>/PWM_min_pulse_width_rd_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:217"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:217"] = {rtwname: "<S1>/PWM_min_pulse_width_rd_AXI"};
	this.rtwnameHashMap["<S1>/PWM_enb_out"] = {sid: "IP_Core_SS_Switch_and_PWM:218"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:218"] = {rtwname: "<S1>/PWM_enb_out"};
	this.rtwnameHashMap["<S1>/Mode_rd_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:220"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:220"] = {rtwname: "<S1>/Mode_rd_AXI"};
	this.rtwnameHashMap["<S1>/Triangular_Max"] = {sid: "IP_Core_SS_Switch_and_PWM:512"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:512"] = {rtwname: "<S1>/Triangular_Max"};
	this.rtwnameHashMap["<S1>/Triangular_Min"] = {sid: "IP_Core_SS_Switch_and_PWM:538"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:538"] = {rtwname: "<S1>/Triangular_Min"};
	this.rtwnameHashMap["<S1>/triangle_out"] = {sid: "IP_Core_SS_Switch_and_PWM:567"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:567"] = {rtwname: "<S1>/triangle_out"};
	this.rtwnameHashMap["<S1>/dir_out"] = {sid: "IP_Core_SS_Switch_and_PWM:598"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:598"] = {rtwname: "<S1>/dir_out"};
	this.rtwnameHashMap["<S3>/PWM_en_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:372"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:372"] = {rtwname: "<S3>/PWM_en_AXI"};
	this.rtwnameHashMap["<S3>/f_carrier_kHz_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:373"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:373"] = {rtwname: "<S3>/f_carrier_kHz_AXI"};
	this.rtwnameHashMap["<S3>/T_carrier_us_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:374"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:374"] = {rtwname: "<S3>/T_carrier_us_AXI"};
	this.rtwnameHashMap["<S3>/min_pulse_width_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:375"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:375"] = {rtwname: "<S3>/min_pulse_width_AXI"};
	this.rtwnameHashMap["<S3>/U1_norm"] = {sid: "IP_Core_SS_Switch_and_PWM:376"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:376"] = {rtwname: "<S3>/U1_norm"};
	this.rtwnameHashMap["<S3>/U2_norm"] = {sid: "IP_Core_SS_Switch_and_PWM:377"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:377"] = {rtwname: "<S3>/U2_norm"};
	this.rtwnameHashMap["<S3>/U3_norm"] = {sid: "IP_Core_SS_Switch_and_PWM:378"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:378"] = {rtwname: "<S3>/U3_norm"};
	this.rtwnameHashMap["<S3>/triangle_in"] = {sid: "IP_Core_SS_Switch_and_PWM:564"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:564"] = {rtwname: "<S3>/triangle_in"};
	this.rtwnameHashMap["<S3>/count_src_ext_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:604"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:604"] = {rtwname: "<S3>/count_src_ext_AXI"};
	this.rtwnameHashMap["<S3>/triangle_shift_HB1_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:1780"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1780"] = {rtwname: "<S3>/triangle_shift_HB1_AXI"};
	this.rtwnameHashMap["<S3>/triangle_shift_HB2_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:2462"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:2462"] = {rtwname: "<S3>/triangle_shift_HB2_AXI"};
	this.rtwnameHashMap["<S3>/triangle_shift_HB3_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:2463"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:2463"] = {rtwname: "<S3>/triangle_shift_HB3_AXI"};
	this.rtwnameHashMap["<S3>/Constant"] = {sid: "IP_Core_SS_Switch_and_PWM:379"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:379"] = {rtwname: "<S3>/Constant"};
	this.rtwnameHashMap["<S3>/Constant1"] = {sid: "IP_Core_SS_Switch_and_PWM:380"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:380"] = {rtwname: "<S3>/Constant1"};
	this.rtwnameHashMap["<S3>/Constant10"] = {sid: "IP_Core_SS_Switch_and_PWM:381"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:381"] = {rtwname: "<S3>/Constant10"};
	this.rtwnameHashMap["<S3>/Constant11"] = {sid: "IP_Core_SS_Switch_and_PWM:382"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:382"] = {rtwname: "<S3>/Constant11"};
	this.rtwnameHashMap["<S3>/Constant2"] = {sid: "IP_Core_SS_Switch_and_PWM:383"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:383"] = {rtwname: "<S3>/Constant2"};
	this.rtwnameHashMap["<S3>/Constant4"] = {sid: "IP_Core_SS_Switch_and_PWM:384"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:384"] = {rtwname: "<S3>/Constant4"};
	this.rtwnameHashMap["<S3>/Constant5"] = {sid: "IP_Core_SS_Switch_and_PWM:385"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:385"] = {rtwname: "<S3>/Constant5"};
	this.rtwnameHashMap["<S3>/Constant7"] = {sid: "IP_Core_SS_Switch_and_PWM:386"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:386"] = {rtwname: "<S3>/Constant7"};
	this.rtwnameHashMap["<S3>/Constant8"] = {sid: "IP_Core_SS_Switch_and_PWM:387"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:387"] = {rtwname: "<S3>/Constant8"};
	this.rtwnameHashMap["<S3>/Counter Ctrl"] = {sid: "IP_Core_SS_Switch_and_PWM:388"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:388"] = {rtwname: "<S3>/Counter Ctrl"};
	this.rtwnameHashMap["<S3>/Data Type Conversion1"] = {sid: "IP_Core_SS_Switch_and_PWM:416"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:416"] = {rtwname: "<S3>/Data Type Conversion1"};
	this.rtwnameHashMap["<S3>/Delay"] = {sid: "IP_Core_SS_Switch_and_PWM:417"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:417"] = {rtwname: "<S3>/Delay"};
	this.rtwnameHashMap["<S3>/Delay1"] = {sid: "IP_Core_SS_Switch_and_PWM:418"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:418"] = {rtwname: "<S3>/Delay1"};
	this.rtwnameHashMap["<S3>/Delay2"] = {sid: "IP_Core_SS_Switch_and_PWM:419"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:419"] = {rtwname: "<S3>/Delay2"};
	this.rtwnameHashMap["<S3>/Delay4"] = {sid: "IP_Core_SS_Switch_and_PWM:612"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:612"] = {rtwname: "<S3>/Delay4"};
	this.rtwnameHashMap["<S3>/Demux"] = {sid: "IP_Core_SS_Switch_and_PWM:420"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:420"] = {rtwname: "<S3>/Demux"};
	this.rtwnameHashMap["<S3>/Demux1"] = {sid: "IP_Core_SS_Switch_and_PWM:421"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:421"] = {rtwname: "<S3>/Demux1"};
	this.rtwnameHashMap["<S3>/Demux2"] = {sid: "IP_Core_SS_Switch_and_PWM:422"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:422"] = {rtwname: "<S3>/Demux2"};
	this.rtwnameHashMap["<S3>/HDL Counter1"] = {sid: "IP_Core_SS_Switch_and_PWM:424"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:424"] = {rtwname: "<S3>/HDL Counter1"};
	this.rtwnameHashMap["<S3>/HDL Counter2"] = {sid: "IP_Core_SS_Switch_and_PWM:425"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425"] = {rtwname: "<S3>/HDL Counter2"};
	this.rtwnameHashMap["<S3>/LimitPulseWidth"] = {sid: "IP_Core_SS_Switch_and_PWM:426"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:426"] = {rtwname: "<S3>/LimitPulseWidth"};
	this.rtwnameHashMap["<S3>/LimitPulseWidth1"] = {sid: "IP_Core_SS_Switch_and_PWM:441"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:441"] = {rtwname: "<S3>/LimitPulseWidth1"};
	this.rtwnameHashMap["<S3>/LimitPulseWidth2"] = {sid: "IP_Core_SS_Switch_and_PWM:456"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:456"] = {rtwname: "<S3>/LimitPulseWidth2"};
	this.rtwnameHashMap["<S3>/MinOrMax"] = {sid: "IP_Core_SS_Switch_and_PWM:550"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:550"] = {rtwname: "<S3>/MinOrMax"};
	this.rtwnameHashMap["<S3>/Relational Operator"] = {sid: "IP_Core_SS_Switch_and_PWM:471"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:471"] = {rtwname: "<S3>/Relational Operator"};
	this.rtwnameHashMap["<S3>/Relational Operator1"] = {sid: "IP_Core_SS_Switch_and_PWM:472"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:472"] = {rtwname: "<S3>/Relational Operator1"};
	this.rtwnameHashMap["<S3>/Relational Operator2"] = {sid: "IP_Core_SS_Switch_and_PWM:473"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:473"] = {rtwname: "<S3>/Relational Operator2"};
	this.rtwnameHashMap["<S3>/Relay1"] = {sid: "IP_Core_SS_Switch_and_PWM:474"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:474"] = {rtwname: "<S3>/Relay1"};
	this.rtwnameHashMap["<S3>/Scope"] = {sid: "IP_Core_SS_Switch_and_PWM:475"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:475"] = {rtwname: "<S3>/Scope"};
	this.rtwnameHashMap["<S3>/Scope10"] = {sid: "IP_Core_SS_Switch_and_PWM:476"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:476"] = {rtwname: "<S3>/Scope10"};
	this.rtwnameHashMap["<S3>/Scope2"] = {sid: "IP_Core_SS_Switch_and_PWM:477"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:477"] = {rtwname: "<S3>/Scope2"};
	this.rtwnameHashMap["<S3>/Scope4"] = {sid: "IP_Core_SS_Switch_and_PWM:479"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:479"] = {rtwname: "<S3>/Scope4"};
	this.rtwnameHashMap["<S3>/Switch"] = {sid: "IP_Core_SS_Switch_and_PWM:480"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:480"] = {rtwname: "<S3>/Switch"};
	this.rtwnameHashMap["<S3>/Switch1"] = {sid: "IP_Core_SS_Switch_and_PWM:481"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:481"] = {rtwname: "<S3>/Switch1"};
	this.rtwnameHashMap["<S3>/Switch10"] = {sid: "IP_Core_SS_Switch_and_PWM:563"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:563"] = {rtwname: "<S3>/Switch10"};
	this.rtwnameHashMap["<S3>/Switch2"] = {sid: "IP_Core_SS_Switch_and_PWM:482"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:482"] = {rtwname: "<S3>/Switch2"};
	this.rtwnameHashMap["<S3>/Switch3"] = {sid: "IP_Core_SS_Switch_and_PWM:483"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:483"] = {rtwname: "<S3>/Switch3"};
	this.rtwnameHashMap["<S3>/Switch4"] = {sid: "IP_Core_SS_Switch_and_PWM:484"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:484"] = {rtwname: "<S3>/Switch4"};
	this.rtwnameHashMap["<S3>/Switch5"] = {sid: "IP_Core_SS_Switch_and_PWM:485"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:485"] = {rtwname: "<S3>/Switch5"};
	this.rtwnameHashMap["<S3>/Switch6"] = {sid: "IP_Core_SS_Switch_and_PWM:486"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:486"] = {rtwname: "<S3>/Switch6"};
	this.rtwnameHashMap["<S3>/Switch7"] = {sid: "IP_Core_SS_Switch_and_PWM:487"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:487"] = {rtwname: "<S3>/Switch7"};
	this.rtwnameHashMap["<S3>/Switch8"] = {sid: "IP_Core_SS_Switch_and_PWM:488"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:488"] = {rtwname: "<S3>/Switch8"};
	this.rtwnameHashMap["<S3>/Switch9"] = {sid: "IP_Core_SS_Switch_and_PWM:489"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:489"] = {rtwname: "<S3>/Switch9"};
	this.rtwnameHashMap["<S3>/Triangle shift"] = {sid: "IP_Core_SS_Switch_and_PWM:1733"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1733"] = {rtwname: "<S3>/Triangle shift"};
	this.rtwnameHashMap["<S3>/Triangle shift1"] = {sid: "IP_Core_SS_Switch_and_PWM:3639"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3639"] = {rtwname: "<S3>/Triangle shift1"};
	this.rtwnameHashMap["<S3>/Triangle shift2"] = {sid: "IP_Core_SS_Switch_and_PWM:3718"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3718"] = {rtwname: "<S3>/Triangle shift2"};
	this.rtwnameHashMap["<S3>/S1"] = {sid: "IP_Core_SS_Switch_and_PWM:490"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:490"] = {rtwname: "<S3>/S1"};
	this.rtwnameHashMap["<S3>/S2"] = {sid: "IP_Core_SS_Switch_and_PWM:491"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:491"] = {rtwname: "<S3>/S2"};
	this.rtwnameHashMap["<S3>/S3"] = {sid: "IP_Core_SS_Switch_and_PWM:492"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:492"] = {rtwname: "<S3>/S3"};
	this.rtwnameHashMap["<S3>/S4"] = {sid: "IP_Core_SS_Switch_and_PWM:493"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:493"] = {rtwname: "<S3>/S4"};
	this.rtwnameHashMap["<S3>/S5"] = {sid: "IP_Core_SS_Switch_and_PWM:494"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:494"] = {rtwname: "<S3>/S5"};
	this.rtwnameHashMap["<S3>/S6"] = {sid: "IP_Core_SS_Switch_and_PWM:495"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:495"] = {rtwname: "<S3>/S6"};
	this.rtwnameHashMap["<S3>/PWM_en_rd_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:496"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:496"] = {rtwname: "<S3>/PWM_en_rd_AXI"};
	this.rtwnameHashMap["<S3>/f_carrier_kHz_rd_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:497"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:497"] = {rtwname: "<S3>/f_carrier_kHz_rd_AXI"};
	this.rtwnameHashMap["<S3>/T_carrier_us_rd_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:498"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:498"] = {rtwname: "<S3>/T_carrier_us_rd_AXI"};
	this.rtwnameHashMap["<S3>/min_pulse_width_rd_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:499"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:499"] = {rtwname: "<S3>/min_pulse_width_rd_AXI"};
	this.rtwnameHashMap["<S3>/enb_out"] = {sid: "IP_Core_SS_Switch_and_PWM:500"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:500"] = {rtwname: "<S3>/enb_out"};
	this.rtwnameHashMap["<S3>/Triangle_Max"] = {sid: "IP_Core_SS_Switch_and_PWM:511"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:511"] = {rtwname: "<S3>/Triangle_Max"};
	this.rtwnameHashMap["<S3>/Triangle_Min"] = {sid: "IP_Core_SS_Switch_and_PWM:539"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:539"] = {rtwname: "<S3>/Triangle_Min"};
	this.rtwnameHashMap["<S3>/triangle_out"] = {sid: "IP_Core_SS_Switch_and_PWM:561"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:561"] = {rtwname: "<S3>/triangle_out"};
	this.rtwnameHashMap["<S3>/dir_out"] = {sid: "IP_Core_SS_Switch_and_PWM:597"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:597"] = {rtwname: "<S3>/dir_out"};
	this.rtwnameHashMap["<S4>/Switch_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:9"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:9"] = {rtwname: "<S4>/Switch_AXI"};
	this.rtwnameHashMap["<S4>/SS0_IN_PWM"] = {sid: "IP_Core_SS_Switch_and_PWM:10"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:10"] = {rtwname: "<S4>/SS0_IN_PWM"};
	this.rtwnameHashMap["<S4>/SS1_IN_PWM"] = {sid: "IP_Core_SS_Switch_and_PWM:12"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:12"] = {rtwname: "<S4>/SS1_IN_PWM"};
	this.rtwnameHashMap["<S4>/SS2_IN_PWM"] = {sid: "IP_Core_SS_Switch_and_PWM:14"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:14"] = {rtwname: "<S4>/SS2_IN_PWM"};
	this.rtwnameHashMap["<S4>/SS3_IN_PWM"] = {sid: "IP_Core_SS_Switch_and_PWM:16"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:16"] = {rtwname: "<S4>/SS3_IN_PWM"};
	this.rtwnameHashMap["<S4>/SS4_IN_PWM"] = {sid: "IP_Core_SS_Switch_and_PWM:18"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:18"] = {rtwname: "<S4>/SS4_IN_PWM"};
	this.rtwnameHashMap["<S4>/SS5_IN_PWM"] = {sid: "IP_Core_SS_Switch_and_PWM:20"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:20"] = {rtwname: "<S4>/SS5_IN_PWM"};
	this.rtwnameHashMap["<S4>/SS0_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:11"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:11"] = {rtwname: "<S4>/SS0_IN_External"};
	this.rtwnameHashMap["<S4>/SS1_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:13"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:13"] = {rtwname: "<S4>/SS1_IN_External"};
	this.rtwnameHashMap["<S4>/SS2_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:15"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:15"] = {rtwname: "<S4>/SS2_IN_External"};
	this.rtwnameHashMap["<S4>/SS3_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:17"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:17"] = {rtwname: "<S4>/SS3_IN_External"};
	this.rtwnameHashMap["<S4>/SS4_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:19"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:19"] = {rtwname: "<S4>/SS4_IN_External"};
	this.rtwnameHashMap["<S4>/SS5_IN_External"] = {sid: "IP_Core_SS_Switch_and_PWM:21"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:21"] = {rtwname: "<S4>/SS5_IN_External"};
	this.rtwnameHashMap["<S4>/TriState_HB1_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:518"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:518"] = {rtwname: "<S4>/TriState_HB1_AXI"};
	this.rtwnameHashMap["<S4>/TriState_HB2_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:519"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:519"] = {rtwname: "<S4>/TriState_HB2_AXI"};
	this.rtwnameHashMap["<S4>/TriState_HB3_AXI"] = {sid: "IP_Core_SS_Switch_and_PWM:520"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:520"] = {rtwname: "<S4>/TriState_HB3_AXI"};
	this.rtwnameHashMap["<S4>/Constant"] = {sid: "IP_Core_SS_Switch_and_PWM:531"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:531"] = {rtwname: "<S4>/Constant"};
	this.rtwnameHashMap["<S4>/Switch"] = {sid: "IP_Core_SS_Switch_and_PWM:1"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1"] = {rtwname: "<S4>/Switch"};
	this.rtwnameHashMap["<S4>/Switch1"] = {sid: "IP_Core_SS_Switch_and_PWM:2"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:2"] = {rtwname: "<S4>/Switch1"};
	this.rtwnameHashMap["<S4>/Switch10"] = {sid: "IP_Core_SS_Switch_and_PWM:529"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:529"] = {rtwname: "<S4>/Switch10"};
	this.rtwnameHashMap["<S4>/Switch11"] = {sid: "IP_Core_SS_Switch_and_PWM:530"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:530"] = {rtwname: "<S4>/Switch11"};
	this.rtwnameHashMap["<S4>/Switch2"] = {sid: "IP_Core_SS_Switch_and_PWM:3"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3"] = {rtwname: "<S4>/Switch2"};
	this.rtwnameHashMap["<S4>/Switch3"] = {sid: "IP_Core_SS_Switch_and_PWM:4"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:4"] = {rtwname: "<S4>/Switch3"};
	this.rtwnameHashMap["<S4>/Switch4"] = {sid: "IP_Core_SS_Switch_and_PWM:5"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:5"] = {rtwname: "<S4>/Switch4"};
	this.rtwnameHashMap["<S4>/Switch5"] = {sid: "IP_Core_SS_Switch_and_PWM:6"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:6"] = {rtwname: "<S4>/Switch5"};
	this.rtwnameHashMap["<S4>/Switch6"] = {sid: "IP_Core_SS_Switch_and_PWM:525"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:525"] = {rtwname: "<S4>/Switch6"};
	this.rtwnameHashMap["<S4>/Switch7"] = {sid: "IP_Core_SS_Switch_and_PWM:526"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:526"] = {rtwname: "<S4>/Switch7"};
	this.rtwnameHashMap["<S4>/Switch8"] = {sid: "IP_Core_SS_Switch_and_PWM:527"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:527"] = {rtwname: "<S4>/Switch8"};
	this.rtwnameHashMap["<S4>/Switch9"] = {sid: "IP_Core_SS_Switch_and_PWM:528"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:528"] = {rtwname: "<S4>/Switch9"};
	this.rtwnameHashMap["<S4>/SS0_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:22"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:22"] = {rtwname: "<S4>/SS0_OUT"};
	this.rtwnameHashMap["<S4>/SS1_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:23"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:23"] = {rtwname: "<S4>/SS1_OUT"};
	this.rtwnameHashMap["<S4>/SS2_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:24"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:24"] = {rtwname: "<S4>/SS2_OUT"};
	this.rtwnameHashMap["<S4>/SS3_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:25"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:25"] = {rtwname: "<S4>/SS3_OUT"};
	this.rtwnameHashMap["<S4>/SS4_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:26"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:26"] = {rtwname: "<S4>/SS4_OUT"};
	this.rtwnameHashMap["<S4>/SS5_OUT"] = {sid: "IP_Core_SS_Switch_and_PWM:27"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:27"] = {rtwname: "<S4>/SS5_OUT"};
	this.rtwnameHashMap["<S5>/hdl_cnt"] = {sid: "IP_Core_SS_Switch_and_PWM:389"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:389"] = {rtwname: "<S5>/hdl_cnt"};
	this.rtwnameHashMap["<S5>/f_carrier_kHz"] = {sid: "IP_Core_SS_Switch_and_PWM:390"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:390"] = {rtwname: "<S5>/f_carrier_kHz"};
	this.rtwnameHashMap["<S5>/T_carrier_us"] = {sid: "IP_Core_SS_Switch_and_PWM:391"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:391"] = {rtwname: "<S5>/T_carrier_us"};
	this.rtwnameHashMap["<S5>/Constant1"] = {sid: "IP_Core_SS_Switch_and_PWM:542"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:542"] = {rtwname: "<S5>/Constant1"};
	this.rtwnameHashMap["<S5>/Constant11"] = {sid: "IP_Core_SS_Switch_and_PWM:392"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:392"] = {rtwname: "<S5>/Constant11"};
	this.rtwnameHashMap["<S5>/Constant12"] = {sid: "IP_Core_SS_Switch_and_PWM:393"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:393"] = {rtwname: "<S5>/Constant12"};
	this.rtwnameHashMap["<S5>/Constant14"] = {sid: "IP_Core_SS_Switch_and_PWM:394"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:394"] = {rtwname: "<S5>/Constant14"};
	this.rtwnameHashMap["<S5>/Delay"] = {sid: "IP_Core_SS_Switch_and_PWM:395"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:395"] = {rtwname: "<S5>/Delay"};
	this.rtwnameHashMap["<S5>/Delay6"] = {sid: "IP_Core_SS_Switch_and_PWM:396"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:396"] = {rtwname: "<S5>/Delay6"};
	this.rtwnameHashMap["<S5>/Logical Operator"] = {sid: "IP_Core_SS_Switch_and_PWM:399"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:399"] = {rtwname: "<S5>/Logical Operator"};
	this.rtwnameHashMap["<S5>/Product"] = {sid: "IP_Core_SS_Switch_and_PWM:400"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:400"] = {rtwname: "<S5>/Product"};
	this.rtwnameHashMap["<S5>/Relational Operator1"] = {sid: "IP_Core_SS_Switch_and_PWM:510"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:510"] = {rtwname: "<S5>/Relational Operator1"};
	this.rtwnameHashMap["<S5>/Relational Operator2"] = {sid: "IP_Core_SS_Switch_and_PWM:541"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:541"] = {rtwname: "<S5>/Relational Operator2"};
	this.rtwnameHashMap["<S5>/Relational Operator4"] = {sid: "IP_Core_SS_Switch_and_PWM:401"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:401"] = {rtwname: "<S5>/Relational Operator4"};
	this.rtwnameHashMap["<S5>/Relational Operator5"] = {sid: "IP_Core_SS_Switch_and_PWM:402"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:402"] = {rtwname: "<S5>/Relational Operator5"};
	this.rtwnameHashMap["<S5>/Scope9"] = {sid: "IP_Core_SS_Switch_and_PWM:407"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:407"] = {rtwname: "<S5>/Scope9"};
	this.rtwnameHashMap["<S5>/Switch6"] = {sid: "IP_Core_SS_Switch_and_PWM:408"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:408"] = {rtwname: "<S5>/Switch6"};
	this.rtwnameHashMap["<S5>/Switch7"] = {sid: "IP_Core_SS_Switch_and_PWM:409"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:409"] = {rtwname: "<S5>/Switch7"};
	this.rtwnameHashMap["<S5>/dir_ctrl"] = {sid: "IP_Core_SS_Switch_and_PWM:410"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:410"] = {rtwname: "<S5>/dir_ctrl"};
	this.rtwnameHashMap["<S5>/triangle_sig"] = {sid: "IP_Core_SS_Switch_and_PWM:411"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:411"] = {rtwname: "<S5>/triangle_sig"};
	this.rtwnameHashMap["<S5>/Period_CenterMax"] = {sid: "IP_Core_SS_Switch_and_PWM:509"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:509"] = {rtwname: "<S5>/Period_CenterMax"};
	this.rtwnameHashMap["<S5>/Period_CenterMin"] = {sid: "IP_Core_SS_Switch_and_PWM:540"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:540"] = {rtwname: "<S5>/Period_CenterMin"};
	this.rtwnameHashMap["<S6>/enb"] = {sid: "IP_Core_SS_Switch_and_PWM:425:233"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:233"] = {rtwname: "<S6>/enb"};
	this.rtwnameHashMap["<S6>/dir"] = {sid: "IP_Core_SS_Switch_and_PWM:425:234"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:234"] = {rtwname: "<S6>/dir"};
	this.rtwnameHashMap["<S6>/Add_wrap"] = {sid: "IP_Core_SS_Switch_and_PWM:425:1"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:1"] = {rtwname: "<S6>/Add_wrap"};
	this.rtwnameHashMap["<S6>/Constant"] = {sid: "IP_Core_SS_Switch_and_PWM:425:212"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:212"] = {rtwname: "<S6>/Constant"};
	this.rtwnameHashMap["<S6>/Count_reg"] = {sid: "IP_Core_SS_Switch_and_PWM:425:9"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:9"] = {rtwname: "<S6>/Count_reg"};
	this.rtwnameHashMap["<S6>/DT_convert"] = {sid: "IP_Core_SS_Switch_and_PWM:425:10"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:10"] = {rtwname: "<S6>/DT_convert"};
	this.rtwnameHashMap["<S6>/DT_convert1"] = {sid: "IP_Core_SS_Switch_and_PWM:425:11"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:11"] = {rtwname: "<S6>/DT_convert1"};
	this.rtwnameHashMap["<S6>/Dir_logic"] = {sid: "IP_Core_SS_Switch_and_PWM:425:12"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:12"] = {rtwname: "<S6>/Dir_logic"};
	this.rtwnameHashMap["<S6>/Free_running_or_modulo"] = {sid: "IP_Core_SS_Switch_and_PWM:425:17"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:17"] = {rtwname: "<S6>/Free_running_or_modulo"};
	this.rtwnameHashMap["<S6>/From_value"] = {sid: "IP_Core_SS_Switch_and_PWM:425:19"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:19"] = {rtwname: "<S6>/From_value"};
	this.rtwnameHashMap["<S6>/Init_value"] = {sid: "IP_Core_SS_Switch_and_PWM:425:18"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:18"] = {rtwname: "<S6>/Init_value"};
	this.rtwnameHashMap["<S6>/Relational Operator"] = {sid: "IP_Core_SS_Switch_and_PWM:425:211"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:211"] = {rtwname: "<S6>/Relational Operator"};
	this.rtwnameHashMap["<S6>/Signal Specification"] = {sid: "IP_Core_SS_Switch_and_PWM:425:210"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:210"] = {rtwname: "<S6>/Signal Specification"};
	this.rtwnameHashMap["<S6>/Step_value"] = {sid: "IP_Core_SS_Switch_and_PWM:425:21"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:21"] = {rtwname: "<S6>/Step_value"};
	this.rtwnameHashMap["<S6>/Sub_wrap"] = {sid: "IP_Core_SS_Switch_and_PWM:425:22"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:22"] = {rtwname: "<S6>/Sub_wrap"};
	this.rtwnameHashMap["<S6>/Switch_dir"] = {sid: "IP_Core_SS_Switch_and_PWM:425:30"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:30"] = {rtwname: "<S6>/Switch_dir"};
	this.rtwnameHashMap["<S6>/Switch_enb"] = {sid: "IP_Core_SS_Switch_and_PWM:425:31"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:31"] = {rtwname: "<S6>/Switch_enb"};
	this.rtwnameHashMap["<S6>/Switch_load"] = {sid: "IP_Core_SS_Switch_and_PWM:425:32"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:32"] = {rtwname: "<S6>/Switch_load"};
	this.rtwnameHashMap["<S6>/Switch_max"] = {sid: "IP_Core_SS_Switch_and_PWM:425:33"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:33"] = {rtwname: "<S6>/Switch_max"};
	this.rtwnameHashMap["<S6>/Switch_reset"] = {sid: "IP_Core_SS_Switch_and_PWM:425:34"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:34"] = {rtwname: "<S6>/Switch_reset"};
	this.rtwnameHashMap["<S6>/Switch_type"] = {sid: "IP_Core_SS_Switch_and_PWM:425:35"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:35"] = {rtwname: "<S6>/Switch_type"};
	this.rtwnameHashMap["<S6>/const_load"] = {sid: "IP_Core_SS_Switch_and_PWM:425:38"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:38"] = {rtwname: "<S6>/const_load"};
	this.rtwnameHashMap["<S6>/const_load_val"] = {sid: "IP_Core_SS_Switch_and_PWM:425:39"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:39"] = {rtwname: "<S6>/const_load_val"};
	this.rtwnameHashMap["<S6>/const_rst"] = {sid: "IP_Core_SS_Switch_and_PWM:425:40"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:40"] = {rtwname: "<S6>/const_rst"};
	this.rtwnameHashMap["<S6>/count_hit_subsystem"] = {sid: "IP_Core_SS_Switch_and_PWM:425:221"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:221"] = {rtwname: "<S6>/count_hit_subsystem"};
	this.rtwnameHashMap["<S6>/count_hit_terminator"] = {sid: "IP_Core_SS_Switch_and_PWM:425:220"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:220"] = {rtwname: "<S6>/count_hit_terminator"};
	this.rtwnameHashMap["<S6>/count"] = {sid: "IP_Core_SS_Switch_and_PWM:425:41"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:41"] = {rtwname: "<S6>/count"};
	this.rtwnameHashMap["<S7>/ref_sig"] = {sid: "IP_Core_SS_Switch_and_PWM:427"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:427"] = {rtwname: "<S7>/ref_sig"};
	this.rtwnameHashMap["<S7>/min_pulse_width"] = {sid: "IP_Core_SS_Switch_and_PWM:428"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:428"] = {rtwname: "<S7>/min_pulse_width"};
	this.rtwnameHashMap["<S7>/Add"] = {sid: "IP_Core_SS_Switch_and_PWM:429"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:429"] = {rtwname: "<S7>/Add"};
	this.rtwnameHashMap["<S7>/Constant"] = {sid: "IP_Core_SS_Switch_and_PWM:430"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:430"] = {rtwname: "<S7>/Constant"};
	this.rtwnameHashMap["<S7>/Constant1"] = {sid: "IP_Core_SS_Switch_and_PWM:431"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:431"] = {rtwname: "<S7>/Constant1"};
	this.rtwnameHashMap["<S7>/Constant2"] = {sid: "IP_Core_SS_Switch_and_PWM:432"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:432"] = {rtwname: "<S7>/Constant2"};
	this.rtwnameHashMap["<S7>/Logical Operator"] = {sid: "IP_Core_SS_Switch_and_PWM:433"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:433"] = {rtwname: "<S7>/Logical Operator"};
	this.rtwnameHashMap["<S7>/Relational Operator"] = {sid: "IP_Core_SS_Switch_and_PWM:434"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:434"] = {rtwname: "<S7>/Relational Operator"};
	this.rtwnameHashMap["<S7>/Relational Operator1"] = {sid: "IP_Core_SS_Switch_and_PWM:435"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:435"] = {rtwname: "<S7>/Relational Operator1"};
	this.rtwnameHashMap["<S7>/Saturation"] = {sid: "IP_Core_SS_Switch_and_PWM:436"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:436"] = {rtwname: "<S7>/Saturation"};
	this.rtwnameHashMap["<S7>/Switch1"] = {sid: "IP_Core_SS_Switch_and_PWM:437"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:437"] = {rtwname: "<S7>/Switch1"};
	this.rtwnameHashMap["<S7>/Switch6"] = {sid: "IP_Core_SS_Switch_and_PWM:438"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:438"] = {rtwname: "<S7>/Switch6"};
	this.rtwnameHashMap["<S7>/ref_sig_limit"] = {sid: "IP_Core_SS_Switch_and_PWM:439"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:439"] = {rtwname: "<S7>/ref_sig_limit"};
	this.rtwnameHashMap["<S8>/ref_sig"] = {sid: "IP_Core_SS_Switch_and_PWM:442"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:442"] = {rtwname: "<S8>/ref_sig"};
	this.rtwnameHashMap["<S8>/min_pulse_width"] = {sid: "IP_Core_SS_Switch_and_PWM:443"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:443"] = {rtwname: "<S8>/min_pulse_width"};
	this.rtwnameHashMap["<S8>/Add"] = {sid: "IP_Core_SS_Switch_and_PWM:444"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:444"] = {rtwname: "<S8>/Add"};
	this.rtwnameHashMap["<S8>/Constant"] = {sid: "IP_Core_SS_Switch_and_PWM:445"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:445"] = {rtwname: "<S8>/Constant"};
	this.rtwnameHashMap["<S8>/Constant1"] = {sid: "IP_Core_SS_Switch_and_PWM:446"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:446"] = {rtwname: "<S8>/Constant1"};
	this.rtwnameHashMap["<S8>/Constant2"] = {sid: "IP_Core_SS_Switch_and_PWM:447"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:447"] = {rtwname: "<S8>/Constant2"};
	this.rtwnameHashMap["<S8>/Logical Operator"] = {sid: "IP_Core_SS_Switch_and_PWM:448"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:448"] = {rtwname: "<S8>/Logical Operator"};
	this.rtwnameHashMap["<S8>/Relational Operator"] = {sid: "IP_Core_SS_Switch_and_PWM:449"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:449"] = {rtwname: "<S8>/Relational Operator"};
	this.rtwnameHashMap["<S8>/Relational Operator1"] = {sid: "IP_Core_SS_Switch_and_PWM:450"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:450"] = {rtwname: "<S8>/Relational Operator1"};
	this.rtwnameHashMap["<S8>/Saturation"] = {sid: "IP_Core_SS_Switch_and_PWM:451"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:451"] = {rtwname: "<S8>/Saturation"};
	this.rtwnameHashMap["<S8>/Switch1"] = {sid: "IP_Core_SS_Switch_and_PWM:452"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:452"] = {rtwname: "<S8>/Switch1"};
	this.rtwnameHashMap["<S8>/Switch6"] = {sid: "IP_Core_SS_Switch_and_PWM:453"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:453"] = {rtwname: "<S8>/Switch6"};
	this.rtwnameHashMap["<S8>/ref_sig_limit"] = {sid: "IP_Core_SS_Switch_and_PWM:454"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:454"] = {rtwname: "<S8>/ref_sig_limit"};
	this.rtwnameHashMap["<S9>/ref_sig"] = {sid: "IP_Core_SS_Switch_and_PWM:457"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:457"] = {rtwname: "<S9>/ref_sig"};
	this.rtwnameHashMap["<S9>/min_pulse_width"] = {sid: "IP_Core_SS_Switch_and_PWM:458"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:458"] = {rtwname: "<S9>/min_pulse_width"};
	this.rtwnameHashMap["<S9>/Add"] = {sid: "IP_Core_SS_Switch_and_PWM:459"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:459"] = {rtwname: "<S9>/Add"};
	this.rtwnameHashMap["<S9>/Constant"] = {sid: "IP_Core_SS_Switch_and_PWM:460"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:460"] = {rtwname: "<S9>/Constant"};
	this.rtwnameHashMap["<S9>/Constant1"] = {sid: "IP_Core_SS_Switch_and_PWM:461"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:461"] = {rtwname: "<S9>/Constant1"};
	this.rtwnameHashMap["<S9>/Constant2"] = {sid: "IP_Core_SS_Switch_and_PWM:462"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:462"] = {rtwname: "<S9>/Constant2"};
	this.rtwnameHashMap["<S9>/Logical Operator"] = {sid: "IP_Core_SS_Switch_and_PWM:463"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:463"] = {rtwname: "<S9>/Logical Operator"};
	this.rtwnameHashMap["<S9>/Relational Operator"] = {sid: "IP_Core_SS_Switch_and_PWM:464"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:464"] = {rtwname: "<S9>/Relational Operator"};
	this.rtwnameHashMap["<S9>/Relational Operator1"] = {sid: "IP_Core_SS_Switch_and_PWM:465"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:465"] = {rtwname: "<S9>/Relational Operator1"};
	this.rtwnameHashMap["<S9>/Saturation"] = {sid: "IP_Core_SS_Switch_and_PWM:466"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:466"] = {rtwname: "<S9>/Saturation"};
	this.rtwnameHashMap["<S9>/Switch1"] = {sid: "IP_Core_SS_Switch_and_PWM:467"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:467"] = {rtwname: "<S9>/Switch1"};
	this.rtwnameHashMap["<S9>/Switch6"] = {sid: "IP_Core_SS_Switch_and_PWM:468"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:468"] = {rtwname: "<S9>/Switch6"};
	this.rtwnameHashMap["<S9>/ref_sig_limit"] = {sid: "IP_Core_SS_Switch_and_PWM:469"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:469"] = {rtwname: "<S9>/ref_sig_limit"};
	this.rtwnameHashMap["<S10>/triangle_in"] = {sid: "IP_Core_SS_Switch_and_PWM:1734"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1734"] = {rtwname: "<S10>/triangle_in"};
	this.rtwnameHashMap["<S10>/triangle_shift"] = {sid: "IP_Core_SS_Switch_and_PWM:1735"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1735"] = {rtwname: "<S10>/triangle_shift"};
	this.rtwnameHashMap["<S10>/0"] = {sid: "IP_Core_SS_Switch_and_PWM:2372"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:2372"] = {rtwname: "<S10>/0"};
	this.rtwnameHashMap["<S10>/Data Type Conversion"] = {sid: "IP_Core_SS_Switch_and_PWM:1804"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1804"] = {rtwname: "<S10>/Data Type Conversion"};
	this.rtwnameHashMap["<S10>/Data Type Conversion1"] = {sid: "IP_Core_SS_Switch_and_PWM:2354"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:2354"] = {rtwname: "<S10>/Data Type Conversion1"};
	this.rtwnameHashMap["<S10>/Delay1"] = {sid: "IP_Core_SS_Switch_and_PWM:2358"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:2358"] = {rtwname: "<S10>/Delay1"};
	this.rtwnameHashMap["<S10>/Delay2"] = {sid: "IP_Core_SS_Switch_and_PWM:1739"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1739"] = {rtwname: "<S10>/Delay2"};
	this.rtwnameHashMap["<S10>/Gain"] = {sid: "IP_Core_SS_Switch_and_PWM:1802"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1802"] = {rtwname: "<S10>/Gain"};
	this.rtwnameHashMap["<S10>/Gain1"] = {sid: "IP_Core_SS_Switch_and_PWM:1805"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1805"] = {rtwname: "<S10>/Gain1"};
	this.rtwnameHashMap["<S10>/Relational Operator1"] = {sid: "IP_Core_SS_Switch_and_PWM:2357"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:2357"] = {rtwname: "<S10>/Relational Operator1"};
	this.rtwnameHashMap["<S10>/Relational Operator7"] = {sid: "IP_Core_SS_Switch_and_PWM:1749"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1749"] = {rtwname: "<S10>/Relational Operator7"};
	this.rtwnameHashMap["<S10>/Subsystem"] = {sid: "IP_Core_SS_Switch_and_PWM:3593"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3593"] = {rtwname: "<S10>/Subsystem"};
	this.rtwnameHashMap["<S10>/Subsystem1"] = {sid: "IP_Core_SS_Switch_and_PWM:3603"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3603"] = {rtwname: "<S10>/Subsystem1"};
	this.rtwnameHashMap["<S10>/Subsystem2"] = {sid: "IP_Core_SS_Switch_and_PWM:3615"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3615"] = {rtwname: "<S10>/Subsystem2"};
	this.rtwnameHashMap["<S10>/Subsystem3"] = {sid: "IP_Core_SS_Switch_and_PWM:3624"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3624"] = {rtwname: "<S10>/Subsystem3"};
	this.rtwnameHashMap["<S10>/Sum10"] = {sid: "IP_Core_SS_Switch_and_PWM:1784"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1784"] = {rtwname: "<S10>/Sum10"};
	this.rtwnameHashMap["<S10>/or"] = {sid: "IP_Core_SS_Switch_and_PWM:2360"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:2360"] = {rtwname: "<S10>/or"};
	this.rtwnameHashMap["<S10>/standardized peak of triangle"] = {sid: "IP_Core_SS_Switch_and_PWM:1799"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1799"] = {rtwname: "<S10>/standardized peak of triangle"};
	this.rtwnameHashMap["<S10>/sw1"] = {sid: "IP_Core_SS_Switch_and_PWM:1769"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1769"] = {rtwname: "<S10>/sw1"};
	this.rtwnameHashMap["<S10>/sw2"] = {sid: "IP_Core_SS_Switch_and_PWM:1770"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1770"] = {rtwname: "<S10>/sw2"};
	this.rtwnameHashMap["<S10>/sw3"] = {sid: "IP_Core_SS_Switch_and_PWM:1771"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1771"] = {rtwname: "<S10>/sw3"};
	this.rtwnameHashMap["<S10>/sw4"] = {sid: "IP_Core_SS_Switch_and_PWM:1785"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1785"] = {rtwname: "<S10>/sw4"};
	this.rtwnameHashMap["<S10>/triangle_sig"] = {sid: "IP_Core_SS_Switch_and_PWM:1736"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1736"] = {rtwname: "<S10>/triangle_sig"};
	this.rtwnameHashMap["<S11>/triangle_in"] = {sid: "IP_Core_SS_Switch_and_PWM:3640"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3640"] = {rtwname: "<S11>/triangle_in"};
	this.rtwnameHashMap["<S11>/triangle_shift"] = {sid: "IP_Core_SS_Switch_and_PWM:3641"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3641"] = {rtwname: "<S11>/triangle_shift"};
	this.rtwnameHashMap["<S11>/0"] = {sid: "IP_Core_SS_Switch_and_PWM:3642"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3642"] = {rtwname: "<S11>/0"};
	this.rtwnameHashMap["<S11>/Data Type Conversion"] = {sid: "IP_Core_SS_Switch_and_PWM:3643"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3643"] = {rtwname: "<S11>/Data Type Conversion"};
	this.rtwnameHashMap["<S11>/Data Type Conversion1"] = {sid: "IP_Core_SS_Switch_and_PWM:3644"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3644"] = {rtwname: "<S11>/Data Type Conversion1"};
	this.rtwnameHashMap["<S11>/Delay1"] = {sid: "IP_Core_SS_Switch_and_PWM:3645"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3645"] = {rtwname: "<S11>/Delay1"};
	this.rtwnameHashMap["<S11>/Delay2"] = {sid: "IP_Core_SS_Switch_and_PWM:3646"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3646"] = {rtwname: "<S11>/Delay2"};
	this.rtwnameHashMap["<S11>/Gain"] = {sid: "IP_Core_SS_Switch_and_PWM:3647"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3647"] = {rtwname: "<S11>/Gain"};
	this.rtwnameHashMap["<S11>/Gain1"] = {sid: "IP_Core_SS_Switch_and_PWM:3648"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3648"] = {rtwname: "<S11>/Gain1"};
	this.rtwnameHashMap["<S11>/Relational Operator1"] = {sid: "IP_Core_SS_Switch_and_PWM:3649"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3649"] = {rtwname: "<S11>/Relational Operator1"};
	this.rtwnameHashMap["<S11>/Relational Operator7"] = {sid: "IP_Core_SS_Switch_and_PWM:3650"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3650"] = {rtwname: "<S11>/Relational Operator7"};
	this.rtwnameHashMap["<S11>/Subsystem"] = {sid: "IP_Core_SS_Switch_and_PWM:3651"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3651"] = {rtwname: "<S11>/Subsystem"};
	this.rtwnameHashMap["<S11>/Subsystem1"] = {sid: "IP_Core_SS_Switch_and_PWM:3665"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3665"] = {rtwname: "<S11>/Subsystem1"};
	this.rtwnameHashMap["<S11>/Subsystem2"] = {sid: "IP_Core_SS_Switch_and_PWM:3681"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3681"] = {rtwname: "<S11>/Subsystem2"};
	this.rtwnameHashMap["<S11>/Subsystem3"] = {sid: "IP_Core_SS_Switch_and_PWM:3693"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3693"] = {rtwname: "<S11>/Subsystem3"};
	this.rtwnameHashMap["<S11>/Sum10"] = {sid: "IP_Core_SS_Switch_and_PWM:3704"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3704"] = {rtwname: "<S11>/Sum10"};
	this.rtwnameHashMap["<S11>/or"] = {sid: "IP_Core_SS_Switch_and_PWM:3705"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3705"] = {rtwname: "<S11>/or"};
	this.rtwnameHashMap["<S11>/standardized peak of triangle"] = {sid: "IP_Core_SS_Switch_and_PWM:3706"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3706"] = {rtwname: "<S11>/standardized peak of triangle"};
	this.rtwnameHashMap["<S11>/sw1"] = {sid: "IP_Core_SS_Switch_and_PWM:3707"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3707"] = {rtwname: "<S11>/sw1"};
	this.rtwnameHashMap["<S11>/sw2"] = {sid: "IP_Core_SS_Switch_and_PWM:3708"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3708"] = {rtwname: "<S11>/sw2"};
	this.rtwnameHashMap["<S11>/sw3"] = {sid: "IP_Core_SS_Switch_and_PWM:3709"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3709"] = {rtwname: "<S11>/sw3"};
	this.rtwnameHashMap["<S11>/sw4"] = {sid: "IP_Core_SS_Switch_and_PWM:3710"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3710"] = {rtwname: "<S11>/sw4"};
	this.rtwnameHashMap["<S11>/triangle_sig"] = {sid: "IP_Core_SS_Switch_and_PWM:3711"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3711"] = {rtwname: "<S11>/triangle_sig"};
	this.rtwnameHashMap["<S12>/triangle_in"] = {sid: "IP_Core_SS_Switch_and_PWM:3719"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3719"] = {rtwname: "<S12>/triangle_in"};
	this.rtwnameHashMap["<S12>/triangle_shift"] = {sid: "IP_Core_SS_Switch_and_PWM:3720"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3720"] = {rtwname: "<S12>/triangle_shift"};
	this.rtwnameHashMap["<S12>/0"] = {sid: "IP_Core_SS_Switch_and_PWM:3721"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3721"] = {rtwname: "<S12>/0"};
	this.rtwnameHashMap["<S12>/Data Type Conversion"] = {sid: "IP_Core_SS_Switch_and_PWM:3722"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3722"] = {rtwname: "<S12>/Data Type Conversion"};
	this.rtwnameHashMap["<S12>/Data Type Conversion1"] = {sid: "IP_Core_SS_Switch_and_PWM:3723"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3723"] = {rtwname: "<S12>/Data Type Conversion1"};
	this.rtwnameHashMap["<S12>/Delay1"] = {sid: "IP_Core_SS_Switch_and_PWM:3724"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3724"] = {rtwname: "<S12>/Delay1"};
	this.rtwnameHashMap["<S12>/Delay2"] = {sid: "IP_Core_SS_Switch_and_PWM:3725"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3725"] = {rtwname: "<S12>/Delay2"};
	this.rtwnameHashMap["<S12>/Gain"] = {sid: "IP_Core_SS_Switch_and_PWM:3726"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3726"] = {rtwname: "<S12>/Gain"};
	this.rtwnameHashMap["<S12>/Gain1"] = {sid: "IP_Core_SS_Switch_and_PWM:3727"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3727"] = {rtwname: "<S12>/Gain1"};
	this.rtwnameHashMap["<S12>/Relational Operator1"] = {sid: "IP_Core_SS_Switch_and_PWM:3728"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3728"] = {rtwname: "<S12>/Relational Operator1"};
	this.rtwnameHashMap["<S12>/Relational Operator7"] = {sid: "IP_Core_SS_Switch_and_PWM:3729"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3729"] = {rtwname: "<S12>/Relational Operator7"};
	this.rtwnameHashMap["<S12>/Subsystem"] = {sid: "IP_Core_SS_Switch_and_PWM:3730"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3730"] = {rtwname: "<S12>/Subsystem"};
	this.rtwnameHashMap["<S12>/Subsystem1"] = {sid: "IP_Core_SS_Switch_and_PWM:3744"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3744"] = {rtwname: "<S12>/Subsystem1"};
	this.rtwnameHashMap["<S12>/Subsystem2"] = {sid: "IP_Core_SS_Switch_and_PWM:3760"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3760"] = {rtwname: "<S12>/Subsystem2"};
	this.rtwnameHashMap["<S12>/Subsystem3"] = {sid: "IP_Core_SS_Switch_and_PWM:3772"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3772"] = {rtwname: "<S12>/Subsystem3"};
	this.rtwnameHashMap["<S12>/Sum10"] = {sid: "IP_Core_SS_Switch_and_PWM:3783"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3783"] = {rtwname: "<S12>/Sum10"};
	this.rtwnameHashMap["<S12>/or"] = {sid: "IP_Core_SS_Switch_and_PWM:3784"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3784"] = {rtwname: "<S12>/or"};
	this.rtwnameHashMap["<S12>/standardized peak of triangle"] = {sid: "IP_Core_SS_Switch_and_PWM:3785"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3785"] = {rtwname: "<S12>/standardized peak of triangle"};
	this.rtwnameHashMap["<S12>/sw1"] = {sid: "IP_Core_SS_Switch_and_PWM:3786"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3786"] = {rtwname: "<S12>/sw1"};
	this.rtwnameHashMap["<S12>/sw2"] = {sid: "IP_Core_SS_Switch_and_PWM:3787"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3787"] = {rtwname: "<S12>/sw2"};
	this.rtwnameHashMap["<S12>/sw3"] = {sid: "IP_Core_SS_Switch_and_PWM:3788"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3788"] = {rtwname: "<S12>/sw3"};
	this.rtwnameHashMap["<S12>/sw4"] = {sid: "IP_Core_SS_Switch_and_PWM:3789"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3789"] = {rtwname: "<S12>/sw4"};
	this.rtwnameHashMap["<S12>/triangle_sig"] = {sid: "IP_Core_SS_Switch_and_PWM:3790"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3790"] = {rtwname: "<S12>/triangle_sig"};
	this.rtwnameHashMap["<S13>/step"] = {sid: "IP_Core_SS_Switch_and_PWM:425:2"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:2"] = {rtwname: "<S13>/step"};
	this.rtwnameHashMap["<S13>/fb"] = {sid: "IP_Core_SS_Switch_and_PWM:425:3"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:3"] = {rtwname: "<S13>/fb"};
	this.rtwnameHashMap["<S13>/Add"] = {sid: "IP_Core_SS_Switch_and_PWM:425:4"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:4"] = {rtwname: "<S13>/Add"};
	this.rtwnameHashMap["<S13>/Compare To Constant"] = {sid: "IP_Core_SS_Switch_and_PWM:425:215"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:215"] = {rtwname: "<S13>/Compare To Constant"};
	this.rtwnameHashMap["<S13>/Mod_value"] = {sid: "IP_Core_SS_Switch_and_PWM:425:5"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:5"] = {rtwname: "<S13>/Mod_value"};
	this.rtwnameHashMap["<S13>/Switch_wrap"] = {sid: "IP_Core_SS_Switch_and_PWM:425:6"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:6"] = {rtwname: "<S13>/Switch_wrap"};
	this.rtwnameHashMap["<S13>/Wrap"] = {sid: "IP_Core_SS_Switch_and_PWM:425:7"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:7"] = {rtwname: "<S13>/Wrap"};
	this.rtwnameHashMap["<S13>/add"] = {sid: "IP_Core_SS_Switch_and_PWM:425:8"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:8"] = {rtwname: "<S13>/add"};
	this.rtwnameHashMap["<S13>/count_hit"] = {sid: "IP_Core_SS_Switch_and_PWM:425:216"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:216"] = {rtwname: "<S13>/count_hit"};
	this.rtwnameHashMap["<S14>/dir_port"] = {sid: "IP_Core_SS_Switch_and_PWM:425:13"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:13"] = {rtwname: "<S14>/dir_port"};
	this.rtwnameHashMap["<S14>/Logical Operator"] = {sid: "IP_Core_SS_Switch_and_PWM:425:14"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:14"] = {rtwname: "<S14>/Logical Operator"};
	this.rtwnameHashMap["<S14>/Pos_step"] = {sid: "IP_Core_SS_Switch_and_PWM:425:15"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:15"] = {rtwname: "<S14>/Pos_step"};
	this.rtwnameHashMap["<S14>/dn"] = {sid: "IP_Core_SS_Switch_and_PWM:425:16"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:16"] = {rtwname: "<S14>/dn"};
	this.rtwnameHashMap["<S15>/step"] = {sid: "IP_Core_SS_Switch_and_PWM:425:23"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:23"] = {rtwname: "<S15>/step"};
	this.rtwnameHashMap["<S15>/fb"] = {sid: "IP_Core_SS_Switch_and_PWM:425:24"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:24"] = {rtwname: "<S15>/fb"};
	this.rtwnameHashMap["<S15>/Add"] = {sid: "IP_Core_SS_Switch_and_PWM:425:25"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:25"] = {rtwname: "<S15>/Add"};
	this.rtwnameHashMap["<S15>/Compare To Constant"] = {sid: "IP_Core_SS_Switch_and_PWM:425:213"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:213"] = {rtwname: "<S15>/Compare To Constant"};
	this.rtwnameHashMap["<S15>/Mod_value"] = {sid: "IP_Core_SS_Switch_and_PWM:425:26"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:26"] = {rtwname: "<S15>/Mod_value"};
	this.rtwnameHashMap["<S15>/Switch_wrap"] = {sid: "IP_Core_SS_Switch_and_PWM:425:27"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:27"] = {rtwname: "<S15>/Switch_wrap"};
	this.rtwnameHashMap["<S15>/Wrap"] = {sid: "IP_Core_SS_Switch_and_PWM:425:28"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:28"] = {rtwname: "<S15>/Wrap"};
	this.rtwnameHashMap["<S15>/sub"] = {sid: "IP_Core_SS_Switch_and_PWM:425:29"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:29"] = {rtwname: "<S15>/sub"};
	this.rtwnameHashMap["<S15>/count_hit"] = {sid: "IP_Core_SS_Switch_and_PWM:425:214"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:214"] = {rtwname: "<S15>/count_hit"};
	this.rtwnameHashMap["<S16>/u"] = {sid: "IP_Core_SS_Switch_and_PWM:425:215:1"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:215:1"] = {rtwname: "<S16>/u"};
	this.rtwnameHashMap["<S16>/Compare"] = {sid: "IP_Core_SS_Switch_and_PWM:425:215:2"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:215:2"] = {rtwname: "<S16>/Compare"};
	this.rtwnameHashMap["<S16>/Constant"] = {sid: "IP_Core_SS_Switch_and_PWM:425:215:3"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:215:3"] = {rtwname: "<S16>/Constant"};
	this.rtwnameHashMap["<S16>/y"] = {sid: "IP_Core_SS_Switch_and_PWM:425:215:4"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:215:4"] = {rtwname: "<S16>/y"};
	this.rtwnameHashMap["<S17>/u"] = {sid: "IP_Core_SS_Switch_and_PWM:425:213:1"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:213:1"] = {rtwname: "<S17>/u"};
	this.rtwnameHashMap["<S17>/Compare"] = {sid: "IP_Core_SS_Switch_and_PWM:425:213:2"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:213:2"] = {rtwname: "<S17>/Compare"};
	this.rtwnameHashMap["<S17>/Constant"] = {sid: "IP_Core_SS_Switch_and_PWM:425:213:3"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:213:3"] = {rtwname: "<S17>/Constant"};
	this.rtwnameHashMap["<S17>/y"] = {sid: "IP_Core_SS_Switch_and_PWM:425:213:4"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:425:213:4"] = {rtwname: "<S17>/y"};
	this.rtwnameHashMap["<S18>/Zero_input"] = {sid: "IP_Core_SS_Switch_and_PWM:3596"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3596"] = {rtwname: "<S18>/Zero_input"};
	this.rtwnameHashMap["<S18>/converted triangle_shift"] = {sid: "IP_Core_SS_Switch_and_PWM:3594"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3594"] = {rtwname: "<S18>/converted triangle_shift"};
	this.rtwnameHashMap["<S18>/modified triangle_shift"] = {sid: "IP_Core_SS_Switch_and_PWM:3595"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3595"] = {rtwname: "<S18>/modified triangle_shift"};
	this.rtwnameHashMap["<S18>/triangle_in"] = {sid: "IP_Core_SS_Switch_and_PWM:3597"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3597"] = {rtwname: "<S18>/triangle_in"};
	this.rtwnameHashMap["<S18>/direction down"] = {sid: "IP_Core_SS_Switch_and_PWM:3598"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3598"] = {rtwname: "<S18>/direction down"};
	this.rtwnameHashMap["<S18>/Relational Operator11"] = {sid: "IP_Core_SS_Switch_and_PWM:1745"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1745"] = {rtwname: "<S18>/Relational Operator11"};
	this.rtwnameHashMap["<S18>/Sum8"] = {sid: "IP_Core_SS_Switch_and_PWM:1761"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1761"] = {rtwname: "<S18>/Sum8"};
	this.rtwnameHashMap["<S18>/Unary Minus"] = {sid: "IP_Core_SS_Switch_and_PWM:1764"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1764"] = {rtwname: "<S18>/Unary Minus"};
	this.rtwnameHashMap["<S18>/and2_1"] = {sid: "IP_Core_SS_Switch_and_PWM:2374"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:2374"] = {rtwname: "<S18>/and2_1"};
	this.rtwnameHashMap["<S18>/and2_2"] = {sid: "IP_Core_SS_Switch_and_PWM:2375"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:2375"] = {rtwname: "<S18>/and2_2"};
	this.rtwnameHashMap["<S18>/shifted triangle_in signal"] = {sid: "IP_Core_SS_Switch_and_PWM:3599"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3599"] = {rtwname: "<S18>/shifted triangle_in signal"};
	this.rtwnameHashMap["<S18>/enable_out"] = {sid: "IP_Core_SS_Switch_and_PWM:3600"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3600"] = {rtwname: "<S18>/enable_out"};
	this.rtwnameHashMap["<S19>/standardized peak of triangle"] = {sid: "IP_Core_SS_Switch_and_PWM:3604"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3604"] = {rtwname: "<S19>/standardized peak of triangle"};
	this.rtwnameHashMap["<S19>/triangle_in"] = {sid: "IP_Core_SS_Switch_and_PWM:3606"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3606"] = {rtwname: "<S19>/triangle_in"};
	this.rtwnameHashMap["<S19>/converted triangle_shift"] = {sid: "IP_Core_SS_Switch_and_PWM:3607"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3607"] = {rtwname: "<S19>/converted triangle_shift"};
	this.rtwnameHashMap["<S19>/modified triangle_shift"] = {sid: "IP_Core_SS_Switch_and_PWM:3608"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3608"] = {rtwname: "<S19>/modified triangle_shift"};
	this.rtwnameHashMap["<S19>/Zero_input"] = {sid: "IP_Core_SS_Switch_and_PWM:3610"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3610"] = {rtwname: "<S19>/Zero_input"};
	this.rtwnameHashMap["<S19>/direction up"] = {sid: "IP_Core_SS_Switch_and_PWM:3611"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3611"] = {rtwname: "<S19>/direction up"};
	this.rtwnameHashMap["<S19>/Relational Operator2"] = {sid: "IP_Core_SS_Switch_and_PWM:1746"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1746"] = {rtwname: "<S19>/Relational Operator2"};
	this.rtwnameHashMap["<S19>/Sum11"] = {sid: "IP_Core_SS_Switch_and_PWM:2361"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:2361"] = {rtwname: "<S19>/Sum11"};
	this.rtwnameHashMap["<S19>/Sum6"] = {sid: "IP_Core_SS_Switch_and_PWM:1759"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1759"] = {rtwname: "<S19>/Sum6"};
	this.rtwnameHashMap["<S19>/and1_1"] = {sid: "IP_Core_SS_Switch_and_PWM:2370"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:2370"] = {rtwname: "<S19>/and1_1"};
	this.rtwnameHashMap["<S19>/and1_2"] = {sid: "IP_Core_SS_Switch_and_PWM:2371"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:2371"] = {rtwname: "<S19>/and1_2"};
	this.rtwnameHashMap["<S19>/shifted triangle_in signal"] = {sid: "IP_Core_SS_Switch_and_PWM:3612"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3612"] = {rtwname: "<S19>/shifted triangle_in signal"};
	this.rtwnameHashMap["<S19>/enable_out"] = {sid: "IP_Core_SS_Switch_and_PWM:3613"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3613"] = {rtwname: "<S19>/enable_out"};
	this.rtwnameHashMap["<S20>/triangle_in"] = {sid: "IP_Core_SS_Switch_and_PWM:3616"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3616"] = {rtwname: "<S20>/triangle_in"};
	this.rtwnameHashMap["<S20>/modified triangle_shift"] = {sid: "IP_Core_SS_Switch_and_PWM:3617"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3617"] = {rtwname: "<S20>/modified triangle_shift"};
	this.rtwnameHashMap["<S20>/standardized peak of triangle"] = {sid: "IP_Core_SS_Switch_and_PWM:3618"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3618"] = {rtwname: "<S20>/standardized peak of triangle"};
	this.rtwnameHashMap["<S20>/Zero_input"] = {sid: "IP_Core_SS_Switch_and_PWM:3620"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3620"] = {rtwname: "<S20>/Zero_input"};
	this.rtwnameHashMap["<S20>/direction down"] = {sid: "IP_Core_SS_Switch_and_PWM:3621"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3621"] = {rtwname: "<S20>/direction down"};
	this.rtwnameHashMap["<S20>/Relational Operator9"] = {sid: "IP_Core_SS_Switch_and_PWM:1751"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1751"] = {rtwname: "<S20>/Relational Operator9"};
	this.rtwnameHashMap["<S20>/Sum1"] = {sid: "IP_Core_SS_Switch_and_PWM:1754"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1754"] = {rtwname: "<S20>/Sum1"};
	this.rtwnameHashMap["<S20>/Sum5"] = {sid: "IP_Core_SS_Switch_and_PWM:1758"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1758"] = {rtwname: "<S20>/Sum5"};
	this.rtwnameHashMap["<S20>/and"] = {sid: "IP_Core_SS_Switch_and_PWM:2376"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:2376"] = {rtwname: "<S20>/and"};
	this.rtwnameHashMap["<S20>/shifted triangle_in signal"] = {sid: "IP_Core_SS_Switch_and_PWM:3622"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3622"] = {rtwname: "<S20>/shifted triangle_in signal"};
	this.rtwnameHashMap["<S20>/enable_out"] = {sid: "IP_Core_SS_Switch_and_PWM:3623"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3623"] = {rtwname: "<S20>/enable_out"};
	this.rtwnameHashMap["<S21>/triangle_in"] = {sid: "IP_Core_SS_Switch_and_PWM:3625"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3625"] = {rtwname: "<S21>/triangle_in"};
	this.rtwnameHashMap["<S21>/modified triangle_shift"] = {sid: "IP_Core_SS_Switch_and_PWM:3627"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3627"] = {rtwname: "<S21>/modified triangle_shift"};
	this.rtwnameHashMap["<S21>/direction up"] = {sid: "IP_Core_SS_Switch_and_PWM:3630"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3630"] = {rtwname: "<S21>/direction up"};
	this.rtwnameHashMap["<S21>/Relational Operator6"] = {sid: "IP_Core_SS_Switch_and_PWM:1748"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1748"] = {rtwname: "<S21>/Relational Operator6"};
	this.rtwnameHashMap["<S21>/Sum2"] = {sid: "IP_Core_SS_Switch_and_PWM:1755"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1755"] = {rtwname: "<S21>/Sum2"};
	this.rtwnameHashMap["<S21>/Sum3"] = {sid: "IP_Core_SS_Switch_and_PWM:1756"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1756"] = {rtwname: "<S21>/Sum3"};
	this.rtwnameHashMap["<S21>/Sum4"] = {sid: "IP_Core_SS_Switch_and_PWM:1757"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1757"] = {rtwname: "<S21>/Sum4"};
	this.rtwnameHashMap["<S21>/Switch3"] = {sid: "IP_Core_SS_Switch_and_PWM:1763"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1763"] = {rtwname: "<S21>/Switch3"};
	this.rtwnameHashMap["<S21>/sw"] = {sid: "IP_Core_SS_Switch_and_PWM:1768"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:1768"] = {rtwname: "<S21>/sw"};
	this.rtwnameHashMap["<S21>/shifted triangle_in signal"] = {sid: "IP_Core_SS_Switch_and_PWM:3631"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3631"] = {rtwname: "<S21>/shifted triangle_in signal"};
	this.rtwnameHashMap["<S22>/Zero_input"] = {sid: "IP_Core_SS_Switch_and_PWM:3652"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3652"] = {rtwname: "<S22>/Zero_input"};
	this.rtwnameHashMap["<S22>/converted triangle_shift"] = {sid: "IP_Core_SS_Switch_and_PWM:3653"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3653"] = {rtwname: "<S22>/converted triangle_shift"};
	this.rtwnameHashMap["<S22>/modified triangle_shift"] = {sid: "IP_Core_SS_Switch_and_PWM:3654"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3654"] = {rtwname: "<S22>/modified triangle_shift"};
	this.rtwnameHashMap["<S22>/triangle_in"] = {sid: "IP_Core_SS_Switch_and_PWM:3655"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3655"] = {rtwname: "<S22>/triangle_in"};
	this.rtwnameHashMap["<S22>/direction down"] = {sid: "IP_Core_SS_Switch_and_PWM:3656"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3656"] = {rtwname: "<S22>/direction down"};
	this.rtwnameHashMap["<S22>/Relational Operator11"] = {sid: "IP_Core_SS_Switch_and_PWM:3657"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3657"] = {rtwname: "<S22>/Relational Operator11"};
	this.rtwnameHashMap["<S22>/Sum8"] = {sid: "IP_Core_SS_Switch_and_PWM:3658"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3658"] = {rtwname: "<S22>/Sum8"};
	this.rtwnameHashMap["<S22>/Unary Minus"] = {sid: "IP_Core_SS_Switch_and_PWM:3659"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3659"] = {rtwname: "<S22>/Unary Minus"};
	this.rtwnameHashMap["<S22>/and2_1"] = {sid: "IP_Core_SS_Switch_and_PWM:3660"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3660"] = {rtwname: "<S22>/and2_1"};
	this.rtwnameHashMap["<S22>/and2_2"] = {sid: "IP_Core_SS_Switch_and_PWM:3661"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3661"] = {rtwname: "<S22>/and2_2"};
	this.rtwnameHashMap["<S22>/shifted triangle_in signal"] = {sid: "IP_Core_SS_Switch_and_PWM:3662"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3662"] = {rtwname: "<S22>/shifted triangle_in signal"};
	this.rtwnameHashMap["<S22>/enable_out"] = {sid: "IP_Core_SS_Switch_and_PWM:3663"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3663"] = {rtwname: "<S22>/enable_out"};
	this.rtwnameHashMap["<S23>/standardized peak of triangle"] = {sid: "IP_Core_SS_Switch_and_PWM:3666"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3666"] = {rtwname: "<S23>/standardized peak of triangle"};
	this.rtwnameHashMap["<S23>/triangle_in"] = {sid: "IP_Core_SS_Switch_and_PWM:3667"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3667"] = {rtwname: "<S23>/triangle_in"};
	this.rtwnameHashMap["<S23>/converted triangle_shift"] = {sid: "IP_Core_SS_Switch_and_PWM:3668"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3668"] = {rtwname: "<S23>/converted triangle_shift"};
	this.rtwnameHashMap["<S23>/modified triangle_shift"] = {sid: "IP_Core_SS_Switch_and_PWM:3669"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3669"] = {rtwname: "<S23>/modified triangle_shift"};
	this.rtwnameHashMap["<S23>/Zero_input"] = {sid: "IP_Core_SS_Switch_and_PWM:3670"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3670"] = {rtwname: "<S23>/Zero_input"};
	this.rtwnameHashMap["<S23>/direction up"] = {sid: "IP_Core_SS_Switch_and_PWM:3671"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3671"] = {rtwname: "<S23>/direction up"};
	this.rtwnameHashMap["<S23>/Relational Operator2"] = {sid: "IP_Core_SS_Switch_and_PWM:3672"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3672"] = {rtwname: "<S23>/Relational Operator2"};
	this.rtwnameHashMap["<S23>/Sum11"] = {sid: "IP_Core_SS_Switch_and_PWM:3673"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3673"] = {rtwname: "<S23>/Sum11"};
	this.rtwnameHashMap["<S23>/Sum6"] = {sid: "IP_Core_SS_Switch_and_PWM:3674"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3674"] = {rtwname: "<S23>/Sum6"};
	this.rtwnameHashMap["<S23>/and1_1"] = {sid: "IP_Core_SS_Switch_and_PWM:3675"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3675"] = {rtwname: "<S23>/and1_1"};
	this.rtwnameHashMap["<S23>/and1_2"] = {sid: "IP_Core_SS_Switch_and_PWM:3676"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3676"] = {rtwname: "<S23>/and1_2"};
	this.rtwnameHashMap["<S23>/shifted triangle_in signal"] = {sid: "IP_Core_SS_Switch_and_PWM:3677"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3677"] = {rtwname: "<S23>/shifted triangle_in signal"};
	this.rtwnameHashMap["<S23>/enable_out"] = {sid: "IP_Core_SS_Switch_and_PWM:3678"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3678"] = {rtwname: "<S23>/enable_out"};
	this.rtwnameHashMap["<S24>/triangle_in"] = {sid: "IP_Core_SS_Switch_and_PWM:3682"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3682"] = {rtwname: "<S24>/triangle_in"};
	this.rtwnameHashMap["<S24>/modified triangle_shift"] = {sid: "IP_Core_SS_Switch_and_PWM:3683"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3683"] = {rtwname: "<S24>/modified triangle_shift"};
	this.rtwnameHashMap["<S24>/standardized peak of triangle"] = {sid: "IP_Core_SS_Switch_and_PWM:3684"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3684"] = {rtwname: "<S24>/standardized peak of triangle"};
	this.rtwnameHashMap["<S24>/Zero_input"] = {sid: "IP_Core_SS_Switch_and_PWM:3685"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3685"] = {rtwname: "<S24>/Zero_input"};
	this.rtwnameHashMap["<S24>/direction down"] = {sid: "IP_Core_SS_Switch_and_PWM:3686"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3686"] = {rtwname: "<S24>/direction down"};
	this.rtwnameHashMap["<S24>/Relational Operator9"] = {sid: "IP_Core_SS_Switch_and_PWM:3687"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3687"] = {rtwname: "<S24>/Relational Operator9"};
	this.rtwnameHashMap["<S24>/Sum1"] = {sid: "IP_Core_SS_Switch_and_PWM:3688"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3688"] = {rtwname: "<S24>/Sum1"};
	this.rtwnameHashMap["<S24>/Sum5"] = {sid: "IP_Core_SS_Switch_and_PWM:3689"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3689"] = {rtwname: "<S24>/Sum5"};
	this.rtwnameHashMap["<S24>/and"] = {sid: "IP_Core_SS_Switch_and_PWM:3690"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3690"] = {rtwname: "<S24>/and"};
	this.rtwnameHashMap["<S24>/shifted triangle_in signal"] = {sid: "IP_Core_SS_Switch_and_PWM:3691"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3691"] = {rtwname: "<S24>/shifted triangle_in signal"};
	this.rtwnameHashMap["<S24>/enable_out"] = {sid: "IP_Core_SS_Switch_and_PWM:3692"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3692"] = {rtwname: "<S24>/enable_out"};
	this.rtwnameHashMap["<S25>/triangle_in"] = {sid: "IP_Core_SS_Switch_and_PWM:3694"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3694"] = {rtwname: "<S25>/triangle_in"};
	this.rtwnameHashMap["<S25>/modified triangle_shift"] = {sid: "IP_Core_SS_Switch_and_PWM:3695"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3695"] = {rtwname: "<S25>/modified triangle_shift"};
	this.rtwnameHashMap["<S25>/direction up"] = {sid: "IP_Core_SS_Switch_and_PWM:3696"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3696"] = {rtwname: "<S25>/direction up"};
	this.rtwnameHashMap["<S25>/Relational Operator6"] = {sid: "IP_Core_SS_Switch_and_PWM:3697"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3697"] = {rtwname: "<S25>/Relational Operator6"};
	this.rtwnameHashMap["<S25>/Sum2"] = {sid: "IP_Core_SS_Switch_and_PWM:3698"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3698"] = {rtwname: "<S25>/Sum2"};
	this.rtwnameHashMap["<S25>/Sum3"] = {sid: "IP_Core_SS_Switch_and_PWM:3699"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3699"] = {rtwname: "<S25>/Sum3"};
	this.rtwnameHashMap["<S25>/Sum4"] = {sid: "IP_Core_SS_Switch_and_PWM:3700"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3700"] = {rtwname: "<S25>/Sum4"};
	this.rtwnameHashMap["<S25>/Switch3"] = {sid: "IP_Core_SS_Switch_and_PWM:3701"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3701"] = {rtwname: "<S25>/Switch3"};
	this.rtwnameHashMap["<S25>/sw"] = {sid: "IP_Core_SS_Switch_and_PWM:3702"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3702"] = {rtwname: "<S25>/sw"};
	this.rtwnameHashMap["<S25>/shifted triangle_in signal"] = {sid: "IP_Core_SS_Switch_and_PWM:3703"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3703"] = {rtwname: "<S25>/shifted triangle_in signal"};
	this.rtwnameHashMap["<S26>/Zero_input"] = {sid: "IP_Core_SS_Switch_and_PWM:3731"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3731"] = {rtwname: "<S26>/Zero_input"};
	this.rtwnameHashMap["<S26>/converted triangle_shift"] = {sid: "IP_Core_SS_Switch_and_PWM:3732"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3732"] = {rtwname: "<S26>/converted triangle_shift"};
	this.rtwnameHashMap["<S26>/modified triangle_shift"] = {sid: "IP_Core_SS_Switch_and_PWM:3733"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3733"] = {rtwname: "<S26>/modified triangle_shift"};
	this.rtwnameHashMap["<S26>/triangle_in"] = {sid: "IP_Core_SS_Switch_and_PWM:3734"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3734"] = {rtwname: "<S26>/triangle_in"};
	this.rtwnameHashMap["<S26>/direction down"] = {sid: "IP_Core_SS_Switch_and_PWM:3735"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3735"] = {rtwname: "<S26>/direction down"};
	this.rtwnameHashMap["<S26>/Relational Operator11"] = {sid: "IP_Core_SS_Switch_and_PWM:3736"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3736"] = {rtwname: "<S26>/Relational Operator11"};
	this.rtwnameHashMap["<S26>/Sum8"] = {sid: "IP_Core_SS_Switch_and_PWM:3737"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3737"] = {rtwname: "<S26>/Sum8"};
	this.rtwnameHashMap["<S26>/Unary Minus"] = {sid: "IP_Core_SS_Switch_and_PWM:3738"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3738"] = {rtwname: "<S26>/Unary Minus"};
	this.rtwnameHashMap["<S26>/and2_1"] = {sid: "IP_Core_SS_Switch_and_PWM:3739"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3739"] = {rtwname: "<S26>/and2_1"};
	this.rtwnameHashMap["<S26>/and2_2"] = {sid: "IP_Core_SS_Switch_and_PWM:3740"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3740"] = {rtwname: "<S26>/and2_2"};
	this.rtwnameHashMap["<S26>/shifted triangle_in signal"] = {sid: "IP_Core_SS_Switch_and_PWM:3741"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3741"] = {rtwname: "<S26>/shifted triangle_in signal"};
	this.rtwnameHashMap["<S26>/enable_out"] = {sid: "IP_Core_SS_Switch_and_PWM:3742"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3742"] = {rtwname: "<S26>/enable_out"};
	this.rtwnameHashMap["<S27>/standardized peak of triangle"] = {sid: "IP_Core_SS_Switch_and_PWM:3745"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3745"] = {rtwname: "<S27>/standardized peak of triangle"};
	this.rtwnameHashMap["<S27>/triangle_in"] = {sid: "IP_Core_SS_Switch_and_PWM:3746"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3746"] = {rtwname: "<S27>/triangle_in"};
	this.rtwnameHashMap["<S27>/converted triangle_shift"] = {sid: "IP_Core_SS_Switch_and_PWM:3747"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3747"] = {rtwname: "<S27>/converted triangle_shift"};
	this.rtwnameHashMap["<S27>/modified triangle_shift"] = {sid: "IP_Core_SS_Switch_and_PWM:3748"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3748"] = {rtwname: "<S27>/modified triangle_shift"};
	this.rtwnameHashMap["<S27>/Zero_input"] = {sid: "IP_Core_SS_Switch_and_PWM:3749"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3749"] = {rtwname: "<S27>/Zero_input"};
	this.rtwnameHashMap["<S27>/direction up"] = {sid: "IP_Core_SS_Switch_and_PWM:3750"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3750"] = {rtwname: "<S27>/direction up"};
	this.rtwnameHashMap["<S27>/Relational Operator2"] = {sid: "IP_Core_SS_Switch_and_PWM:3751"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3751"] = {rtwname: "<S27>/Relational Operator2"};
	this.rtwnameHashMap["<S27>/Sum11"] = {sid: "IP_Core_SS_Switch_and_PWM:3752"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3752"] = {rtwname: "<S27>/Sum11"};
	this.rtwnameHashMap["<S27>/Sum6"] = {sid: "IP_Core_SS_Switch_and_PWM:3753"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3753"] = {rtwname: "<S27>/Sum6"};
	this.rtwnameHashMap["<S27>/and1_1"] = {sid: "IP_Core_SS_Switch_and_PWM:3754"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3754"] = {rtwname: "<S27>/and1_1"};
	this.rtwnameHashMap["<S27>/and1_2"] = {sid: "IP_Core_SS_Switch_and_PWM:3755"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3755"] = {rtwname: "<S27>/and1_2"};
	this.rtwnameHashMap["<S27>/shifted triangle_in signal"] = {sid: "IP_Core_SS_Switch_and_PWM:3756"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3756"] = {rtwname: "<S27>/shifted triangle_in signal"};
	this.rtwnameHashMap["<S27>/enable_out"] = {sid: "IP_Core_SS_Switch_and_PWM:3757"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3757"] = {rtwname: "<S27>/enable_out"};
	this.rtwnameHashMap["<S28>/triangle_in"] = {sid: "IP_Core_SS_Switch_and_PWM:3761"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3761"] = {rtwname: "<S28>/triangle_in"};
	this.rtwnameHashMap["<S28>/modified triangle_shift"] = {sid: "IP_Core_SS_Switch_and_PWM:3762"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3762"] = {rtwname: "<S28>/modified triangle_shift"};
	this.rtwnameHashMap["<S28>/standardized peak of triangle"] = {sid: "IP_Core_SS_Switch_and_PWM:3763"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3763"] = {rtwname: "<S28>/standardized peak of triangle"};
	this.rtwnameHashMap["<S28>/Zero_input"] = {sid: "IP_Core_SS_Switch_and_PWM:3764"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3764"] = {rtwname: "<S28>/Zero_input"};
	this.rtwnameHashMap["<S28>/direction down"] = {sid: "IP_Core_SS_Switch_and_PWM:3765"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3765"] = {rtwname: "<S28>/direction down"};
	this.rtwnameHashMap["<S28>/Relational Operator9"] = {sid: "IP_Core_SS_Switch_and_PWM:3766"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3766"] = {rtwname: "<S28>/Relational Operator9"};
	this.rtwnameHashMap["<S28>/Sum1"] = {sid: "IP_Core_SS_Switch_and_PWM:3767"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3767"] = {rtwname: "<S28>/Sum1"};
	this.rtwnameHashMap["<S28>/Sum5"] = {sid: "IP_Core_SS_Switch_and_PWM:3768"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3768"] = {rtwname: "<S28>/Sum5"};
	this.rtwnameHashMap["<S28>/and"] = {sid: "IP_Core_SS_Switch_and_PWM:3769"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3769"] = {rtwname: "<S28>/and"};
	this.rtwnameHashMap["<S28>/shifted triangle_in signal"] = {sid: "IP_Core_SS_Switch_and_PWM:3770"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3770"] = {rtwname: "<S28>/shifted triangle_in signal"};
	this.rtwnameHashMap["<S28>/enable_out"] = {sid: "IP_Core_SS_Switch_and_PWM:3771"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3771"] = {rtwname: "<S28>/enable_out"};
	this.rtwnameHashMap["<S29>/triangle_in"] = {sid: "IP_Core_SS_Switch_and_PWM:3773"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3773"] = {rtwname: "<S29>/triangle_in"};
	this.rtwnameHashMap["<S29>/modified triangle_shift"] = {sid: "IP_Core_SS_Switch_and_PWM:3774"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3774"] = {rtwname: "<S29>/modified triangle_shift"};
	this.rtwnameHashMap["<S29>/direction up"] = {sid: "IP_Core_SS_Switch_and_PWM:3775"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3775"] = {rtwname: "<S29>/direction up"};
	this.rtwnameHashMap["<S29>/Relational Operator6"] = {sid: "IP_Core_SS_Switch_and_PWM:3776"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3776"] = {rtwname: "<S29>/Relational Operator6"};
	this.rtwnameHashMap["<S29>/Sum2"] = {sid: "IP_Core_SS_Switch_and_PWM:3777"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3777"] = {rtwname: "<S29>/Sum2"};
	this.rtwnameHashMap["<S29>/Sum3"] = {sid: "IP_Core_SS_Switch_and_PWM:3778"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3778"] = {rtwname: "<S29>/Sum3"};
	this.rtwnameHashMap["<S29>/Sum4"] = {sid: "IP_Core_SS_Switch_and_PWM:3779"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3779"] = {rtwname: "<S29>/Sum4"};
	this.rtwnameHashMap["<S29>/Switch3"] = {sid: "IP_Core_SS_Switch_and_PWM:3780"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3780"] = {rtwname: "<S29>/Switch3"};
	this.rtwnameHashMap["<S29>/sw"] = {sid: "IP_Core_SS_Switch_and_PWM:3781"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3781"] = {rtwname: "<S29>/sw"};
	this.rtwnameHashMap["<S29>/shifted triangle_in signal"] = {sid: "IP_Core_SS_Switch_and_PWM:3782"};
	this.sidHashMap["IP_Core_SS_Switch_and_PWM:3782"] = {rtwname: "<S29>/shifted triangle_in signal"};
	this.getSID = function(rtwname) { return this.rtwnameHashMap[rtwname];}
	this.getRtwname = function(sid) { return this.sidHashMap[sid];}
}
RTW_rtwnameSIDMap.instance = new RTW_rtwnameSIDMap();
