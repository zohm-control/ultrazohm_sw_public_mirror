.. _ip_cores:

========
IP Cores
========


..	toctree::
    :maxdepth: 2
    :caption: IP Cores
    
    pwm_and_ss_control
    uz_PWM_SS_2L/uz_pwm_ss_2l
    uz_mux_axi/uz_mux_axi
    adc_LTC2311
    adc_ltc2311_v3/adc_ltc2311_v3
    uz_incrementalEncoder/uz_incrementalEncoder
    interlock
    axi_testIP/axi_testIP
    uz_interlockDeadtime2L/uz_interlockDeadtime2L
    uz_plantModel_pt1/uz_plantModel_pt1
    uz_pmsmModel/uz_pmsmModel
    uz_inverter_3ph/uz_inverter_3ph
    uz_dataMover/uz_dataMover
    uz_dq_transformation/uz_dq_transformation
    uz_mlp_three_layer/uz_mlp_three_layer
    uz_dac_interface/uz_dac_interface
    uz_rs_flip_flop/uz_rs_flip_flop
    uz_inverter_adapter/uz_inverter_adapter
    uz_cil_pmsm/uz_cil_pmsm
    uz_resolverIP/uz_resolverIP