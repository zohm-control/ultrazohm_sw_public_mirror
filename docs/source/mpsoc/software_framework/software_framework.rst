.. _software_framework:

==================
Software Framework
==================

.. toctree::
    :maxdepth: 2
    :caption: Software Framework

    software_development_guidlines/software_development_guidlines
    hardwareAbstractionLayer/hardwareAbstractionLayer
    array/array
    complex/uz_complex
    uz_Transformation/uz_Transformation
    CurrentControl/CurrentControl
    newton_raphson/newton_raphson
    movingAverage/movingAverage
    linear_decoupling/linear_decoupling
    piController/piController
    uz_resonant_controller/uz_resonant_controller
    PMSM_config/uz_PMSM_config
    signals/signals
    exponential_smoothing/uz_exp_smooth
    space_vector_limitation/space_vector_limitation
    SpeedControl/SpeedControl
    SetPoint/SetPoint
    space_vector_modulation/space_vector_modulation
    wavegen/wavegen
    global_configuration/global_configuration
    unit_tests/unit_tests
    assertions/assertions
    SystemTime/SystemTime
    matrix/matrix_math
    neural_network/neural_network
    uz_fixedpoint/uz_fixedpoint
    uz_integrator/uz_integrator
