.. _Tutorials:

=========
Tutorials
=========

In this section tutorials on how to use the system are gathered.

..	toctree::
	:maxdepth: 2
	:caption: Tutorials

	first_steps/first_steps
	first_changes/first_changes
	modify_GUI/modify_gui
	gate_signals/gate_signals
	CIL_PMSM_FOC/cil_pmsm_foc
	vio_led_optical/vio_led
	encoder/encoder
	adc_loopback/adc_loopback
	simscape_hdl/simscape_hdl